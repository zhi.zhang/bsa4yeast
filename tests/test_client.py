import re
import unittest
from flask import url_for,session
from app import create_app, db
from app.models import User,Role,Files,Permission
from StringIO import StringIO
import json
import os

class FlaskClientTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.app.config['SECRET_KEY'] = os.environ.get('SECRET_KEY') or 'hard to guess string'
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        User.query.all()
        Role.query.all()
        Files.query.all()
        self.client = self.app.test_client(use_cookies=True)

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_index_page_get(self):
        response = self.client.get(url_for('auth.index'))
        self.assertTrue(b'a) Enter the number (or pairs) of Map file:' in response.data)
        
    def test_index_page_post(self):
        response = self.client.post(url_for('main.index'),data={'numbermap':1})
        self.assertTrue(b'Redirecting...' in response.data)

    def test_index2_page_get(self):
        with self.client as c:
            with c.session_transaction() as sess:
                sess['numbermap'] = 1
            response = c.get('auth/index2')
        self.assertTrue(b'Enter the type of smoothing kernel:' in response.data)

    def test_index2_page_post(self):
        with self.client as c:
            with c.session_transaction() as sess:
                sess['numbermap'] = 1
                sess['filename1'] = [os.path.join(self.app.config['UPLOAD_FOLDER'], '1047a_YO416b_YJP_SCraff.Hmap')]
                sess['filename2'] = [os.path.join(self.app.config['UPLOAD_FOLDER'], '1047a_YO416b_YJP_SCraff.Lmap')]
            response = c.post('auth/index2')
        self.assertTrue(b'Redirecting...' in response.data)
        
    def test_upload_page_get(self):
        with self.client as c:
            with c.session_transaction() as sess:
                sess['filename1'] = [os.path.join(self.app.config['UPLOAD_FOLDER'], '1047a_YO416b_YJP_SCraff.Hmap')]
                sess['filename2'] = [os.path.join(self.app.config['UPLOAD_FOLDER'], '1047a_YO416b_YJP_SCraff.Lmap')]
                sess['numbermap'] = 1
                sess['kernel'] = 'tricube'
                sess['width'] = 33750
                sess['chrnum'] = 'all'
                sess['noraw'] = 'no'
            response = c.get('/upload')
        self.assertTrue(b'The input setting up is:' in response.data)    
    
    def test_upload_page_post(self):
        with self.client as c:
            with c.session_transaction() as sess:
                sess['filename1'] = [os.path.join(self.app.config['UPLOAD_FOLDER'], '1047a_YO416b_YJP_SCraff.Hmap')]
                sess['filename2'] = [os.path.join(self.app.config['UPLOAD_FOLDER'], '1047a_YO416b_YJP_SCraff.Lmap')]
                sess['numbermap'] = 1
                sess['kernel'] = 'tricube'
                sess['width'] = 33750
                sess['chrnum'] = 'all'
                sess['noraw'] = 'no'
            response = c.post('/upload')
            print dir(response)
            print response.data
        self.assertTrue(b'Redirecting...' in response.data) 
