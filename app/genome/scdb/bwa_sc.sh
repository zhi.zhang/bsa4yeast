#!/bin/bash -login
module load BWA/0.7.5a-goolf-1.4.10
bwa index -a bwtsw sc64.fa
java -jar /home/users/zzhang/picard-tools-1.110/picard-tools-1.110/CreateSequenceDictionary.jar R=sc64.fa O= sc64.dict
module load SAMtools/0.1.18-ictce-5.3.0
samtools faidx sc64.fa
