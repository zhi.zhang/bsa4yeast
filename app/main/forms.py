from flask_wtf import Form
from wtforms.validators import Required, Length, Email, Regexp, EqualTo, NumberRange, Optional
from wtforms import ValidationError,RadioField, FloatField, FileField, SubmitField, FieldList, TextField, SelectField, StringField, FormField, PasswordField, BooleanField,TextAreaField
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from .. import db
from ..models import User,Role,Files
from wtforms.fields.core import IntegerField
from flask import current_app,session
from flask_login import current_user
#from flask_login import login_user, logout_user, login_required, current_user

#class LoginForm(Form):
#    email = StringField('Email', validators=[Required(), Length(1, 64),
#                                             Email()])
#    password = PasswordField('Password', validators=[Required()])
#    remember_me = BooleanField('Keep me logged in')
#    submit = SubmitField('Log In')
#
#
#class RegistrationForm(Form):
#    email = StringField('Email', validators=[Required(), Length(1, 64),
#                                           Email()])
#    username = StringField('Username', validators=[
#        Required(), Length(1, 64), Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
#                                          'Usernames must have only letters, '
#                                          'numbers, dots or underscores')])
#    password = PasswordField('Password', validators=[
#        Required(), EqualTo('password2', message='Passwords must match.')])
#    password2 = PasswordField('Confirm password', validators=[Required()])
#    submit = SubmitField('Register')
#
#    def validate_email(self, field):
#        if User.query.filter_by(email=field.data).first():
#            raise ValidationError('Email already registered.')
#
#    def validate_username(self, field):
#        if User.query.filter_by(username=field.data).first():
#            raise ValidationError('Username already in use.')
#
#
#class ChangePasswordForm(Form):
#    old_password = PasswordField('Old password', validators=[Required()])
#    password = PasswordField('New password', validators=[
#        Required(), EqualTo('password2', message='Passwords must match')])
#    password2 = PasswordField('Confirm new password', validators=[Required()])
#    submit = SubmitField('Update Password')
#
#
#class PasswordResetRequestForm(Form):
#    email = StringField('Email', validators=[Required(), Length(1, 64),
#                                             Email()])
#    submit = SubmitField('Reset Password')
#
#
#class PasswordResetForm(Form):
#    email = StringField('Email', validators=[Required(), Length(1, 64),
#                                             Email()])
#    password = PasswordField('New Password', validators=[
#        Required(), EqualTo('password2', message='Passwords must match')])
#    password2 = PasswordField('Confirm password', validators=[Required()])
#    submit = SubmitField('Reset Password')
#
#    def validate_email(self, field):
#        if User.query.filter_by(email=field.data).first() is None:
#            raise ValidationError('Unknown email address.')
#
#
#class ChangeEmailForm(Form):
#    email = StringField('New Email', validators=[Required(), Length(1, 64),
#                                                 Email()])
#    password = PasswordField('Password', validators=[Required()])
#    submit = SubmitField('Update Email Address')
#
#    def validate_email(self, field):
#        if User.query.filter_by(email=field.data).first():
#            raise ValidationError('Email already registered.')

class UploadTestForm(Form):
    uploadfile1 = FileField('Enter uploading file1', validators=[Required()])
    uploadfile2 = FileField('Enter uploading file2', validators=[Required()])
    submit = SubmitField('Upload')
       
def fastq_choices():      
    return Files.query.filter_by(owners=session.get('anony_id')).filter_by(ftype1 ='U').filter_by(ftype3 ='Fastq')
    
def bam_choices():      
    return Files.query.filter_by(owners=session.get('anony_id')).filter_by(ftype1 ='U').filter_by(ftype3 ='Bam')

def map_choices():      
    return Files.query.filter_by(owners=session.get('anony_id')).filter_by(ftype1 ='U').filter_by(ftype3 ='Map')

def length_choices():      
    return Files.query.filter_by(owners=session.get('anony_id')).filter_by(ftype1 ='U').filter_by(ftype3 ='Length')

class NameFormLength(Form):
    uploadLengthfile = QuerySelectField('Length file&nbsp&nbsp', validators=[Required()],query_factory=length_choices)
    
class NameFormBam(Form):
    uploadfile1 = QuerySelectField('H_Bam file&nbsp&nbsp', validators=[Required()],query_factory=bam_choices)
    uploadfile2 = QuerySelectField('L_Bam file&nbsp&nbsp', validators=[Required()],query_factory=bam_choices)
    
class NameFormBam1(Form):
    uploadfile1 = QuerySelectField(u'Bam file&nbsp&nbsp', validators=[Required()],query_factory=bam_choices)

class NameFormFastq(Form):
    uploadfile1 = QuerySelectField(u'H_Fastq file&nbsp&nbsp', validators=[Required()],query_factory=fastq_choices)
    uploadfile2 = QuerySelectField(u'L_Fastq file&nbsp&nbsp', validators=[Required()],query_factory=fastq_choices)
    
class NameFormFastq1(Form):
    uploadfile1 = QuerySelectField(u'Fastq file&nbsp&nbsp', validators=[Required()],query_factory=fastq_choices)

class NameFormMap(Form):
    uploadfile1 = QuerySelectField(u'H_Map file&nbsp&nbsp', validators=[Required()],query_factory=map_choices)
    uploadfile2 = QuerySelectField(u'L_Map file&nbsp&nbsp', validators=[Required()],query_factory=map_choices)
    
class NameFormMap1(Form):
    uploadfile1 = QuerySelectField(u'Map file&nbsp&nbsp', validators=[Required()],query_factory=map_choices)

class SubmitForm(Form):
    submit = SubmitField('Calculate G\'')
    
class IndexForm(Form):
    input_type = SelectField('a) Choose input file type: ', default='Fastq', choices=[('Fastq', 'Fastq'), ('Bam', 'Bam'), ('Map', 'Map')], validators=[Optional()])
    numbermap = SelectField('b) Enter the number of duplicate: ', default='1',choices=[(str(i), str(i)) for i in range(1, 21)], validators=[Optional()])
    result_prefix=StringField('c) Enter the prefix of result: ',default='myresult',validators=[
        Required(), Length(1, 64), Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
                                          'Usernames must have only letters, '
                                          'numbers, dots or underscores')])
    bulk_design = SelectField('d) Enter 1_ or 2_ bulk design: ', choices=(('1','1'),('2','2')), default='2', validators=[Optional()])
    submit = SubmitField('Next')
    
class IndexForm_anony(Form):
    input_type = SelectField('a) Choose input file type: ', default='Fastq', choices=[('Fastq', 'Fastq'), ('Bam', 'Bam'), ('Map', 'Map')], validators=[Optional()])
    numbermap = SelectField('b) Enter the number (or pairs) of Bulk file: ', default='1',choices=[(str(i), str(i)) for i in range(1, 21)], validators=[Optional()])
    result_prefix=StringField('c) Enter the prefix of result: ',default='myresult',validators=[
        Required(), Length(1, 64), Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
                                          'Usernames must have only letters, '
                                          'numbers, dots or underscores')])
    bulk_design = SelectField('d) Enter 1_ or 2_ bulk design: ', choices=(('1','1'),('2','2')), default='2', validators=[Optional()])
    email = StringField('e) Optional email:', validators=[Optional(), Length(0, 64),Email()]) 
    submit = SubmitField('Next')

class FastqForm2(Form):
    P1fastqfile = QuerySelectField('Enter P1 fastq file: ', validators=[Required()],query_factory=fastq_choices)
    P2fastqfile = QuerySelectField('Enter P2 fastq file: ', validators=[Required()],query_factory=fastq_choices)
    Bulkfastqfile = FieldList(FormField(NameFormFastq),label='Bulk fastq file list: ', min_entries=0, validators=[Required()])
    readlen=IntegerField('Enter the length of reads (bp): ', default=100, validators=[Required(),NumberRange(20,2000)])
    phred=SelectField('Enter Phred score: ', choices=(('33','33'),('64','64')), default='33', validators=[Optional()])
    qccheck=SelectField('Enter whether perform qccheck: ', choices=(('true','true'),('false','false')), default='true', validators=[Optional()])
    trim=SelectField('Enter whether perform trim reads: ', choices=(('true','true'),('false','false')), default='true', validators=[Optional()])
    lastkeep=IntegerField('Enter the length of trimmed reads (bp): ', default=75, validators=[Required(),NumberRange(20,2000)])
    rmadapt=SelectField('Enter whether remove adapter of reads: ', choices=(('true','true'),('false','false')), default='true', validators=[Optional()])
    ladapter=StringField('Enter the left adapter: ',default='AGATCGGAAGAGC', validators=[Optional(),Regexp('^[ATCGatcg]*$')])
    radapter=StringField('Enter the right adapter: ',default='AGATCGGAAGAGC', validators=[Optional(),Regexp('^[ATCGatcg]*$')])
    minlen=IntegerField('Discard trimmed reads that are shorter than (bp): ', default=50, validators=[Required(),NumberRange(20,2000)])
    removen=SelectField('Enter whether remove "N" bases of reads: ', choices=(('true','true'),('false','false')), default='true', validators=[Optional()])
    ncutoff=FloatField('N cutoff: ', default=0.1, validators=[Required(),NumberRange(0,1)])
    filtq=SelectField('Filters sequences based on quality of reads: ', choices=(('true','true'),('false','false')), default='true', validators=[Optional()])
    minq=IntegerField('Minimum quality score to keep: ', default=20, validators=[Required(),NumberRange(10,35)])
    pminq=IntegerField('Minimum percent of bases: ', default=80, validators=[Required(),NumberRange(0,100)])
    rmunpaired=SelectField('Remove unpaied reads of reads: ', choices=(('true','true'),('false','false')), default='true', validators=[Optional()])
    qcstat=SelectField('Generate QC statistic plot for reads: ', choices=(('true','true'),('false','false')), default='true', validators=[Optional()])
    insertsize=IntegerField('Insert size of sequencing: ', default=400, validators=[Required(),NumberRange(20,800)])
    windowsize=IntegerField('Coverage window size for plot: ', default=50000, validators=[Required(),NumberRange(5000,100000)])
    pass
    cov_depth = SelectField('Enter the number of the depth of coverage: ', default='5',choices=[(str(i), str(i)) for i in range(5, 105,5)], validators=[Optional()])
    kernel = SelectField('Enter the type of smoothing kernel: ', default='tricube', choices=[('tricube', 'tricube'), ('uniform', 'uniform'), ('triangle', 'triangle'), ('triweight', 'triweight'), ('quartic', 'quartic')], validators=[Optional()])
    width = IntegerField('Enter the width of the smoothing kernel (bp): ', default=33750, validators=[Required(),NumberRange(10000,1000000)])
    a = [(str(i), str(i)) for i in range(1, 17)]
    a.insert(0, ('all', 'all'))
    chrnum = SelectField('Enter the chromosome number to draw: ', choices=a, default='all', validators=[Optional()])
    noraw = SelectField("Enter whether drawing raw G\' values or not: ", choices=[('no', 'no'), ('yes', 'yes')], default='no', validators=[Optional()])
    submit = SubmitField('Upload')

class BamForm2(Form):
    P1bamfile = QuerySelectField('Enter P1 bam file: ', validators=[Required()],query_factory=bam_choices)
    P2bamfile = QuerySelectField('Enter P2 bam file: ', validators=[Required()],query_factory=bam_choices)
    Bulkbamfile = FieldList(FormField(NameFormBam),label='Bam file list: ', min_entries=0, validators=[Required()])
    cov_depth = SelectField('Enter the number of the depth of coverage: ', default='5',choices=[(str(i), str(i)) for i in range(5, 105,5)], validators=[Optional()])
    kernel = SelectField('Enter the type of smoothing kernel: ', default='tricube', choices=[('tricube', 'tricube'), ('uniform', 'uniform'), ('triangle', 'triangle'), ('triweight', 'triweight'), ('quartic', 'quartic')], validators=[Optional()])
    width = IntegerField('Enter the width of the smoothing kernel (bp): ', default=33750, validators=[Required(),NumberRange(10000,1000000)])
    a = [(str(i), str(i)) for i in range(1, 17)]
    a.insert(0, ('all', 'all'))
    chrnum = SelectField('Enter the chromosome number to draw: ', choices=a, default='all', validators=[Optional()])
    noraw = SelectField("Enter whether drawing raw G\' values or not: ", choices=[('no', 'no'), ('yes', 'yes')], default='no', validators=[Optional()])
    submit = SubmitField('Upload')
    
class MapForm2(Form):
    Bulkmapfile = FieldList(FormField(NameFormMap),label='Map file list: ', min_entries=0, validators=[Required()])
    kernel = SelectField('Enter the type of smoothing kernel: ', default='tricube', choices=[('tricube', 'tricube'), ('uniform', 'uniform'), ('triangle', 'triangle'), ('triweight', 'triweight'), ('quartic', 'quartic')], validators=[Optional()])
    length_input=FieldList(FormField(NameFormLength),label='Length file: ', min_entries=0, validators=[Required()])
    width = IntegerField('Enter the width of the smoothing kernel (bp): ', default=33750, validators=[Required(),NumberRange(10000,1000000)])
    a = [(str(i), str(i)) for i in range(1, 17)]
    a.insert(0, ('all', 'all'))
    chrnum = SelectField('Enter the chromosome number to draw: ', choices=a, default='all', validators=[Optional()])
    noraw = SelectField("Enter whether drawing raw G\' values or not: ", choices=[('no', 'no'), ('yes', 'yes')], default='no', validators=[Optional()])
    submit = SubmitField('Upload')

class FastqForm1(Form):
    P1fastqfile = QuerySelectField('Enter P1 fastq file', validators=[Required()],query_factory=fastq_choices)
    P2fastqfile = QuerySelectField('Enter P2 fastq file', validators=[Required()],query_factory=fastq_choices)
    Bulkfastqfile = FieldList(FormField(NameFormFastq1),label='Bulk fastq file list: ', min_entries=0, validators=[Required()])
    readlen=IntegerField('Enter the length of reads (bp): ', default=100, validators=[Required(),NumberRange(20,2000)])
    phred=SelectField('Enter Phred score: ', choices=(('33','33'),('64','64')), default='33', validators=[Optional()])
    qccheck=SelectField('Enter whether perform qccheck: ', choices=(('true','true'),('false','false')), default='true', validators=[Optional()])
    trim=SelectField('Enter whether perform trim reads: ', choices=(('true','true'),('false','false')), default='true', validators=[Optional()])
    lastkeep=IntegerField('Enter the length of trimmed reads (bp): ', default=75, validators=[Required(),NumberRange(20,2000)])
    rmadapt=SelectField('Enter whether remove adapter of reads: ', choices=(('true','true'),('false','false')), default='true', validators=[Optional()])
    ladapter=StringField('Enter the left adapter: ',default='AGATCGGAAGAGC', validators=[Optional(),Regexp('^[ATCGatcg]*$')])
    radapter=StringField('Enter the right adapter: ',default='AGATCGGAAGAGC', validators=[Optional(),Regexp('^[ATCGatcg]*$')])
    minlen=IntegerField('Discard trimmed reads that are shorter than (bp): ', default=50, validators=[Required(),NumberRange(20,2000)])
    removen=SelectField('Enter whether remove "N" bases of reads: ', choices=(('true','true'),('false','false')), default='true', validators=[Optional()])
    ncutoff=FloatField('N cutoff: ', default=0.1, validators=[Required(),NumberRange(0,1)])
    filtq=SelectField('Filters sequences based on quality of reads: ', choices=(('true','true'),('false','false')), default='true', validators=[Optional()])
    minq=IntegerField('Minimum quality score to keep: ', default=20, validators=[Required(),NumberRange(10,35)])
    pminq=IntegerField('Minimum percent of bases: ', default=80, validators=[Required(),NumberRange(0,100)])
    rmunpaired=SelectField('Remove unpaied reads of reads: ', choices=(('true','true'),('false','false')), default='true', validators=[Optional()])
    qcstat=SelectField('Generate QC statistic plot for reads: ', choices=(('true','true'),('false','false')), default='true', validators=[Optional()])
    insertsize=IntegerField('Insert size of sequencing: ', default=400, validators=[Required(),NumberRange(20,800)])
    windowsize=IntegerField('Coverage window size for plot: ', default=50000, validators=[Required(),NumberRange(5000,100000)])
    pass
    cov_depth = SelectField('Enter the number of the depth of coverage: ', default='5',choices=[(str(i), str(i)) for i in range(5, 105,5)], validators=[Optional()])
    kernel = SelectField('Enter the type of smoothing kernel: ', default='tricube', choices=[('tricube', 'tricube'), ('uniform', 'uniform'), ('triangle', 'triangle'), ('triweight', 'triweight'), ('quartic', 'quartic')], validators=[Optional()])
    width = IntegerField('Enter the width of the smoothing kernel (bp): ', default=33750, validators=[Required(),NumberRange(10000,1000000)])
    a = [(str(i), str(i)) for i in range(1, 17)]
    a.insert(0, ('all', 'all'))
    chrnum = SelectField('Enter the chromosome number to draw: ', choices=a, default='all', validators=[Optional()])
    noraw = SelectField("Enter whether drawing raw G\' values or not: ", choices=[('no', 'no'), ('yes', 'yes')], default='no', validators=[Optional()])
    submit = SubmitField('Upload')
    
class BamForm1(Form):
    P1bamfile = QuerySelectField('Enter P1 bam file', validators=[Required()],query_factory=bam_choices)
    P2bamfile = QuerySelectField('Enter P2 bam file', validators=[Required()],query_factory=bam_choices)
    Bulkbamfile = FieldList(FormField(NameFormBam1),label='Bam file list: ', min_entries=0, validators=[Required()])
    cov_depth = SelectField('Enter the number of the depth of coverage: ', default='5',choices=[(str(i), str(i)) for i in range(5, 105,5)], validators=[Optional()])
    kernel = SelectField('Enter the type of smoothing kernel: ', default='tricube', choices=[('tricube', 'tricube'), ('uniform', 'uniform'), ('triangle', 'triangle'), ('triweight', 'triweight'), ('quartic', 'quartic')], validators=[Optional()])
    width = IntegerField('Enter the width of the smoothing kernel (bp): ', default=33750, validators=[Required(),NumberRange(10000,1000000)])
    a = [(str(i), str(i)) for i in range(1, 17)]
    a.insert(0, ('all', 'all'))
    chrnum = SelectField('Enter the chromosome number to draw: ', choices=a, default='all', validators=[Optional()])
    noraw = SelectField("Enter whether drawing raw G' values or not: ", choices=[('no', 'no'), ('yes', 'yes')], default='no', validators=[Optional()])
    submit = SubmitField('Upload')
    
class MapForm1(Form):
    Bulkmapfile = FieldList(FormField(NameFormMap1),label='Map file list: ', min_entries=0, validators=[Required()])
    kernel = SelectField('Enter the type of smoothing kernel: ', default='tricube', choices=[('tricube', 'tricube'), ('uniform', 'uniform'), ('triangle', 'triangle'), ('triweight', 'triweight'), ('quartic', 'quartic')], validators=[Optional()])
    length_input=FieldList(FormField(NameFormLength),label='Length file: ', min_entries=0, validators=[Required()])
    width = IntegerField('Enter the width of the smoothing kernel (bp): ', default=33750, validators=[Required(),NumberRange(10000,1000000)])
    a = [(str(i), str(i)) for i in range(1, 17)]
    a.insert(0, ('all', 'all'))
    chrnum = SelectField('Enter the chromosome number to draw: ', choices=a, default='all', validators=[Optional()])
    noraw = SelectField("Enter whether drawing raw G' values or not: ", choices=[('no', 'no'), ('yes', 'yes')], default='no', validators=[Optional()])
    submit = SubmitField('Upload')
    
#class EditProfileForm(Form):
#    name = StringField('Real name', validators=[Length(0, 64)])
#    location = StringField('Location', validators=[Length(0, 64)])
#    about_me = TextAreaField('About me')
#    submit = SubmitField('Submit')
#
#class EditProfileAdminForm(Form):
#    email = StringField('Email', validators=[Required(), Length(1, 64),
#                                             Email()])
#    username = StringField('Username', validators=[
#        Required(), Length(1, 64), Regexp('^[A-Za-z][A-Za-z0-9_.]*$', 0,
#                                          'Usernames must have only letters, '
#                                          'numbers, dots or underscores')])
#    confirmed = BooleanField('Confirmed')
#    role = SelectField('Role', coerce=int)
#    name = StringField('Real name', validators=[Length(0, 64)])
#    location = StringField('Location', validators=[Length(0, 64)])
#    about_me = TextAreaField('About me')
#    submit = SubmitField('Submit')
#
#    def __init__(self, user, *args, **kwargs):
#        super(EditProfileAdminForm, self).__init__(*args, **kwargs)
#        self.role.choices = [(role.id, role.name)
#                             for role in Role.query.order_by(Role.name).all()]
#        self.user = user
#
#    def validate_email(self, field):
#        if field.data != self.user.email and \
#                User.query.filter_by(email=field.data).first():
#            raise ValidationError('Email already registered.')
#
#    def validate_username(self, field):
#        if field.data != self.user.username and \
#                User.query.filter_by(username=field.data).first():
#            raise ValidationError('Username already in use.')
