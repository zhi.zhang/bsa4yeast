from flask import jsonify, flash, render_template, session, redirect, url_for, current_app, send_from_directory, request,g
from flask_security import Security, SQLAlchemyUserDatastore, UserMixin, RoleMixin, login_required
from werkzeug import secure_filename
from .. import db
from ..models import User,Role,Files,Permission,Tasks
# from ..email import send_email
from . import main
#from ..auth import auth
from .forms import   SubmitForm,IndexForm, FastqForm2,FastqForm1,BamForm2,BamForm1,MapForm2,MapForm1,UploadTestForm,IndexForm_anony
#from flask_login import login_user, logout_user, login_required, current_user
from flask_login import current_user
import os
import time 
import uuid
import os.path
#from app.tasks.mytask import long_task_bam2,long_task_bam1,long_task_fastq2,long_task_fastq1,long_task_map2,long_task_map1 
from app.tasks.mytask_anony import long_task_bam2_anony,long_task_bam1_anony,long_task_fastq2_anony,long_task_fastq1_anony,long_task_map2_anony,long_task_map1_anony 
import subprocess
import pandas as pd
import re, sys
from datetime import datetime
from app import celery
from celery.app.control import Control
import shutil
from collections import OrderedDict
import json
#from ..decorators import admin_required, permission_required
import gzip

basedir = os.path.dirname(os.path.dirname(__file__))

def remove_folder(path):
    # check if folder exists
    if os.path.exists(path):
         # remove if exists
         shutil.rmtree(path)

def get_file_size(f):
    if os.path.isfile(f):
        file_size=os.path.getsize(f)
        if file_size<1000:
            return str(file_size)+'B'
        elif file_size >= 1000 and file_size<1000000:
            return str(file_size/1000)+'K'
        elif file_size >= 1000000 and file_size<1000000000:
            return str(file_size/1000000)+'M'
        elif file_size >= 1000000000 and file_size<1000000000000:
            return str(file_size/1000000000)+'G'
        else:
            return 'Not Available'
    else:
        return 'Not A File'
        
def get_folder_size(start_path):
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            total_size += os.path.getsize(fp)
    return total_size

def count_free_upload_api():
    anony_id=session.get('anony_id')
    used_upload=get_folder_size(os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'],anony_id))
    free_upload=int(current_app.config['CUSTOM_UPLOADS_FOLDER_SIZE_LIMIT'])-int(used_upload)
    if free_upload>0:
        return free_upload
    else:
        return 'NA'

def count_free_result_api():
    anony_id=session.get('anony_id')
    used_result=get_folder_size(os.path.join(current_app.config['RESULTS_ANONY_FOLDER'],anony_id))
    free_result=int(current_app.config['CUSTOM_RESULTS_FOLDER_SIZE_LIMIT'])-int(used_result)
    if free_result>0:
        return free_result
    else:
        return 'NA'

class Solution(object):
    def intToRoman(self, num):
        """
        :type num: int
        :rtype: str
        """
        M = ["", "M", "MM", "MMM"]
        C = ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"]
        X = ["", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"]
        I = ["", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"]
        return M[num/1000] + C[(num%1000)/100] + X[(num%100)/10] + I[num%10]
        
@main.before_app_request
def before_first_request():
    if current_user.is_anonymous:
#        print dir(request)
#        print request
#        print "*"*20
#        print request.full_path
#        print "*"*20
        pattern=re.compile(r'[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}')
        fullpath=request.full_path
        if re.findall(pattern,fullpath):
            link_anony_id=re.findall(pattern,fullpath)[0]
            if Files.query.filter_by(owners=link_anony_id).first():
                session['anony_id']=link_anony_id
        elif session.get('anony_id'):
            session['anony_id']=session.get('anony_id')
        else:
            session['anony_id']=str(uuid.uuid4())
        if not os.path.exists(os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'], session['anony_id'])):
                os.mkdir(os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'], session['anony_id']))    
        if not Files.query.filter_by(owners=session['anony_id']).filter_by(ftype1 ='U').filter_by(ftype3='Length').filter_by(fname='sacCer3.length').all():
            with open(os.path.join(os.path.dirname(os.path.dirname(__file__)),'genome/sc_chr.length')) as f_in:
                with open(os.path.join(os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'], session['anony_id']), 'sacCer3.length'), 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)
                    files1=Files(ftype1='U',ftype2='-',ftype3='Length',ffolder='-',fname='sacCer3.length',fsize=get_file_size(os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'], session['anony_id'],'sacCer3.length')),owners=session['anony_id'],owners_name='Anonymous')
                    db.session.add(files1)
        print "%%%%%%%%%%%%%%%%%%%%%"
        print session['anony_id']
#        return redirect(url_for('main.%s.upload_accept'%(session['anony_id'])),anony_id=session['anony_id'])
#        return render_template('upload_anony.html',anony_id=session['anony_id']) 
           
@main.route('/', methods=['GET', 'POST'])
def index():
    if current_user.is_authenticated:
        return redirect(url_for('auth.index'))
    else:
        return render_template('index.html')
        
@main.route('/<anony_id>', methods=['GET', 'POST'])
def index_anony(anony_id):
    if current_user.is_anonymous:
        pattern=re.compile(r'[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}')
        fullpath=request.full_path
        if re.findall(pattern,fullpath):
            link_anony_id=re.findall(pattern,fullpath)[0]
            if Files.query.filter_by(owners=link_anony_id).first():
                session['anony_id']=link_anony_id
        return render_template('index.html',anony_id=session.get('anony_id'))

#@main.route('/admin')
#@login_required
#@admin_required
#def for_admins_only():
#    return "For administrators!"
#    
#@main.route('/moderator')
#@login_required
#@permission_required(Permission.MODERATE_COMMENTS)
#def for_moderators_only():
#    return "For comment moderators!"
    
@main.route('/downloadref/<file>')
def downloadref(file):
    """Download ref"""
    return send_from_directory(os.path.join(current_app.config['UPLOADS_FOLDER'], 'ref'), file, as_attachment=True)

@main.route('/<anony_id>/Home', methods=['GET', 'POST'])
def Home(anony_id):
    anony_id=session.get('anony_id')
    return redirect(url_for('main.index',anony_id=anony_id))

@main.route('/<anony_id>/Use', methods=['GET', 'POST'])
def Use(anony_id):
    anony_id=session.get('anony_id')
    return render_template('Use.html',anony_id=anony_id)

@main.route('/<anony_id>/Contact', methods=['GET', 'POST'])
def Contact(anony_id):
    anony_id=session.get('anony_id')
    return render_template('Contact.html',anony_id=anony_id)

@main.route('/<anony_id>/Reference', methods=['GET', 'POST'])
def Reference(anony_id):
    anony_id=session.get('anony_id')
    ref = 'BSA_NGS.pdf'
    ref2='ref2.pdf'
    ref3='ref3.pdf'
    sacCer3_refGene='sacCer3_refGene.txt'
    sacCer3_refGeneMrna='sacCer3_refGeneMrna.fa'
    sc_chr_length='sc_chr.length'
    sc64_bed='sc64.bed'
    sc64_fa='sc64.fa'
    sc64_fa_fai='sc64.fa.fai'
    sc64_gtf='sc64.gtf'
    return render_template('Reference.html', anony_id=anony_id,ref=ref,ref2=ref2,ref3=ref3,sacCer3_refGene=sacCer3_refGene,sacCer3_refGeneMrna=sacCer3_refGeneMrna,sc_chr_length=sc_chr_length,sc64_bed=sc64_bed,sc64_fa=sc64_fa,sc64_fa_fai=sc64_fa_fai,sc64_gtf=sc64_gtf)

@main.route('/<anony_id>/upload_accept', methods=['GET', 'POST'])
def upload_accept(anony_id):
    anony_id=session.get('anony_id')
    try:
        if request.method == 'POST' and request.files['file'].filename.endswith(('.map','.fastq','fastq.gz','.bam','.length')):
            task = anony_id
#            task=current_user.get_id()
            if not os.path.exists(os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'], task)):
                os.mkdir(os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'], task))
            user_upload_folder_size=  get_folder_size(os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'], task))
            if user_upload_folder_size>int(current_app.config['CUSTOM_UPLOADS_FOLDER_SIZE_LIMIT']):
                flash ('Your upload folder has too many files! Please delete some files first using Uploeaded Files tab at left nav bar.')
                return render_template('500.html')
            elif request.method == 'POST' and not request.files['file'].filename.endswith(('.map','.fastq','.fastq.gz','.bam','.length')):
                flash ("The file's name must be ended with '.fastq' ,'.fastq.gz', '.bam', '.map' or '.length'.")  
                return render_template('upload_anony.html')
            elif request.method == 'POST' and Files.query.filter_by(owners=task).filter_by(fname =request.files['file'].filename).first():
                flash ('The file name already uesed, change another one!')
                return render_template('500.html') 
            else:
                pass
            upload_file = request.files['file']
#            print   upload_file.filename
#            print   dir(upload_file)
#            print request.form
#            print request
#            print dir(request)       
            chunk = request.form.get('chunk', 0)        
            filename = '%s_%s_%s' % (upload_file.filename,task, chunk)       
            upload_file.save(os.path.join(os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'], task), filename)) 
    except Exception,e:
        print "***********************" 
        print task
        print e
        return redirect(url_for('main.upload_accept',anony_id=session.get('anony_id')))      
    return render_template('upload_anony.html',anony_id=session.get('anony_id')) 
    
@main.route('/<anony_id>/upload_complete', methods=['GET'])
def upload_complete(anony_id): 
    anony_id=session.get('anony_id')
    if request.method == 'GET':
        task = anony_id  
        ext = request.args.get('ext', '')  
        upload_type = request.args.get('type') 
        filename= request.args.get('filename') 
        if len(ext) == 0 and upload_type:  
            ext = upload_type.split('/')[1]  
        ext = '' if len(ext) == 0 else '.%s' % ext     
        chunk = 0  
        with open(os.path.join(os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'], task), filename), 'w') as target_file: 
            while True:  
                try:  
                    filename_chunk = os.path.join(os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'], task), filename+'_'+task+'_'+str(chunk))  
                    source_file = open(filename_chunk, 'r')                 
                    target_file.write(source_file.read())             
                    source_file.close()  
                except IOError:  
                    break  
                chunk += 1  
                os.remove(filename_chunk)
        if filename.endswith('.fastq'):
            files1=Files(ftype1='U',ftype2='-',ftype3='Fastq',ffolder='-',fname=filename,fsize=get_file_size(os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'], task,filename)),owners=task,owners_name='Anonymous')
            db.session.add(files1)
        elif filename.endswith('.fastq.gz'):
#            with gzip.open(os.path.join(os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'], task), filename), 'rb') as f_in:
#                with open(os.path.join(os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'], task), os.path.splitext(filename)[0]), 'wb') as f_out:
#                    shutil.copyfileobj(f_in, f_out)
#            os.remove(os.path.join(os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'], task), filename))
#            files1=Files(ftype1='U',ftype2='-',ftype3='Fastq.gz',ffolder='-',fname=os.path.splitext(filename)[0],fsize=get_file_size(os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'], task,os.path.splitext(filename)[0])),owners=task,owners_name='Anonymous')
            files1=Files(ftype1='U',ftype2='gz',ftype3='Fastq',ffolder='-',fname=filename,fsize=get_file_size(os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'], task,filename)),owners=task,owners_name='Anonymous')
            db.session.add(files1)
        elif filename.endswith('.bam'):
            files1=Files(ftype1='U',ftype2='-',ftype3='Bam',ffolder='-',fname=filename,fsize=get_file_size(os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'], task,filename)),owners=task,owners_name='Anonymous')
            db.session.add(files1)
        elif filename.endswith('.map'):
            files1=Files(ftype1='U',ftype2='-',ftype3='Map',ffolder='-',fname=filename,fsize=get_file_size(os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'], task,filename)),owners=task,owners_name='Anonymous')
            db.session.add(files1) 
        elif filename.endswith('.length'):
            files1=Files(ftype1='U',ftype2='-',ftype3='Length',ffolder='-',fname=filename,fsize=get_file_size(os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'], task,filename)),owners=task,owners_name='Anonymous')
            db.session.add(files1)
        else:
            pass
#        return redirect(url_for('main.upload_accept',anony_id=session.get('anony_id')))
        print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
        return render_template('upload_anony_success.html',anony_id=session.get('anony_id'))
#        flash('Not right file format!')
#        return redirect(url_for('auth.upload_accept'))                    
#    return render_template('upload.html')  
#    return redirect(url_for('auth.upload_accept'))
#    return render_template('upload_anony.html',anony_id=session.get('anony_id'))
    return render_template('upload_anony_success.html',anony_id=session.get('anony_id'))
        
@main.route('/<anony_id>/analysis1', methods=['GET', 'POST'])
def analysis1(anony_id):
    anony_id=session.get('anony_id')
    form = IndexForm_anony(request.form)
    user_result_folder_size=  get_folder_size(os.path.join(current_app.config['RESULTS_ANONY_FOLDER'], anony_id))
    if user_result_folder_size>int(current_app.config['CUSTOM_RESULTS_FOLDER_SIZE_LIMIT']):
        flash ('Your result folder has too many files! Please delete some files first using Results Files tab at left nav bar.')
        return render_template('500.html') 
    if form.validate_on_submit() :
        session['input_type'] = str(form.input_type.data)
        session['numbermap'] = form.numbermap.data
        session['result_prefix'] = form.result_prefix.data
        session['bulk_design'] = form.bulk_design.data
        session['optional_email'] = form.email.data
        return redirect(url_for('main.analysis2', input_type=session.get('input_type'), numbermap=session.get('numbermap'), result_prefix=session.get('result_prefix'), bulk_design=session.get('bulk_design'),anony_id=anony_id))
    elif request.method == 'POST' and not form.validate():
        flash('Prefix must have only letters, numbers, dots or underscores.')
        return render_template('analysis1_anony.html', form=form,anony_id=anony_id)
    return render_template('analysis1_anony.html', form=form,anony_id=anony_id) 
    
@main.route('/<anony_id>/analysis2', methods=['GET', 'POST'])
def analysis2(anony_id):
    anony_id=session.get('anony_id')
    input_type=session.get('input_type')
    numbermap = int(session.get('numbermap'))  
    result_prefix=str(session.get('result_prefix'))
    bulk_design=int(session.get('bulk_design'))
    if bulk_design==2 and input_type=='Fastq':
        form = FastqForm2(request.form)
        if not form.is_submitted() :
            for i in range(1, numbermap+1):
                form.Bulkfastqfile.append_entry(i)  
    elif bulk_design==2 and input_type=='Bam':
        form = BamForm2(request.form)
        if not form.is_submitted() :
            for i in range(1, numbermap+1):
                form.Bulkbamfile.append_entry(i)   
    elif bulk_design==2 and input_type=='Map':  
        form = MapForm2(request.form) 
        if not form.is_submitted() :
            for i in range(1, numbermap+1):
                form.Bulkmapfile.append_entry(i)
            form.length_input.append_entry() 
    elif bulk_design==1 and input_type=='Fastq':
        form = FastqForm1(request.form)
        if not form.is_submitted() :
            for i in range(1, numbermap+1):
                form.Bulkfastqfile.append_entry(i) 
    elif bulk_design==1 and input_type=='Bam':
        form = BamForm1(request.form)  
        if not form.is_submitted() :
            for i in range(1, numbermap+1):
                form.Bulkbamfile.append_entry(i)
    else:  
        form = MapForm1(request.form) 
        if not form.is_submitted() :
            for i in range(1, numbermap+1):
                form.Bulkmapfile.append_entry()
            form.length_input.append_entry() 
    print "#######################" 
    print    form.data
    if form.validate_on_submit():
        if bulk_design==2 and input_type=='Bam':
#            print 'success from index2 submit'
#            print form.data.get('P1bamfile')
#            print dir(form.data.get('P1bamfile'))
#            print form.data.get('P1bamfile').fname
            P1bamfile=secure_filename(form.data.get('P1bamfile').fname)
            P2bamfile=secure_filename(form.data.get('P2bamfile').fname)
            Bulkbamfile1 = []
            Bulkbamfile2 = []                
            for i in range(0, numbermap):
#                print form.data.values()
#                print dir(form.data)
#                print form.data.get('Bulkbamfile')[0]['uploadfile1']
                Bulkbamfile1.append(secure_filename(form.data.get('Bulkbamfile')[i]['uploadfile1'].fname))
                Bulkbamfile2.append(secure_filename(form.data.get('Bulkbamfile')[i]['uploadfile2'].fname))
#                print Bulkbamfile1[i]
#                print Bulkbamfile2[i]
#            print form.data
            session['P1bamfile'] = P1bamfile
            session['P2bamfile'] = P2bamfile
            session['Bulkbamfile1'] = Bulkbamfile1
            session['Bulkbamfile2'] = Bulkbamfile2
            session['cov_depth']=int(form.cov_depth.data)
            session['kernel'] = form.kernel.data
            session['width'] = int(form.width.data)
            session['chrnum'] = form.chrnum.data
            session['noraw'] = form.noraw.data
            return redirect(url_for('main.analysis3',input_type=session.get('input_type'), numbermap=session.get('numbermap'), result_prefix=session.get('result_prefix'), bulk_design=session.get('bulk_design'),\
                                     P1bamfile=session.get('P1bamfile'),P2bamfile=session.get('P2bamfile'),Bulkbamfile1=session.get('Bulkbamfile1'),Bulkbamfile2=session.get('Bulkbamfile2'),\
                                     cov_depth=session.get('cov_depth'),kernel=session.get('kernel'),width=session.get('width'),chrnum=session.get('chrnum'),noraw=session.get('noraw'),anony_id=anony_id))
        elif bulk_design==1 and input_type=='Bam':
#            print 'success from index2 submit'
            P1bamfile=secure_filename(form.data.get('P1bamfile').fname)
            P2bamfile=secure_filename(form.data.get('P2bamfile').fname)
            Bulkbamfile1 = []              
            for i in range(0, numbermap):
#                print form.data.values()
#                print dir(form.data)
#                print form.data.get('Bulkbamfile')[0]['uploadfile1']
                Bulkbamfile1.append(secure_filename(form.data.get('Bulkbamfile')[i]['uploadfile1'].fname))
#                print Bulkbamfile1[i]
#            print form.data
            session['P1bamfile'] = P1bamfile
            session['P2bamfile'] = P2bamfile
            session['Bulkbamfile1'] = Bulkbamfile1
            session['cov_depth']=int(form.cov_depth.data)
            session['kernel'] = form.kernel.data
            session['width'] = int(form.width.data)
            session['chrnum'] = form.chrnum.data
            session['noraw'] = form.noraw.data
            return redirect(url_for('main.analysis3',input_type=session.get('input_type'), numbermap=session.get('numbermap'), result_prefix=session.get('result_prefix'), bulk_design=session.get('bulk_design'),\
                                     P1bamfile=session.get('P1bamfile'),P2bamfile=session.get('P2bamfile'),Bulkbamfile1=session.get('Bulkbamfile1'),\
                                     cov_depth=session.get('cov_depth'),kernel=session.get('kernel'),width=session.get('width'),chrnum=session.get('chrnum'),noraw=session.get('noraw'),anony_id=anony_id))
        elif bulk_design==2 and input_type=='Fastq':
#            print 'success from index2 submit'
            P1fastqfile=[]
            P2fastqfile=[]
            P1fastqfile.append(secure_filename(form.data.get('P1fastqfile').fname))
            if 'P1fastqfile2' in request.form:
                k=int(request.form['P1fastqfile2'])
                P1fastqfile.append(secure_filename(Files.query.filter_by(id=k).first().fname))
            P2fastqfile.append(secure_filename(form.data.get('P2fastqfile').fname))
            if 'P2fastqfile2' in request.form:
                k=int(request.form['P2fastqfile2'])
                P2fastqfile.append(secure_filename(Files.query.filter_by(id=k).first().fname))
            Bulkfastqfile1 = []
            Bulkfastqfile2 = []                
            for i in range(0, numbermap):
#                print form.data.values()
#                print dir(form.data)
                print form.data.get('Bulkfastqfile')[0]['uploadfile1']
                Bulkfastqfile1.append([secure_filename(form.data.get('Bulkfastqfile')[i]['uploadfile1'].fname)])
                if 'Bulkfastqfile-'+str(i)+'-uploadfile12' in request.form:
                    k=int(request.form['Bulkfastqfile-'+str(i)+'-uploadfile12'])
                    Bulkfastqfile1[i].append(secure_filename(Files.query.filter_by(id=k).first().fname))
                Bulkfastqfile2.append([secure_filename(form.data.get('Bulkfastqfile')[i]['uploadfile2'].fname)])
                if 'Bulkfastqfile-'+str(i)+'-uploadfile22' in request.form:
                    k=int(request.form['Bulkfastqfile-'+str(i)+'-uploadfile22'])
                    Bulkfastqfile2[i].append(secure_filename(Files.query.filter_by(id=k).first().fname))
                
#                print Bulkfastqfile1[i]
#                print Bulkfastqfile2[i]
#            print form.data
            session['P1fastqfile'] = P1fastqfile
            session['P2fastqfile'] = P2fastqfile
            session['Bulkfastqfile1'] = Bulkfastqfile1
            session['Bulkfastqfile2'] = Bulkfastqfile2
            session['readlen'] = int(form.readlen.data)
            session['phred'] = form.phred.data
            session['qccheck'] = form.qccheck.data
            session['trim'] = form.trim.data
            session['lastkeep'] = form.lastkeep.data
            session['rmadapt'] = form.rmadapt.data
            session['ladapter'] = form.ladapter.data
            session['radapter'] = form.radapter.data
            session['minlen'] = form.minlen.data
            session['removen'] = form.removen.data
            session['ncutoff'] = form.ncutoff.data
            session['filtq'] = form.filtq.data
            session['minq'] = form.minq.data
            session['pminq'] = form.pminq.data
            session['rmunpaired'] = form.rmunpaired.data
            session['qcstat'] = form.qcstat.data
            session['insertsize'] = form.insertsize.data
            session['windowsize'] = form.windowsize.data
            session['cov_depth']=int(form.cov_depth.data)
            session['kernel'] = form.kernel.data
            session['width'] = int(form.width.data)
            session['chrnum'] = form.chrnum.data
            session['noraw'] = form.noraw.data
            return redirect(url_for('main.analysis3',input_type=session.get('input_type'), numbermap=session.get('numbermap'), result_prefix=session.get('result_prefix'), bulk_design=session.get('bulk_design'),\
                                     P1fastqfile=session.get('P1fastqfile'),P2fastqfile=session.get('P2fastqfile'),Bulkfastqfile1=session.get('Bulkfastqfile1'),Bulkfastqfile2=session.get('Bulkfastqfile2'),\
                                     readlen=session.get('readlen'),phred=session.get('phred'),qccheck=session.get('qccheck'),trim=session.get('trim'),\
                                     lastkeep=session.get('lastkeep'),rmadapt=session.get('rmadapt'),ladapter=session.get('ladapter'),radapter=session.get('radapter'),\
                                     minlen=session.get('minlen'),removen=session.get('removen'),ncutoff=session.get('ncutoff'),filtq=session.get('filtq'),\
                                     minq=session.get('minq'),pminq=session.get('pminq'),rmunpaired=session.get('rmunpaired'),qcstat=session.get('qcstat'),\
                                     insertsize=session.get('insertsize'),windowsize=session.get('windowsize'),\
                                     cov_depth=session.get('cov_depth'),kernel=session.get('kernel'),width=session.get('width'),chrnum=session.get('chrnum'),noraw=session.get('noraw'),anony_id=anony_id))            
        elif bulk_design==1 and input_type=='Fastq':
#            print 'success from index2 submit'
            P1fastqfile=[]
            P2fastqfile=[]
            P1fastqfile.append(secure_filename(form.data.get('P1fastqfile').fname))
            if 'P1fastqfile2' in request.form:
                k=int(request.form['P1fastqfile2'])
                P1fastqfile.append(secure_filename(Files.query.filter_by(id=k).first().fname))            
            P2fastqfile.append(secure_filename(form.data.get('P2fastqfile').fname))
            if 'P2fastqfile2' in request.form:
                k=int(request.form['P2fastqfile2'])
                P2fastqfile.append(secure_filename(Files.query.filter_by(id=k).first().fname))            
            Bulkfastqfile1 = []              
            for i in range(0, numbermap):
#                print form.data.values()
#                print dir(form.data)
#                print form.data.get('Bulkfastqfile')[0]['uploadfile1']
                Bulkfastqfile1.append([secure_filename(form.data.get('Bulkfastqfile')[i]['uploadfile1'].fname)])
                if 'Bulkfastqfile-'+str(i)+'-uploadfile12' in request.form:
                    k=int(request.form['Bulkfastqfile-'+str(i)+'-uploadfile12'])
                    Bulkfastqfile1[i].append(secure_filename(Files.query.filter_by(id=k).first().fname))
#                print Bulkfastqfile1[i]
#            print form.data
            session['P1fastqfile'] = P1fastqfile
            session['P2fastqfile'] = P2fastqfile
            session['Bulkfastqfile1'] = Bulkfastqfile1
            session['readlen'] = int(form.readlen.data)
            session['phred'] = form.phred.data
            session['qccheck'] = form.qccheck.data
            session['trim'] = form.trim.data
            session['lastkeep'] = form.lastkeep.data
            session['rmadapt'] = form.rmadapt.data
            session['ladapter'] = form.ladapter.data
            session['radapter'] = form.radapter.data
            session['minlen'] = form.minlen.data
            session['removen'] = form.removen.data
            session['ncutoff'] = form.ncutoff.data
            session['filtq'] = form.filtq.data
            session['minq'] = form.minq.data
            session['pminq'] = form.pminq.data
            session['rmunpaired'] = form.rmunpaired.data
            session['qcstat'] = form.qcstat.data
            session['insertsize'] = form.insertsize.data
            session['windowsize'] = form.windowsize.data
            session['cov_depth']=int(form.cov_depth.data)
            session['kernel'] = form.kernel.data
            session['width'] = int(form.width.data)
            session['chrnum'] = form.chrnum.data
            session['noraw'] = form.noraw.data
            return redirect(url_for('main.analysis3',input_type=session.get('input_type'), numbermap=session.get('numbermap'), result_prefix=session.get('result_prefix'), bulk_design=session.get('bulk_design'),\
                                     P1fastqfile=session.get('P1fastqfile'),P2fastqfile=session.get('P2fastqfile'),Bulkfastqfile1=session.get('Bulkfastqfile1'),\
                                     readlen=session.get('readlen'),phred=session.get('phred'),qccheck=session.get('qccheck'),trim=session.get('trim'),\
                                     lastkeep=session.get('lastkeep'),rmadapt=session.get('rmadapt'),ladapter=session.get('ladapter'),radapter=session.get('radapter'),\
                                     minlen=session.get('minlen'),removen=session.get('removen'),ncutoff=session.get('ncutoff'),filtq=session.get('filtq'),\
                                     minq=session.get('minq'),pminq=session.get('pminq'),rmunpaired=session.get('rmunpaired'),qcstat=session.get('qcstat'),\
                                     insertsize=session.get('insertsize'),windowsize=session.get('windowsize'),\
                                     cov_depth=session.get('cov_depth'),kernel=session.get('kernel'),width=session.get('width'),chrnum=session.get('chrnum'),noraw=session.get('noraw'),anony_id=anony_id))            
        elif bulk_design==2 and input_type=='Map':
#            print 'success from index2 submit' 
            Bulkmapfile1 = []
            Bulkmapfile2 = []                
            for i in range(0, numbermap):
#                print form.data.values()
#                print form.data.get('Bulkmapfile')[0]['uploadfile1']
                Bulkmapfile1.append(secure_filename(form.data.get('Bulkmapfile')[i]['uploadfile1'].fname))
                Bulkmapfile2.append(secure_filename(form.data.get('Bulkmapfile')[i]['uploadfile2'].fname))
#                print Bulkmapfile1[i]
#                print Bulkmapfile2[i]
#            print form.data
            session['Bulkmapfile1'] = Bulkmapfile1
            session['Bulkmapfile2'] = Bulkmapfile2
            session['kernel'] = form.kernel.data
            session['width'] = int(form.width.data)
            session['chrnum'] = form.chrnum.data
            session['noraw'] = form.noraw.data
            session['length_input'] = secure_filename(form.data.get('length_input')[0]['uploadLengthfile'].fname)
            print "!!!!!!!!!!!!!!!!!!!!!!!!!"
            return redirect(url_for('main.analysis3',input_type=session.get('input_type'), numbermap=session.get('numbermap'), result_prefix=session.get('result_prefix'), bulk_design=session.get('bulk_design'),\
                                     Bulkbamfile1=session.get('Bulkbamfile1'),Bulkbamfile2=session.get('Bulkbamfile2'),\
                                     kernel=session.get('kernel'),width=session.get('width'),chrnum=session.get('chrnum'),noraw=session.get('noraw'),length_input=session.get('length_input'),anony_id=anony_id))
        else:
#            print 'success from index2 submit'
            Bulkmapfile1 = []              
            for i in range(0, numbermap):
#                print form.data.values()
#                print dir(form.data)
#                print form.data.get('Bulkmapfile')[0]['uploadfile1']
                Bulkmapfile1.append(secure_filename(form.data.get('Bulkmapfile')[i]['uploadfile1'].fname))
#                print Bulkmapfile1[i]
#            print form.data
            session['Bulkmapfile1'] = Bulkmapfile1
            session['kernel'] = form.kernel.data
            session['width'] = int(form.width.data)
            session['chrnum'] = form.chrnum.data
            session['noraw'] = form.noraw.data
            session['length_input'] = secure_filename(form.data.get('length_input')[0]['uploadLengthfile'].fname)
            return redirect(url_for('main.analysis3',input_type=session.get('input_type'), numbermap=session.get('numbermap'), result_prefix=session.get('result_prefix'), bulk_design=session.get('bulk_design'),\
                                     Bulkbamfile1=session.get('Bulkbamfile1'),\
                                     kernel=session.get('kernel'),width=session.get('width'),chrnum=session.get('chrnum'),noraw=session.get('noraw'),length_input=session.get('length_input'),anony_id=anony_id))
    elif request.method == 'POST' and not form.validate():
        flash('Incorrect input format.')
        return render_template('analysis2_anony.html', form=form,input_type=session.get('input_type'),numbermap=session.get('numbermap'),result_prefix=session.get('result_prefix'),bulk_design=session.get('bulk_design'),anony_id=anony_id)
    return render_template('analysis2_anony.html', form=form,input_type=session.get('input_type'),numbermap=session.get('numbermap'),result_prefix=session.get('result_prefix'),bulk_design=session.get('bulk_design'),anony_id=anony_id)
     
@main.route('/<anony_id>/analysis3', methods=[ 'GET', 'POST'])
def analysis3(anony_id):
    anony_id=session.get('anony_id')
    form = SubmitForm(request.form)
    input_type=session.get('input_type')
    numbermap = int(session.get('numbermap'))  
    result_prefix=str(session.get('result_prefix'))
    bulk_design=int(session.get('bulk_design'))
    if bulk_design==2 and input_type=='Bam':
        P1bamfile=session.get('P1bamfile')
        P2bamfile=session.get('P2bamfile')
        Bulkbamfile1=session.get('Bulkbamfile1')
        Bulkbamfile2=session.get('Bulkbamfile2')
        cov_depth=session.get('cov_depth')
        kernel=session.get('kernel')
        width=session.get('width')
        chrnum=session.get('chrnum')
        noraw=session.get('noraw')
    elif bulk_design==1 and input_type=='Bam':
        P1bamfile=session.get('P1bamfile')
        P2bamfile=session.get('P2bamfile')
        Bulkbamfile1=session.get('Bulkbamfile1')
        cov_depth=session.get('cov_depth')
        kernel=session.get('kernel')
        width=session.get('width')
        chrnum=session.get('chrnum')
        noraw=session.get('noraw')        
    elif bulk_design==2 and input_type=='Fastq':
        P1fastqfile=session.get('P1fastqfile')
        P2fastqfile=session.get('P2fastqfile')
        Bulkfastqfile1=session.get('Bulkfastqfile1')
        Bulkfastqfile2=session.get('Bulkfastqfile2')
        readlen=session.get('readlen')
        phred=session.get('phred')
        qccheck=session.get('qccheck')
        trim=session.get('trim')
        lastkeep=session.get('lastkeep')
        rmadapt=session.get('rmadapt')
        ladapter=session.get('ladapter')
        radapter=session.get('radapter')
        minlen=session.get('minlen')
        removen=session.get('removen')
        ncutoff=session.get('ncutoff')
        filtq=session.get('filtq')
        minq=session.get('minq')
        pminq=session.get('pminq')
        rmunpaired=session.get('rmunpaired')
        qcstat=session.get('qcstat')
        insertsize=session.get('insertsize')
        windowsize=session.get('windowsize')
        cov_depth=session.get('cov_depth')
        kernel=session.get('kernel')
        width=session.get('width')
        chrnum=session.get('chrnum')
        noraw=session.get('noraw')
    elif bulk_design==1 and input_type=='Fastq':
        P1fastqfile=session.get('P1fastqfile')
        P2fastqfile=session.get('P2fastqfile')
        Bulkfastqfile1=session.get('Bulkfastqfile1')
        readlen=session.get('readlen')
        phred=session.get('phred')
        qccheck=session.get('qccheck')
        trim=session.get('trim')
        lastkeep=session.get('lastkeep')
        rmadapt=session.get('rmadapt')
        ladapter=session.get('ladapter')
        radapter=session.get('radapter')
        minlen=session.get('minlen')
        removen=session.get('removen')
        ncutoff=session.get('ncutoff')
        filtq=session.get('filtq')
        minq=session.get('minq')
        pminq=session.get('pminq')
        rmunpaired=session.get('rmunpaired')
        qcstat=session.get('qcstat')
        insertsize=session.get('insertsize')
        windowsize=session.get('windowsize')
        cov_depth=session.get('cov_depth')
        kernel=session.get('kernel')
        width=session.get('width')
        chrnum=session.get('chrnum')
        noraw=session.get('noraw')
    elif bulk_design==2 and input_type=='Map':
        Bulkmapfile1=session.get('Bulkmapfile1')
        Bulkmapfile2=session.get('Bulkmapfile2')
        kernel=session.get('kernel')
        length_input=session.get('length_input')
        width=session.get('width')
        chrnum=session.get('chrnum')
        noraw=session.get('noraw')
    else:
        Bulkmapfile1=session.get('Bulkmapfile1')
        kernel=session.get('kernel')
        length_input=session.get('length_input')
        width=session.get('width')
        chrnum=session.get('chrnum')
        noraw=session.get('noraw')
    if chrnum == 'all':
        chrnum = 0
    else:
        chrnum = int(chrnum)
    noraw_label = ''
    if noraw == 'yes':
        noraw_label = 'yes'
    else:
        noraw_label = ''
#    if form.is_submitted():
#        print "submitted"
#    if form.validate():
#        print "valid"
#    print form.errors
    if form.validate_on_submit() :
#        print 'success from upload submit'
        token = str(uuid.uuid4())
        output_folder = os.path.join(current_app.config['RESULTS_ANONY_FOLDER'],anony_id)
        if input_type=='Bam':
            P1bamfilelist=os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'],anony_id,P1bamfile)
            P2bamfilelist=os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'],anony_id,P2bamfile)
            Bulkbamfile1list = []
            if bulk_design==2:
                Bulkbamfile2list = []
            for i in Bulkbamfile1:
                Bulkbamfile1list.append(os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'],anony_id, i))
            if bulk_design==2:
                for i in Bulkbamfile2:
                    Bulkbamfile2list.append(os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'],anony_id, i))
            if bulk_design==2:
                Hbamlist = ' '.join([i for i in Bulkbamfile1list])
                Lbamlist = ' '.join([i for i in Bulkbamfile2list])
                session['Lbamlist'] = Lbamlist
                session['Hbamlist'] = Hbamlist
            else:
                bamlist = ' '.join([i for i in Bulkbamfile1list])
                session['bamlist'] = bamlist
        elif input_type=='Fastq':
#            P1fastqfilelist=os.path.join(current_app.config['UPLOADS_FOLDER'],current_user.email,P1fastqfile)
            P1fastqfilelist=[os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'],anony_id,i) for i in P1fastqfile]
#            P2fastqfilelist=os.path.join(current_app.config['UPLOADS_FOLDER'],current_user.email,P2fastqfile)
            P2fastqfilelist=[os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'],anony_id,i) for i in P2fastqfile]
            Bulkfastqfile1list = []
            if bulk_design==2:
                Bulkfastqfile2list = []
            for i in range(len(Bulkfastqfile1)):
#                Bulkfastqfile1list.append(os.path.join(current_app.config['UPLOADS_FOLDER'],current_user.email, i))
                print "-----------------------",i
                Bulkfastqfile1list.append([os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'],anony_id, j) for j in Bulkfastqfile1[i]])
            print "-----------------------",Bulkfastqfile1list
            if bulk_design==2:
                for i in range(len(Bulkfastqfile2)):
#                    Bulkfastqfile2list.append(os.path.join(current_app.config['UPLOADS_FOLDER'],current_user.email, i))
                    Bulkfastqfile2list.append([os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'],anony_id, j) for j in Bulkfastqfile2[i]])
            if bulk_design==2:
#                Hfastqlist = ' '.join([i for i in Bulkfastqfile1list])
#                Lfastqlist = ' '.join([i for i in Bulkfastqfile2list])
#                session['Lfastqlist'] = Lfastqlist
#                session['Hfastqlist'] = Hfastqlist
                Hfastqlist=Bulkfastqfile1list
                Lfastqlist =Bulkfastqfile2list
                session['Lfastqlist'] = Lfastqlist
                session['Hfastqlist'] = Hfastqlist
                print "###############",Hfastqlist
                print "###############",Lfastqlist
            else:
#                fastqlist = ' '.join([i for i in Bulkfastqfile1list])
#                session['fastqlist'] = fastqlist
                fastqlist = Bulkfastqfile1list
                session['fastqlist'] = fastqlist
        else:
            Bulkmapfile1list = []
            if bulk_design==2:
                Bulkmapfile2list = []
            for i in Bulkmapfile1:
                Bulkmapfile1list.append(os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'],anony_id, i))
            if bulk_design==2:
                for i in Bulkmapfile2:
                    Bulkmapfile2list.append(os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'],anony_id, i))
            if bulk_design==2:
                Hmaplist = ' '.join([i for i in Bulkmapfile1list])
                Lmaplist = ' '.join([i for i in Bulkmapfile2list])
                session['Lmaplist'] = Lmaplist
                session['Hmaplist'] = Hmaplist
            else:
                maplist = ' '.join([i for i in Bulkmapfile1list])
                session['maplist'] = maplist
            length_input_file=os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'],anony_id, length_input)
            session['length_input_file'] = length_input_file
        session['token'] = token
        optional_email=session.get('optional_email')
        if bulk_design==2 and input_type=='Bam':
            task_db=Tasks(owners=anony_id,owners_name='Anonymous',token_id=token,status='Running')
            task_db.create_time=datetime.utcnow()
            db.session.add(task_db)
            db.session.commit()
            return redirect(url_for('main.longtask',output_folder=output_folder,url_server=url_for('main.index_anony',anony_id=session.get('anony_id'),_external=True),token=token,\
            input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
            P1bamfile=P1bamfilelist,\
            P2bamfile=P2bamfilelist,\
            Hbamlist=Hbamlist,\
            Lbamlist=Lbamlist, \
            cov_depth=cov_depth,\
            kernel=kernel,\
            width=width,\
            chrnum=chrnum,\
            noraw=noraw,anony_id=anony_id,optional_email=optional_email))
        elif bulk_design==1 and input_type=='Bam':
            task_db=Tasks(owners=anony_id,owners_name='Anonymous',token_id=token,status='Running')
            task_db.create_time=datetime.utcnow()
            db.session.add(task_db)
            db.session.commit()
            return redirect(url_for('main.longtask',output_folder=output_folder,url_server=url_for('main.index_anony',anony_id=session.get('anony_id'),_external=True),token=token,\
            input_type=input_type,numbermap = int(numbermap)  ,result_prefix=result_prefix,bulk_design=int(bulk_design),\
            P1bamfile=P1bamfilelist,\
            P2bamfile=P2bamfilelist,\
            bamlist=bamlist,\
            cov_depth=int(cov_depth),\
            kernel=kernel,\
            width=int(width),\
            chrnum=chrnum,\
            noraw=noraw,anony_id=anony_id,optional_email=optional_email))
        elif bulk_design==2 and input_type=='Fastq':
            task_db=Tasks(owners=anony_id,owners_name='Anonymous',token_id=token,status='Running')
            task_db.create_time=datetime.utcnow()
            db.session.add(task_db)
            db.session.commit()
            print "!!!!!!!!!!!!!!",Hfastqlist
            print "!!!!!!!!!!!!!",Lfastqlist
            return redirect(url_for('main.longtask', output_folder=output_folder,url_server=url_for('main.index_anony',anony_id=session.get('anony_id'),_external=True),token=token,input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
            P1fastqfile=json.dumps(P1fastqfilelist),\
            P2fastqfile=json.dumps(P2fastqfilelist),\
            Hfastqlist=json.dumps(Hfastqlist),\
            Lfastqlist=json.dumps(Lfastqlist),\
            readlen=readlen,\
            phred=phred,\
            qccheck=qccheck,\
            trim=trim,\
            lastkeep=lastkeep,\
            rmadapt=rmadapt,\
            ladapter=ladapter,\
            radapter=radapter,\
            minlen=minlen,\
            removen=removen,\
            ncutoff=ncutoff,\
            filtq=filtq,\
            minq=minq,\
            pminq=pminq,\
            rmunpaired=rmunpaired,\
            qcstat=qcstat,\
            insertsize=insertsize,\
            windowsize=windowsize,\
            cov_depth=cov_depth,\
            kernel=kernel,\
            width=width,\
            chrnum=chrnum,\
            noraw=noraw,anony_id=anony_id,optional_email=optional_email\
            ))
        elif bulk_design==1 and input_type=='Fastq':
            task_db=Tasks(owners=anony_id,owners_name='Anonymous',token_id=token,status='Running')
            task_db.create_time=datetime.utcnow()
            db.session.add(task_db)
            db.session.commit()
            return redirect(url_for('main.longtask',output_folder=output_folder,url_server=url_for('main.index_anony',anony_id=session.get('anony_id'),_external=True),token=token, input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
            P1fastqfile=json.dumps(P1fastqfilelist),\
            P2fastqfile=json.dumps(P2fastqfilelist),\
            fastqlist=json.dumps(fastqlist),\
            readlen=readlen,\
            phred=phred,\
            qccheck=qccheck,\
            trim=trim,\
            lastkeep=lastkeep,\
            rmadapt=rmadapt,\
            ladapter=ladapter,\
            radapter=radapter,\
            minlen=minlen,\
            removen=removen,\
            ncutoff=ncutoff,\
            filtq=filtq,\
            minq=minq,\
            pminq=pminq,\
            rmunpaired=rmunpaired,\
            qcstat=qcstat,\
            insertsize=insertsize,\
            windowsize=windowsize,\
            cov_depth=cov_depth,\
            kernel=kernel,\
            width=width,\
            chrnum=chrnum,\
            noraw=noraw,anony_id=anony_id,optional_email=optional_email\
            ))
        elif bulk_design==2 and input_type=='Map':
            task_db=Tasks(owners=anony_id,owners_name='Anonymous',token_id=token,status='Running')
            task_db.create_time=datetime.utcnow()
            db.session.add(task_db)
            db.session.commit()
            return redirect(url_for('main.longtask', output_folder=output_folder,url_server=url_for('main.index_anony',anony_id=session.get('anony_id'),_external=True),token=token,input_type=input_type,numbermap = numbermap ,result_prefix=result_prefix,bulk_design=bulk_design,\
            Bulkmapfile1=Bulkmapfile1,\
            Bulkmapfile2=Bulkmapfile2,\
            kernel=kernel,\
            length_input=length_input,\
            width=width,\
            chrnum=chrnum,\
            noraw=noraw,anony_id=anony_id,optional_email=optional_email,\
            length_input_file=length_input_file\
            ))
        else:
            task_db=Tasks(owners=anony_id,owners_name='Anonymous',token_id=token,status='Running')
            task_db.create_time=datetime.utcnow()
            db.session.add(task_db)
            db.session.commit()
            return redirect(url_for('main.longtask',output_folder=output_folder,url_server=url_for('main.index_anony',anony_id=session.get('anony_id'),_external=True),token=token, input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
            Bulkmapfile1=Bulkmapfile1,\
            kernel=kernel,\
            length_input=length_input,\
            width=width,\
            chrnum=chrnum,\
            noraw=noraw,anony_id=anony_id,optional_email=optional_email,\
            length_input_file=length_input_file\
            ))
    elif request.method == 'POST' and not form.validate():
        flash('Incorrect input format.')
        if bulk_design==2 and input_type=='Bam':
            return render_template('analysis3_anony.html', form=form, input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
        P1bamfile=P1bamfile,\
        P2bamfile=P2bamfile,\
        Bulkbamfile1=Bulkbamfile1,\
        Bulkbamfile2=Bulkbamfile2,\
        cov_depth=cov_depth,\
        kernel=kernel,\
        width=width,\
        chrnum=chrnum,\
        noraw=noraw,anony_id=anony_id)
        elif bulk_design==1 and input_type=='Bam':
            return render_template('analysis3_anony.html', form=form, input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
        P1bamfile=P1bamfile,\
        P2bamfile=P2bamfile,\
        Bulkbamfile1=Bulkbamfile1,\
        cov_depth=cov_depth,\
        kernel=kernel,\
        width=width,\
        chrnum=chrnum,\
        noraw=noraw,anony_id=anony_id)
        elif bulk_design==2 and input_type=='Fastq':
            return render_template('analysis3_anony.html', form=form, input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
        P1fastqfile=P1fastqfile,\
        P2fastqfile=P2fastqfile,\
        Bulkfastqfile1=Bulkfastqfile1,\
        Bulkfastqfile2=Bulkfastqfile2,\
        readlen=readlen,\
        phred=phred,\
        qccheck=qccheck,\
        trim=trim,\
        lastkeep=lastkeep,\
        rmadapt=rmadapt,\
        ladapter=ladapter,\
        radapter=radapter,\
        minlen=minlen,\
        removen=removen,\
        ncutoff=ncutoff,\
        filtq=filtq,\
        minq=minq,\
        pminq=pminq,\
        rmunpaired=rmunpaired,\
        qcstat=qcstat,\
        insertsize=insertsize,\
        windowsize=windowsize,\
        cov_depth=cov_depth,\
        kernel=kernel,\
        width=width,\
        chrnum=chrnum,\
        noraw=noraw,anony_id=anony_id\
        )        
        elif bulk_design==1 and input_type=='Fastq':
            return render_template('analysis3_anony.html', form=form, input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
        P1fastqfile=P1fastqfile,\
        P2fastqfile=P2fastqfile,\
        Bulkfastqfile1=Bulkfastqfile1,\
        readlen=readlen,\
        phred=phred,\
        qccheck=qccheck,\
        trim=trim,\
        lastkeep=lastkeep,\
        rmadapt=rmadapt,\
        ladapter=ladapter,\
        radapter=radapter,\
        minlen=minlen,\
        removen=removen,\
        ncutoff=ncutoff,\
        filtq=filtq,\
        minq=minq,\
        pminq=pminq,\
        rmunpaired=rmunpaired,\
        qcstat=qcstat,\
        insertsize=insertsize,\
        windowsize=windowsize,\
        cov_depth=cov_depth,\
        kernel=kernel,\
        width=width,\
        chrnum=chrnum,\
        noraw=noraw,anony_id=anony_id\
        )
        elif bulk_design==2 and input_type=='Map':
            return render_template('analysis3_anony.html', form=form, input_type=input_type,numbermap = numbermap ,result_prefix=result_prefix,bulk_design=bulk_design,\
        Bulkmapfile1=Bulkmapfile1,\
        Bulkmapfile2=Bulkmapfile2,\
        kernel=kernel,\
        length_input=length_input,\
        width=width,\
        chrnum=chrnum,\
        noraw=noraw,anony_id=anony_id\
        )
        else:
            return render_template('analysis3_anony.html', form=form, input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
        Bulkmapfile1=Bulkmapfile1,\
        kernel=kernel,\
        length_input=length_input,\
        width=width,\
        chrnum=chrnum,\
        noraw=noraw,anony_id=anony_id\
        )
    if bulk_design==2 and input_type=='Bam':
        return render_template('analysis3_anony.html', form=form, input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
        P1bamfile=P1bamfile,\
        P2bamfile=P2bamfile,\
        Bulkbamfile1=Bulkbamfile1,\
        Bulkbamfile2=Bulkbamfile2,\
        cov_depth=cov_depth,\
        kernel=kernel,\
        width=width,\
        chrnum=chrnum,\
        noraw=noraw,anony_id=anony_id)
    elif bulk_design==1 and input_type=='Bam':
        return render_template('analysis3_anony.html', form=form, input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
        P1bamfile=P1bamfile,\
        P2bamfile=P2bamfile,\
        Bulkbamfile1=Bulkbamfile1,\
        cov_depth=cov_depth,\
        kernel=kernel,\
        width=width,\
        chrnum=chrnum,\
        noraw=noraw,anony_id=anony_id)
    elif bulk_design==2 and input_type=='Fastq':
        return render_template('analysis3_anony.html', form=form, input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
        P1fastqfile=P1fastqfile,\
        P2fastqfile=P2fastqfile,\
        Bulkfastqfile1=Bulkfastqfile1,\
        Bulkfastqfile2=Bulkfastqfile2,\
        readlen=readlen,\
        phred=phred,\
        qccheck=qccheck,\
        trim=trim,\
        lastkeep=lastkeep,\
        rmadapt=rmadapt,\
        ladapter=ladapter,\
        radapter=radapter,\
        minlen=minlen,\
        removen=removen,\
        ncutoff=ncutoff,\
        filtq=filtq,\
        minq=minq,\
        pminq=pminq,\
        rmunpaired=rmunpaired,\
        qcstat=qcstat,\
        insertsize=insertsize,\
        windowsize=windowsize,\
        cov_depth=cov_depth,\
        kernel=kernel,\
        width=width,\
        chrnum=chrnum,\
        noraw=noraw,anony_id=anony_id\
        )        
    elif bulk_design==1 and input_type=='Fastq':
        return render_template('analysis3_anony.html', form=form, input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
        P1fastqfile=P1fastqfile,\
        P2fastqfile=P2fastqfile,\
        Bulkfastqfile1=Bulkfastqfile1,\
        readlen=readlen,\
        phred=phred,\
        qccheck=qccheck,\
        trim=trim,\
        lastkeep=lastkeep,\
        rmadapt=rmadapt,\
        ladapter=ladapter,\
        radapter=radapter,\
        minlen=minlen,\
        removen=removen,\
        ncutoff=ncutoff,\
        filtq=filtq,\
        minq=minq,\
        pminq=pminq,\
        rmunpaired=rmunpaired,\
        qcstat=qcstat,\
        insertsize=insertsize,\
        windowsize=windowsize,\
        cov_depth=cov_depth,\
        kernel=kernel,\
        width=width,\
        chrnum=chrnum,\
        noraw=noraw,anony_id=anony_id\
        )
    elif bulk_design==2 and input_type=='Map':
        return render_template('analysis3_anony.html', form=form, input_type=input_type,numbermap = numbermap ,result_prefix=result_prefix,bulk_design=bulk_design,\
        Bulkmapfile1=Bulkmapfile1,\
        Bulkmapfile2=Bulkmapfile2,\
        kernel=kernel,\
        length_input=length_input,\
        width=width,\
        chrnum=chrnum,\
        noraw=noraw,anony_id=anony_id\
        )
    else:
        return render_template('analysis3_anony.html', form=form, input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
        Bulkmapfile1=Bulkmapfile1,\
        kernel=kernel,\
        length_input=length_input,\
        width=width,\
        chrnum=chrnum,\
        noraw=noraw,anony_id=anony_id\
        )

@main.route('/<anony_id>/longtask', methods=['GET', 'POST'])
def longtask(anony_id):
    anony_id=session.get('anony_id')
    params = request.args.items()
    print "@@@@@@@@@@@@@@@@@@@@@@@@",params
    for i, j in params:
        session[i] = j.encode("ascii")
    if int(session.get('bulk_design'))==2 and session.get('input_type')=='Bam' :
        output_folder=session.get('output_folder')
        url_server=session.get('url_server')
        token=session.get('token')
#        task_id=session.get('task_id')
        input_type=session.get('input_type')
        numbermap = int(session.get('numbermap'))
        result_prefix=session.get('result_prefix')
        bulk_design=int(session.get('bulk_design'))
        P1bamfile=session.get('P1bamfile')
        P2bamfile=session.get('P2bamfile')
        Hbamlist=session.get('Hbamlist')
        Lbamlist=session.get('Lbamlist')
        cov_depth=int(session.get('cov_depth'))
        kernel=session.get('kernel')
        width=int(session.get('width'))
        chrnum=session.get('chrnum')
        noraw=session.get('noraw')
        print "*************",Hbamlist
        print "*************",Lbamlist
        optional_email=session.get('optional_email')
        task = long_task_bam2_anony.apply_async(args=[output_folder,url_server,token,input_type,numbermap,result_prefix,bulk_design,P1bamfile,P2bamfile,Hbamlist,Lbamlist,cov_depth,kernel,width,chrnum,noraw,optional_email],task_id=session.get('token'))
        session['input_type'] = input_type
        session['bulk_design'] = bulk_design
        return render_template('waiting_anony.html',task_id=session.get('token'),token=session.get('token'),anony_id=session.get('anony_id'))
    elif int(session.get('bulk_design'))==1 and session.get('input_type')=='Bam' :
        output_folder=session.get('output_folder')
        url_server=session.get('url_server')
        token=session.get('token')
#        task_id=session.get('task_id')
        input_type=session.get('input_type')
        numbermap = int(session.get('numbermap') )
        result_prefix=session.get('result_prefix')
        bulk_design=int(session.get('bulk_design'))
        P1bamfile=session.get('P1bamfile')
        P2bamfile=session.get('P2bamfile')
        bamlist=session.get('bamlist')
        cov_depth=int(session.get('cov_depth'))
        kernel=session.get('kernel')
        width=int(session.get('width'))
        chrnum=session.get('chrnum')
        noraw=session.get('noraw')
        optional_email=session.get('optional_email')
        task = long_task_bam1_anony.apply_async(args=[output_folder,url_server,token,input_type,numbermap,result_prefix,bulk_design,P1bamfile,P2bamfile,bamlist,cov_depth,kernel,width,chrnum,noraw,optional_email],task_id=session.get('token'))
#        session['task_id'] = task.id
        session['input_type'] = input_type
        session['bulk_design'] = bulk_design
        return render_template('waiting_anony.html',task_id=session.get('token'),token=session.get('token'),anony_id=session.get('anony_id'))
    elif int(session.get('bulk_design'))==2 and session.get('input_type')=='Fastq' :
        output_folder=session.get('output_folder')
        url_server=session.get('url_server')
        token=session.get('token')
#        task_id=session.get('task_id')
        input_type=session.get('input_type')
        numbermap = int(session.get('numbermap')) 
        result_prefix=session.get('result_prefix')
        bulk_design=session.get('bulk_design')
        P1fastqfile=session.get('P1fastqfile')
        P2fastqfile=session.get('P2fastqfile')
        Hfastqlist=session.get('Hfastqlist')
        Lfastqlist=session.get('Lfastqlist')
        readlen=session.get('readlen')
        phred=session.get('phred')
        qccheck=session.get('qccheck')
        trim=session.get('trim')
        lastkeep=session.get('lastkeep')
        rmadapt=session.get('rmadapt')
        ladapter=session.get('ladapter')
        radapter=session.get('radapter')
        minlen=session.get('minlen')
        removen=session.get('removen')
        ncutoff=session.get('ncutoff')
        filtq=session.get('filtq')
        minq=session.get('minq')
        pminq=session.get('pminq')
        rmunpaired=session.get('rmunpaired')
        qcstat=session.get('qcstat')
        insertsize=session.get('insertsize')
        windowsize=session.get('windowsize')
        cov_depth=int(session.get('cov_depth'))
        kernel=session.get('kernel')
        width=int(session.get('width'))
        chrnum=session.get('chrnum')
        noraw=session.get('noraw')
        optional_email=session.get('optional_email')
        task = long_task_fastq2_anony.apply_async(args=[output_folder,url_server,token,input_type,numbermap,result_prefix,bulk_design,P1fastqfile,P2fastqfile,Hfastqlist,Lfastqlist,readlen,phred,qccheck,trim,lastkeep,rmadapt,ladapter,radapter,minlen,removen,ncutoff,filtq,minq,pminq,rmunpaired,qcstat,insertsize,windowsize,cov_depth,kernel,width,chrnum,noraw,optional_email],task_id=session.get('token'))
#        session['task_id'] = task.id
        session['input_type'] = input_type
        session['bulk_design'] = bulk_design
        return render_template('waiting_anony.html',task_id=session.get('token'),token=session.get('token'),anony_id=session.get('anony_id'))
    elif int(session.get('bulk_design'))==1 and session.get('input_type')=='Fastq' :
        output_folder=session.get('output_folder')
        url_server=session.get('url_server')
        token=session.get('token')
#        task_id=session.get('task_id')
        input_type=session.get('input_type')
        numbermap = int(session.get('numbermap') )
        result_prefix=session.get('result_prefix')
        bulk_design=int(session.get('bulk_design'))
        P1fastqfile=session.get('P1fastqfile')
        P2fastqfile=session.get('P2fastqfile')
        fastqlist=session.get('fastqlist')
        readlen=session.get('readlen')
        phred=session.get('phred')
        qccheck=session.get('qccheck')
        trim=session.get('trim')
        lastkeep=session.get('lastkeep')
        rmadapt=session.get('rmadapt')
        ladapter=session.get('ladapter')
        radapter=session.get('radapter')
        minlen=session.get('minlen')
        removen=session.get('removen')
        ncutoff=session.get('ncutoff')
        filtq=session.get('filtq')
        minq=session.get('minq')
        pminq=session.get('pminq')
        rmunpaired=session.get('rmunpaired')
        qcstat=session.get('qcstat')
        insertsize=session.get('insertsize')
        windowsize=session.get('windowsize')
        cov_depth=int(session.get('cov_depth'))
        kernel=session.get('kernel')
        width=int(session.get('width'))
        chrnum=session.get('chrnum')
        noraw=session.get('noraw')
        optional_email=session.get('optional_email')
        task = long_task_fastq1_anony.apply_async(args=[output_folder,url_server,token,input_type,numbermap,result_prefix,bulk_design,P1fastqfile,P2fastqfile,fastqlist,readlen,phred,qccheck,trim,lastkeep,rmadapt,ladapter,radapter,minlen,removen,ncutoff,filtq,minq,pminq,rmunpaired,qcstat,insertsize,windowsize,cov_depth,kernel,width,chrnum,noraw,optional_email],task_id=session.get('token'))
#        session['task_id'] = task.id
        session['input_type'] = input_type
        session['bulk_design'] = bulk_design
        return render_template('waiting_anony.html',task_id=session.get('token'),token=session.get('token'),anony_id=session.get('anony_id'))
    elif int(session.get('bulk_design'))==2 and session.get('input_type')=='Map' :
        output_folder=session.get('output_folder')
        url_server=session.get('url_server')
        token=session.get('token')
 #       task_id=session.get('task_id')
        input_type=session.get('input_type')
        numbermap = int(session.get('numbermap')) 
        result_prefix=session.get('result_prefix')
        bulk_design=int(session.get('bulk_design'))
        Hmaplist=session.get('Hmaplist')
        Lmaplist=session.get('Lmaplist')
        kernel=session.get('kernel')
        length_input_file=session.get('length_input_file')
        width=int(session.get('width'))
        chrnum=session.get('chrnum')
        noraw=session.get('noraw')
        optional_email=session.get('optional_email')
        task = long_task_map2_anony.apply_async(args=[output_folder,url_server,token,input_type,numbermap,result_prefix,bulk_design,Hmaplist,Lmaplist,kernel,width,chrnum,noraw,optional_email,length_input_file],task_id=session.get('token'))
#        session['task_id'] = task.id
        session['input_type'] = input_type
        session['bulk_design'] = bulk_design
        return render_template('waiting_anony.html',task_id=session.get('token'),token=session.get('token'),anony_id=session.get('anony_id'))
    else :
        output_folder=session.get('output_folder')
        url_server=session.get('url_server')
        token=session.get('token')
 #       task_id=session.get('task_id')
        input_type=session.get('input_type')
        numbermap = int(session.get('numbermap')) 
        result_prefix=session.get('result_prefix')
        bulk_design=int(session.get('bulk_design'))
        maplist=session.get('maplist')
        kernel=session.get('kernel')
        length_input_file=session.get('length_input_file')
        width=int(session.get('width'))
        chrnum=session.get('chrnum')
        noraw=session.get('noraw')
        optional_email=session.get('optional_email')
        task = long_task_map1_anony.apply_async(args=[output_folder,url_server,token,input_type,numbermap,result_prefix,bulk_design,maplist,kernel,width,chrnum,noraw,optional_email,length_input_file],task_id=session.get('token'))
#        session['task_id'] = task.id
        session['input_type'] = input_type
        session['bulk_design'] = bulk_design
        return render_template('waiting_anony.html',task_id=session.get('token'),token=session.get('token'),anony_id=session.get('anony_id'))
    
@main.route('/<anony_id>/waiting/<task_id>', methods=['GET', 'POST'])
def waiting(task_id,anony_id):
    anony_id=session.get('anony_id')
    if int(session.get('bulk_design'))==2 and session.get('input_type')=='Bam' :
        task = long_task_bam2_anony.AsyncResult(task_id)
    elif int(session.get('bulk_design'))==1 and session.get('input_type')=='Bam' :
        task = long_task_bam1_anony.AsyncResult(task_id)
    elif int(session.get('bulk_design'))==2 and session.get('input_type')=='Fastq' :
        task = long_task_fastq2_anony.AsyncResult(task_id)
    elif int(session.get('bulk_design'))==1 and session.get('input_type')=='Fastq' :
        task = long_task_fastq1_anony.AsyncResult(task_id)
    elif int(session.get('bulk_design'))==2 and session.get('input_type')=='Map' :
        task = long_task_map2_anony.AsyncResult(task_id)
    else:
        task = long_task_map1_anony.AsyncResult(task_id)
    if task.state == 'PENDING':
        response = {
            'state': task.state,
            'current': 0,
            'status': 'Pending...'
        }
    elif task.state != 'FAILURE':
        response = {
            'state': task.state,
            'current': task.info.get('current', 0),
            'status': task.info.get('status', ''),
            "progress_bar":task.info.get('progress_bar', '')
        }
        print "%%%%%%%%%%%%%",task.info
        if 'result' in task.info:
            response['result'] = task.info['result']
    else:
        # something went wrong in the background job
        response = {
            'state': task.state,
            'current': 1,
            'status': str(task.info),  # this is the exception raised
            "progress_bar":task.meta['progress_bar']
        }
    return jsonify(response)
    
@main.route('/<anony_id>/longtask_run_example_fastq', methods=['GET', 'POST'])
def longtask_run_example_fastq(anony_id):
    anony_id=session.get('anony_id')
    token = str(uuid.uuid4())
    output_folder = os.path.join(current_app.config['RESULTS_ANONY_FOLDER'],anony_id)
    session['token'] = token
    bulk_design=2
    input_type='Fastq'
    numbermap=1
    result_prefix="myresult"
    cov_depth=10
    kernel='tricube'
    width=33750
    chrnum=0
    noraw='yes'
    P1fastqfilelist=[os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','sake_strain_P1_1.fastq'),os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','sake_strain_P1_2.fastq')]
    P2fastqfilelist=[os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','white_tecc_strain_P2_1.fastq'),os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','white_tecc_strain_P2_2.fastq')]
    Hfastqlist=[[os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','Bulk_H.fastq')]]
    Lfastqlist=[[os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','Bulk_L.fastq')]]
    optional_email=''
    task_db=Tasks(owners=anony_id,owners_name='Anonymous',token_id=token,status='Running')
    task_db.create_time=datetime.utcnow()
    db.session.add(task_db)
    db.session.commit()
    return redirect(url_for('main.longtask', output_folder=output_folder,url_server=url_for('main.index_anony',anony_id=session.get('anony_id'),_external=True),token=token,input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
            P1fastqfile=json.dumps(P1fastqfilelist),\
            P2fastqfile=json.dumps(P2fastqfilelist),\
            Hfastqlist=json.dumps(Hfastqlist),\
            Lfastqlist=json.dumps(Lfastqlist),\
            readlen="",\
            phred="",\
            qccheck="",\
            trim="",\
            lastkeep="",\
            rmadapt="",\
            ladapter="",\
            radapter="",\
            minlen="",\
            removen="",\
            ncutoff="",\
            filtq="",\
            minq="",\
            pminq="",\
            rmunpaired="",\
            qcstat="",\
            insertsize="",\
            windowsize="",\
            cov_depth=cov_depth,\
            kernel=kernel,\
            width=width,\
            chrnum=chrnum,\
            noraw=noraw,anony_id=anony_id,optional_email=optional_email\
            ))
    
@main.route('/<anony_id>/longtask_run_example_bam', methods=['GET', 'POST'])
def longtask_run_example_bam(anony_id):
    anony_id=session.get('anony_id')
    token = str(uuid.uuid4())
    output_folder = os.path.join(current_app.config['RESULTS_ANONY_FOLDER'],anony_id)
    session['token'] = token
    bulk_design=2
    input_type='Bam'
    numbermap=1
    result_prefix="myresult"
    P1bamfilelist=os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','sake_strain_P1.bam')
    P2bamfilelist=os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','white_tecc_strain_P2.bam')
    Hbamlist=os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','Bulk_H.bam')
    Lbamlist=os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','Bulk_L.bam')
    cov_depth=10
    kernel='tricube'
    width=33750
    chrnum=0
    noraw='yes'
    session['Lbamlist'] = Lbamlist
    session['Hbamlist'] = Hbamlist
    optional_email=''
    task_db=Tasks(owners=anony_id,owners_name='Anonymous',token_id=token,status='Running')
    task_db.create_time=datetime.utcnow()
    db.session.add(task_db)
    db.session.commit()
    return redirect(url_for('main.longtask',output_folder=output_folder,url_server=url_for('main.index_anony',anony_id=session.get('anony_id'),_external=True),token=token,\
            input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
            P1bamfile=P1bamfilelist,\
            P2bamfile=P2bamfilelist,\
            Hbamlist=Hbamlist,\
            Lbamlist=Lbamlist, \
            cov_depth=cov_depth,\
            kernel=kernel,\
            width=width,\
            chrnum=chrnum,\
            noraw=noraw,anony_id=anony_id,optional_email=optional_email))
    
@main.route('/<anony_id>/longtask_run_example_map', methods=['GET', 'POST'])
def longtask_run_example_map(anony_id):
    anony_id=session.get('anony_id')
    token = str(uuid.uuid4())
    output_folder = os.path.join(current_app.config['RESULTS_ANONY_FOLDER'],anony_id)
    session['token'] = token
    bulk_design=2
    input_type='Map'
    numbermap=1
    result_prefix="myresult"
    Bulkmapfile1=['Bulk_H.map']
    Bulkmapfile2=['Bulk_L.map']
    kernel='tricube'
    length_input_file=os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','sc_chr.length')
    width=33750
    chrnum=0
    noraw='yes'
    Hmaplist = os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','Bulk_H.map')
    Lmaplist = os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','Bulk_L.map')
    session['Lmaplist'] = Lmaplist
    session['Hmaplist'] = Hmaplist
    optional_email=''
    task_db=Tasks(owners=anony_id,owners_name='Anonymous',token_id=token,status='Running')
    task_db.create_time=datetime.utcnow()
    db.session.add(task_db)
    db.session.commit()
    return redirect(url_for('main.longtask', output_folder=output_folder,url_server=url_for('main.index_anony',anony_id=session.get('anony_id'),_external=True),token=token,input_type=input_type,numbermap = numbermap ,result_prefix=result_prefix,bulk_design=bulk_design,\
            Bulkmapfile1=Bulkmapfile1,\
            Bulkmapfile2=Bulkmapfile2,\
            kernel=kernel,\
            width=width,\
            chrnum=chrnum,\
            noraw=noraw,anony_id=anony_id,optional_email=optional_email,\
            length_input_file=length_input_file\
            ))

@main.route('/<anony_id>/longtask_run_example_pseudo_map', methods=['GET', 'POST'])
def longtask_run_example_pseudo_map(anony_id):
    anony_id=session.get('anony_id')
    token = str(uuid.uuid4())
    output_folder = os.path.join(current_app.config['RESULTS_ANONY_FOLDER'],anony_id)
    session['token'] = token
    bulk_design=2
    input_type='Map'
    numbermap=1
    result_prefix="myresult"
    Bulkmapfile1=['pseudo_Bulk_H.map']
    Bulkmapfile2=['pseudo_Bulk_L.map']
    kernel='tricube'
    length_input_file=os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','pseudo_chr.length')
    width=33750
    chrnum=0
    noraw='yes'
    Hmaplist = os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','pseudo_Bulk_H.map')
    Lmaplist = os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','pseudo_Bulk_L.map')
    session['Lmaplist'] = Lmaplist
    session['Hmaplist'] = Hmaplist
    optional_email=''
    task_db=Tasks(owners=anony_id,owners_name='Anonymous',token_id=token,status='Running')
    task_db.create_time=datetime.utcnow()
    db.session.add(task_db)
    db.session.commit()
    return redirect(url_for('main.longtask', output_folder=output_folder,url_server=url_for('main.index_anony',anony_id=session.get('anony_id'),_external=True),token=token,input_type=input_type,numbermap = numbermap ,result_prefix=result_prefix,bulk_design=bulk_design,\
            Bulkmapfile1=Bulkmapfile1,\
            Bulkmapfile2=Bulkmapfile2,\
            kernel=kernel,\
            width=width,\
            chrnum=chrnum,\
            noraw=noraw,anony_id=anony_id,optional_email=optional_email,\
            length_input_file=length_input_file\
            ))
            
@main.route('/<anony_id>/results', methods=['GET', 'POST'])
def results(anony_id):
    anony_id=session.get('anony_id')
    params = request.args.items()
    print params
    for i, j in params:
        session[i] = j.encode("ascii")
    flash('Calculation done!')
#    token = str(session.get('token'))
    if session.get('input_type')=='Bam':
        if session.get('optional_email'):
            flash('A job done email has been sent to you by email.')
        return render_template('results_anony.html', input_type=session.get('input_type'),token=session.get('token'),pcutoff=float(session.get('pcutoff')), pdf_file=str(session.get('pdf_file')), gcutoff=float(session.get('gcutoff')), region_anno_file=str(session.get('region_anno_file'))+'.copy', G_file=str(session.get('G_file')), peak_anno_file=str(session.get('peak_anno_file'))+'.copy',freq_pdf_file=str(session.get('freq_pdf_file')),P1_P2_combined_exonic_variant_function_file=str(session.get('P1_P2_combined_exonic_variant_function_file'))+'copy',anony_id=anony_id)
    elif session.get('input_type')=='Fastq':
        if session.get('optional_email'):
            flash('A job done email has been sent to you by email.')
        return render_template('results_anony.html', input_type=session.get('input_type'),token=session.get('token'),pcutoff=float(session.get('pcutoff')), pdf_file=str(session.get('pdf_file')), gcutoff=float(session.get('gcutoff')), region_anno_file=str(session.get('region_anno_file'))+'.copy', G_file=str(session.get('G_file')), peak_anno_file=str(session.get('peak_anno_file'))+'.copy',freq_pdf_file=str(session.get('freq_pdf_file')),P1_P2_combined_exonic_variant_function_file=str(session.get('P1_P2_combined_exonic_variant_function_file'))+'copy',anony_id=anony_id)
    elif session.get('input_type')=='Map':
        if session.get('optional_email'):
            flash('A job done email has been sent to you by email.')
        return render_template('results_anony.html', input_type=session.get('input_type'),token=session.get('token'),pcutoff=float(session.get('pcutoff')), pdf_file=str(session.get('pdf_file')), gcutoff=float(session.get('gcutoff')),  G_file=str(session.get('G_file')), freq_pdf_file=str(session.get('freq_pdf_file')),anony_id=anony_id)
    else:
        pass
    
@main.route('/<anony_id>/download/<token_id>/<file>')
def download(token_id,file,anony_id):
    """Download"""
    anony_id=session.get('anony_id')
    return send_from_directory(os.path.join(current_app.config['RESULTS_ANONY_FOLDER'], anony_id, token_id),file, as_attachment=True)

@main.route('/<anony_id>/downloadresults/<token_id>/<file>')
def downloadresults(token_id,file,anony_id):
    """Download results"""
    anony_id=session.get('anony_id')
    return send_from_directory(os.path.join(current_app.config['RESULTS_ANONY_FOLDER'], anony_id, token_id), file, as_attachment=True)

@main.route("/<anony_id>/show_G_file/<token_id>/<file>")
def show_G_file(token_id,file,anony_id):
    anony_id=session.get('anony_id')
    G_table = pd.read_table(os.path.join(current_app.config['RESULTS_ANONY_FOLDER'], anony_id,token_id, file), header=None, names=['chr', 'coord', 'G','Gprime'], index_col=False)
    Gprime_mean=G_table.groupby(['chr'])['Gprime'].quantile([0,0.25,0.5,0.75,1])
    n_groups=G_table.groupby(['chr']).ngroups
    chr_levels=[]
    for i in range(1,n_groups+1):
        chr_levels.append(Solution().intToRoman(i))
    Gprime_mean.index.levels=[chr_levels,[0.0, 0.25, 0.5, 0.75, 1.0]]
    Gprime_mean_OrderedDict=OrderedDict()
    for i,j in enumerate(chr_levels):
        Gprime_mean_OrderedDict[j]=[Gprime_mean[5*i],Gprime_mean[5*i+1],Gprime_mean[5*i+3],Gprime_mean[5*i+4],Gprime_mean[5*i+2]]
#    G_table['chr']=G_table['chr'].replace([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],['I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII','XIII','XIV','XV','XVI'])
    return jsonify(Gprime_mean_OrderedDict)
    
@main.route("/<anony_id>/show_G_file2/<token_id>/<file>")
def show_G_file2(token_id,file,anony_id):
    anony_id=session.get('anony_id')
    G_table = pd.read_table(os.path.join(current_app.config['RESULTS_ANONY_FOLDER'], anony_id,token_id, file), header=None, names=['chr', 'coord', 'G','Gprime'], index_col=False)
    n_groups=G_table.groupby(['chr']).ngroups
    chr_levels=[]
    chr_levels_numeric=[]
    for i in range(1,n_groups+1):
        chr_levels_numeric.append(i-1)
        chr_levels.append(Solution().intToRoman(i))    
    G_table =G_table [['chr', 'coord','Gprime']]
    G_table['chr']=G_table['chr'].replace(chr_levels_numeric,chr_levels)
    Gprime_OrderedDict=G_table.to_dict(orient='index')
#    Gprime_OrderedDict=OrderedDict()
#    for i in range(len(G_table)):
#        Gprime_OrderedDict[i]=[G_table['chr'][i],G_table['coord'][i],G_table['Gprime'][i]]
    return jsonify(Gprime_OrderedDict)
    
    
@main.route("/<anony_id>/show_mapfile_tables/<token_id>/<file>")
def show_mapfile_tables(token_id,file,anony_id):
    anony_id=session.get('anony_id')
#    mapfile_table = pd.read_table(os.path.join(current_app.config['RESULTS_ANONY_FOLDER'], current_user.email,token_id, file), header=None, names=['chr', 'coord', 'P1', 'P2'], index_col=False)
    mapfile_table = pd.read_table(os.path.join(current_app.config['RESULTS_ANONY_FOLDER'], anony_id,token_id, file), header=None, names=['chr', 'coord', 'P1', 'P2'], index_col=False)
#    mapfile_table_P1_mean=mapfile_table.groupby(['chr'])['P1'].quantile([0,0.25,0.5,0.75,1])
#    mapfile_table_P2_mean=mapfile_table.groupby(['chr'])['P2'].quantile([0,0.25,0.5,0.75,1])
    mapfile_table_P1_mean=mapfile_table.groupby(['chr'])['P1'].mean().to_dict()
    mapfile_table_P2_mean=mapfile_table.groupby(['chr'])['P2'].mean().to_dict()
#    mapfile_table_P1_mean.index.levels=[['I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII','XIII','XIV','XV','XVI'],[0.0, 0.25, 0.5, 0.75, 1.0]]
#    mapfile_table_P1_mean_OrderedDict=OrderedDict()
#    for i,j in enumerate(['I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII','XIII','XIV','XV','XVI']):
#        mapfile_table_P1_mean_OrderedDict[j]=[mapfile_table_P1_mean[5*i],mapfile_table_P1_mean[5*i+1],mapfile_table_P1_mean[5*i+3],mapfile_table_P1_mean[5*i+4],mapfile_table_P1_mean[5*i+2]]
#    mapfile_table_P2_mean.index.levels=[['I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII','XIII','XIV','XV','XVI'],[0.0, 0.25, 0.5, 0.75, 1.0]]
#    mapfile_table_P2_mean_OrderedDict=OrderedDict()
#    for i,j in enumerate(['I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII','XIII','XIV','XV','XVI']):
#        mapfile_table_P2_mean_OrderedDict[j]=[mapfile_table_P2_mean[5*i],mapfile_table_P2_mean[5*i+1],mapfile_table_P2_mean[5*i+3],mapfile_table_P2_mean[5*i+4],mapfile_table_P2_mean[5*i+2]]    
#    return jsonify({'P1':mapfile_table_P1_mean_OrderedDict,'P2':mapfile_table_P2_mean_OrderedDict})
    return jsonify({'P1':mapfile_table_P1_mean,'P2':mapfile_table_P2_mean})

@main.route("/<anony_id>/show_mapfile_tables_page/<token_id>/<file>")
def show_mapfile_tables_page(token_id,file,anony_id):
    anony_id=session.get('anony_id')
    return render_template('view_mapfile_tables_anony.html',token_id=token_id,file=file,anony_id=anony_id)    

@main.route("/<anony_id>/show_freq_file/<token_id>/<file>")
def show_freq_file(token_id,file,anony_id):
    anony_id=session.get('anony_id')
    freq_table = pd.read_table(os.path.join(current_app.config['RESULTS_ANONY_FOLDER'], anony_id,token_id, file), header=None, names=['label', 'x', 'y'], index_col=False,sep=' ')
    n_groups=freq_table.groupby(['label']).ngroups
    color_name_index=['Blue','BlueViolet','Brown','BurlyWood','CadetBlue','Chartreuse','Chocolate','CornflowerBlue',\
                 'Crimson','DarkBlue','DarkCyan','DarkGoldenRod','DarkMagenta','DarkOrange','DeepSkyBlue','ForestGreen']
    color_index=OrderedDict()
    for i in range(1,n_groups+1):
        color_index.update({Solution().intToRoman(i):color_name_index[i%16]})   
#    color_index={'I':'Blue','II':'BlueViolet','III':'Brown','IV':'BurlyWood','V':'CadetBlue','VI':'Chartreuse','VII':'Chocolate','VIII':'CornflowerBlue',\
#                 'IX':'Crimson','X':'DarkBlue','XI':'DarkCyan','XII':'DarkGoldenRod','XIII':'DarkMagenta','XIV':'DarkOrange','XV':'DeepSkyBlue','XVI':'ForestGreen'}
    freq_table['color']=[color_index[i] for i in [x for x in freq_table['label']]]
    freq_Dict={}
    for i in color_index.keys():
        freq_Dict[i]=freq_table.groupby('label').get_group(i).to_dict(orient='records')
    return jsonify(freq_Dict)


@main.route("/<anony_id>/show_freq_file_mean/<token_id>/<file>")
def show_freq_file_mean(token_id,file,anony_id):
    anony_id=session.get('anony_id')
    freq_table = pd.read_table(os.path.join(current_app.config['RESULTS_ANONY_FOLDER'], anony_id,token_id, file), header=None, names=['chr', 'coord', 'freq'], index_col=False,sep=' ')       
    freq_table_mean=freq_table['freq'].mean()
    return jsonify({'freq_mean':freq_table_mean})
    
@main.route("/<anony_id>/show_freq_file_page/<token_id>/<file>")
def show_freq_file_page(token_id,file,anony_id):
    anony_id=session.get('anony_id')
    return render_template('view_freq_table_anony.html',token_id=token_id,file=file,anony_id=anony_id)

    
@main.route("/<anony_id>/show_threshold/<token_id>/<threshold_file>")
def show_threshold(token_id,threshold_file,anony_id):
    anony_id=session.get('anony_id')
    threshold_table = pd.read_table(os.path.join(current_app.config['RESULTS_ANONY_FOLDER'], anony_id,token_id, threshold_file), header=0, names=['pcutoff','gcutoff'], index_col=False)
    if threshold_table['pcutoff'][0]=='None':
        threshold_table['pcutoff'][0]=0
    if threshold_table['gcutoff'][0]=='None':
        threshold_table['gcutoff'][0]=0
    return jsonify({'pcutoff':threshold_table['pcutoff'][0],'gcutoff':threshold_table['gcutoff'][0]})

@main.route("/<anony_id>/show_threshold_page/<token_id>/<threshold_file>")
def show_threshold_page(token_id,threshold_file,anony_id):
    anony_id=session.get('anony_id')
    threshold_table = pd.read_table(os.path.join(current_app.config['RESULTS_ANONY_FOLDER'], anony_id,token_id, threshold_file), header=0, names=['pcutoff','gcutoff'], index_col=False)
    return render_template('view_threshold_table.html',token_id=token_id,threshold_table=threshold_table,anony_id=anony_id)

@main.route("/<anony_id>/show_peak_page/<token_id>/<peak_file>")
def show_peak_page(token_id,peak_file,anony_id):
    anony_id=session.get('anony_id')
    peak_table = pd.read_table(os.path.join(current_app.config['RESULTS_ANONY_FOLDER'], anony_id,token_id, peak_file), header=None, names=['chr','coord','coord1'], index_col=False)
    return render_template('view_peak_table.html',token_id=token_id,peak_table=peak_table,anony_id=anony_id)

@main.route("/<anony_id>/show_region_page/<token_id>/<region_file>")
def show_region_page(token_id,region_file,anony_id):
    anony_id=session.get('anony_id')
    region_table = pd.read_table(os.path.join(current_app.config['RESULTS_ANONY_FOLDER'], anony_id,token_id, region_file), header=None, names=['chr','coord1','coord2'], index_col=False)
    return render_template('view_region_table.html',token_id=token_id,region_table=region_table,anony_id=anony_id)

@main.route("/<anony_id>/show_G_file_page/<token_id>/<file>/<threshold_file>")
def show_G_file_page(token_id,file,threshold_file,anony_id):
    anony_id=session.get('anony_id')
    return render_template('view_G_table_anony.html',token_id=token_id,file=file,threshold_file=threshold_file,anony_id=anony_id)

@main.route("/<anony_id>/show_region_anno_tables/<token_id>/<file>")
def show_region_anno_tables(token_id,file,anony_id):
    anony_id=session.get('anony_id')
    region_anno = pd.read_table(os.path.join(current_app.config['RESULTS_ANONY_FOLDER'], anony_id,token_id, file), header=0, names=['chr', 'coord_variant', 'P1_start', 'P1_stop', 'P1_ref', 'P1_alt', 'P2_alt', 'P1_variant_type', 'P1_anno', 'gene_id_P1', 'exon_P1', 'nt_P1', 'protein_P1', 'gene_id_link_P1', 'P2_variant_type', 'P2_anno', 'gene_id_P2', 'exon_P2', 'nt_P2', 'protein_P2', 'gene_id_link_P2','P1_SNAP',	'P2_SNAP', 'chr_region', 'region_start', 'region_end'], index_col=False)
    region_anno1=region_anno[['chr', 'region_start', 'region_end', 'coord_variant',  'P1_ref', 'P1_alt', 'P2_alt', 'P1_variant_type', 'P1_anno', 'gene_id_P1', 'exon_P1', 'nt_P1', 'protein_P1','P1_SNAP', 'gene_id_link_P1', 'P2_variant_type', 'P2_anno', 'gene_id_P2', 'exon_P2', 'nt_P2', 'protein_P2','P2_SNAP', 'gene_id_link_P2']]
    return render_template('view_region_anno.html',token_id=token_id,table_file=region_anno1,anony_id=anony_id)

@main.route("/<anony_id>/show_peak_anno_tables/<token_id>/<file>")
def show_peak_anno_tables(token_id,file,anony_id):
    anony_id=session.get('anony_id')
    peak_anno = pd.read_table(os.path.join(current_app.config['RESULTS_ANONY_FOLDER'], anony_id,token_id, file), header=0, names=['chr', 'coord_variant', 'start', 'stop', 'P1_ref', 'P1_alt', 'P2_alt', 'P1_variant_type', 'P1_anno', 'gene_id_P1', 'exon_P1', 'nt_P1', 'protein_P1', 'gene_id_link_P1', 'P2_variant_type', 'P2_anno', 'gene_id_P2', 'exon_P2', 'nt_P2', 'protein_P2', 'gene_id_link_P2','P1_SNAP',	'P2_SNAP', 'chr_peak', 'peak', 'end_region'], index_col=False)
    peak_anno1=peak_anno[['chr', 'peak', 'coord_variant',  'P1_ref', 'P1_alt', 'P2_alt', 'P1_variant_type', 'P1_anno', 'gene_id_P1', 'exon_P1', 'nt_P1', 'protein_P1', 'P1_SNAP','gene_id_link_P1', 'P2_variant_type', 'P2_anno', 'gene_id_P2', 'exon_P2', 'nt_P2', 'protein_P2', 'P2_SNAP','gene_id_link_P2']]
    return render_template('view_peak_anno.html',token_id=token_id,table_file=peak_anno1,anony_id=anony_id)

@main.route("/<anony_id>/show_tables/<token_id>/<file>")
def show_tables(token_id,file,anony_id):
    anony_id=session.get('anony_id')
    return render_template('view_anony.html',token_id=token_id,table_file=file,anony_id=anony_id)

@main.route("/<anony_id>/show_P1tables_api/<token_id>/<file>")
def show_P1tables_api(token_id,file,anony_id):
    anony_id=session.get('anony_id')
    ff=pd.read_table(os.path.join(current_app.config['RESULTS_ANONY_FOLDER'], anony_id, token_id,file), header=0)
    ff.fillna('NotAvail',inplace=True)
    data_table=ff
    aa=data_table.groupby(['P1_variant_type'])['P1_variant_type'].value_counts()
    aa_OrderedDict=OrderedDict()
    for i in aa.index.levels[0]:
        aa_OrderedDict[i]=aa[i].values[0]
    aa_OrderedDict.popitem(last=True)
    aa_OrderedDict.popitem(last=False)    
    return jsonify(aa_OrderedDict)

@main.route("/<anony_id>/show_P2tables_api/<token_id>/<file>")
def show_P2tables_api(token_id,file,anony_id):
    anony_id=session.get('anony_id')
    ff=pd.read_table(os.path.join(current_app.config['RESULTS_ANONY_FOLDER'], anony_id, token_id,file), header=0)
    ff.fillna('NotAvail',inplace=True)
    data_table=ff
    aa=data_table.groupby(['P2_variant_type'])['P2_variant_type'].value_counts()
    aa_OrderedDict=OrderedDict()
    for i in aa.index.levels[0]:
        aa_OrderedDict[i]=aa[i].values[0]
    aa_OrderedDict.popitem(last=False)    
    return jsonify(aa_OrderedDict)
    
@main.route("/<anony_id>/tables/<token_id>/<file>")
def tables(token_id,file,anony_id):
    anony_id=session.get('anony_id')
    params = request.args.items()
    params1={}
    for i in params:
        j,k=i
        params1[j]=k
    print params1
    ff=pd.read_table(os.path.join(current_app.config['RESULTS_ANONY_FOLDER'], anony_id, token_id,file), header=0)
    ff.fillna('NotAvail',inplace=True)
    data_table=ff
    request_values=params1
    data_table_columns=list(data_table)
    result_data_table = None
    data_table_cardinality_filtered = 0
    data_table_cardinality = len(data_table.index)
    data_table_draw=int(params1['draw'])
    data_table_start=int(params1['start'])
    data_table_length=int(params1['length'])
    if (request_values.has_key('search[value]') ) and ( request_values['search[value]'] != "" ):             
        row_filter = [] 
        alist=[]
        for i in data_table_columns:
            data_table[i]=data_table[i].astype(str)
            alist=alist+data_table.index[data_table[i].str.contains(request_values['search[value]'])].tolist()
        row_filter=list(set(alist))             
        result_data_table = data_table.iloc[row_filter]
        data_table_cardinality_filtered=len(result_data_table.index)
        if request_values.has_key('order[0][column]'):
            if request_values['order[0][dir]']=='asc':
                result_data_table.sort_values(data_table_columns[int(request_values['order[0][column]'])],0,True,True)
            else:
                result_data_table.sort_values(data_table_columns[int(request_values['order[0][column]'])],0,False,True)
    else:
        result_data_table=data_table
        data_table_cardinality_filtered=len(result_data_table.index)
        if request_values.has_key('order[0][column]'):
            if request_values['order[0][dir]']=='asc':
                result_data_table.sort_values(data_table_columns[int(request_values['order[0][column]'])],0,True,True)
            else:
                result_data_table.sort_values(data_table_columns[int(request_values['order[0][column]'])],0,False,True)
    if data_table_start+data_table_length>ff.shape[0]:
        try:
            last_nrow=ff.shape[0]-data_table_start
            result_data_table2=result_data_table.iloc[range(data_table_start,data_table_start+last_nrow)]
        except:
            result_data_table2=result_data_table
    else:
        try:
            result_data_table2=result_data_table.iloc[range(data_table_start,data_table_start+data_table_length)]
        except:
            result_data_table2=result_data_table
    return jsonify({'draw':data_table_draw,'recordsTotal':data_table_cardinality,'recordsFiltered':data_table_cardinality_filtered,'data':result_data_table2.to_dict(orient='records'),'anony_id':anony_id})


@main.route('/<anony_id>/Genome', methods=['GET', 'POST'])
def Genome_anony(anony_id):
    sacCer3_refGene='sacCer3_refGene.txt'
    sacCer3_refGeneMrna='sacCer3_refGeneMrna.fa'
    sc_chr_length='sc_chr.length'
    sc64_bed='sc64.bed'
    sc64_fa='sc64.fa'
    sc64_fa_fai='sc64.fa.fai'
    sc64_gtf='sc64.gtf'
    sacCer3_zip='sacCer3.zip'
    return render_template('Genome_anony.html', sacCer3_zip=sacCer3_zip,sacCer3_refGene=sacCer3_refGene,sacCer3_refGeneMrna=sacCer3_refGeneMrna,sc_chr_length=sc_chr_length,sc64_bed=sc64_bed,sc64_fa=sc64_fa,sc64_fa_fai=sc64_fa_fai,sc64_gtf=sc64_gtf,anony_id=anony_id)

@main.route('/<anony_id>/Example', methods=['GET', 'POST'])
def Example_anony(anony_id):
    P1_1_fastq_zip='sake_strain_P1_1.fastq.gz'
    P1_2_fastq_zip='sake_strain_P1_2.fastq.gz'
    P2_1_fastq_zip='white_tecc_strain_P2_1.fastq.gz'
    P2_2_fastq_zip='white_tecc_strain_P2_2.fastq.gz'
    P1_bam='sake_strain_P1.bam'
    P2_bam='white_tecc_strain_P2.bam'
    Hbulk_fastq_zip='Bulk_H.fastq.gz'
    Lbulk_fastq_zip='Bulk_L.fastq.gz'
    Hbulk_bam='Bulk_H.bam'
    Lbulk_bam='Bulk_L.bam'
    Hbulk_map_zip='Bulk_H.map.zip'
    Lbulk_map_zip='Bulk_L.map.zip'
    pseudo_Hbulk_map_zip='pseudo_Bulk_H.map.zip'
    pseudo_Lbulk_map_zip='pseudo_Bulk_L.map.zip'
    pseudo_chr_length='pseudo_chr.length'
    sc_chr_length='sc_chr.length'
    return render_template('Example_anony.html', P1_1_fastq_zip=P1_1_fastq_zip,P1_2_fastq_zip=P1_2_fastq_zip,P2_1_fastq_zip=P2_1_fastq_zip,P2_2_fastq_zip=P2_2_fastq_zip,P1_bam=P1_bam,P2_bam=P2_bam,Hbulk_fastq_zip=Hbulk_fastq_zip,Lbulk_fastq_zip=Lbulk_fastq_zip,Hbulk_bam=Hbulk_bam,Lbulk_bam=Lbulk_bam,Hbulk_map_zip=Hbulk_map_zip,Lbulk_map_zip=Lbulk_map_zip,pseudo_Hbulk_map_zip=pseudo_Hbulk_map_zip,pseudo_Lbulk_map_zip=pseudo_Lbulk_map_zip,pseudo_chr_length=pseudo_chr_length,sc_chr_length=sc_chr_length,anony_id=anony_id)
     
#@auth.route('/Show_user', methods=['GET', 'POST'])
#@login_required
#def Show_user():
#    user=User.query.filter_by(id=current_user.id).first()
##    return redirect(url_for('auth.index'))
#    return render_template('Show_user.html',user=user)
    
@main.route('/<anony_id>/show_upload', methods=['GET', 'POST'])
def show_upload(anony_id):
    anony_id=session.get('anony_id')
    upload_files=Files.query.filter_by(owners=anony_id).filter_by(ftype1 ='U').all()
    return render_template('show_upload_anony.html',upload_files=upload_files,anony_id=anony_id)

@main.route('/<anony_id>/count_upload', methods=['GET', 'POST'])
def count_upload(anony_id):
    anony_id=session.get('anony_id')
    n=Files.query.filter_by(owners=anony_id).filter_by(ftype1 ='U').count()
#    return jsonify({'num_uploaded_files':n})   
    return str(n)
    
@main.route('/<anony_id>/delete_upload', methods=['GET', 'POST'])
def delete_upload(anony_id):
    anony_id=session.get('anony_id')
    params = request.args.items()
    delete_items={}
    for i in params:
        j,k=i
        if re.search('checkbox_',j):
             delete_items[j]=k
    print delete_items
    if delete_items:
        for i,j in delete_items.items():
            f=os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'],anony_id,Files.query.filter_by(id=j).first().fname)
            print f
#            if f.endswith('.fastq.gz') and os.path.isfile(os.path.splitext(f)[0]):
#                os.remove(os.path.splitext(f)[0])
            if os.path.isfile(f):
                os.remove(f)
            Files.query.filter_by(id=j).delete()
            db.session.commit()
    upload_files=Files.query.filter_by(owners=anony_id).filter_by(ftype1 ='U').all()
    return render_template('show_upload_anony.html',upload_files=upload_files,anony_id=anony_id)

@main.route('/<anony_id>/refresh_upload', methods=['GET', 'POST'])
def refresh_upload(anony_id):
    anony_id=session.get('anony_id')
    upload_files=Files.query.filter_by(owners=anony_id).filter_by(ftype1 ='U').all()
    return render_template('show_upload_anony.html',upload_files=upload_files,anony_id=anony_id)

@main.route('/<anony_id>/show_result', methods=['GET', 'POST'])
def show_result(anony_id):
    anony_id=session.get('anony_id')
    result_files=Files.query.filter_by(owners=anony_id).filter_by(ftype1='R').all()
    return render_template('show_result_anony.html',result_files=result_files,anony_id=anony_id)

@main.route('/<anony_id>/count_result', methods=['GET', 'POST'])
def count_result(anony_id):
    anony_id=session.get('anony_id')
    n=Files.query.filter_by(owners=anony_id).filter_by(ftype1 ='R').count()
    return str(n)
        
@main.route('/<anony_id>/delete_result', methods=['GET', 'POST'])
def delete_result(anony_id):
    anony_id=session.get('anony_id')
    params = request.args.items()
    delete_items={}
    for i in params:
        j,k=i
        if re.search('checkbox_',j):
             delete_items[j]=k
    print delete_items
    if delete_items:
        for i,j in delete_items.items():
#            f=os.path.join(current_app.config['RESULTS_ANONY_FOLDER'],current_user.email,Files.query.filter_by(id=j).first().ffolder,Files.query.filter_by(id=j).first().fname)
            f=os.path.join(current_app.config['RESULTS_ANONY_FOLDER'],anony_id,Files.query.filter_by(id=j).first().ffolder,Files.query.filter_by(id=j).first().fname)
            print f
            if os.path.isfile(f):
                os.remove(f)
            Files.query.filter_by(id=j).delete()
            db.session.commit()
    result_files=Files.query.filter_by(owners=anony_id).filter_by(ftype1='R').all()
    return render_template('show_result_anony.html',result_files=result_files,anony_id=anony_id)

#@auth.route('/Download_all_result', methods=['GET', 'POST'])
#@login_required
#def Download_all_result():
#    params = request.args.items()
#    delete_items={}
#    for i in params:
#        j,k=i
#        if re.search('checkbox_',j):
#             delete_items[j]=k
#    print delete_items
#    if delete_items:
#        for i,j in delete_items.items():
#            Files.query.filter_by(id=j).delete()
#            db.session.commit()
#    result_files=Files.query.filter_by(owners=current_user.id).filter_by(ftype1='R').all()
#    return render_template('show_result.html',result_files=result_files)
    
@main.route('/<anony_id>/refresh_result', methods=['GET', 'POST'])
def refresh_result(anony_id):
    anony_id=session.get('anony_id')
    result_files=Files.query.filter_by(owners=anony_id).filter_by(ftype1='R').all()
    return render_template('show_result_anony.html',result_files=result_files,anony_id=anony_id)
    
@main.route('/<anony_id>/show_job', methods=['GET', 'POST'])
def show_job(anony_id):
    anony_id=session.get('anony_id')
    job_files=Tasks.query.filter_by(owners=anony_id).all()
    return render_template('show_job_anony.html',job_files=job_files,anony_id=anony_id)

@main.route('/<anony_id>/delete_job', methods=['GET', 'POST'])
def delete_job(anony_id):
    anony_id=session.get('anony_id')
    params = request.args.items()
    delete_items={}
    for i in params:
        j,k=i
        if re.search('checkbox_',j):
             delete_items[j]=k
    print delete_items
    if delete_items:
        for i,j in delete_items.items():
            running_tasks=Tasks.query.filter_by(id=j).filter_by(status='Running').all()
            if running_tasks:
                for running_task in running_tasks:
                    running_task_id=running_task.token_id
                    Control(app=celery).revoke(running_task_id,  terminate=True)
                    Tasks.query.filter_by(owners=anony_id).filter_by(token_id=running_task_id).delete()        
                    Files.query.filter_by(owners=anony_id).filter_by(ffolder=running_task_id).delete()        
                    remove_folder(os.path.join(current_app.config['RESULTS_ANONY_FOLDER'],anony_id,running_task_id))
            other_tasks=Tasks.query.filter_by(id=j).filter(Tasks.status!='Running').all()
            if other_tasks:
                for other_task in other_tasks:
                    other_task_id=other_task.token_id
                    Tasks.query.filter_by(owners=anony_id).filter_by(token_id=other_task_id).delete()        
                    Files.query.filter_by(owners=anony_id).filter_by(ffolder=other_task_id).delete()        
                    remove_folder(os.path.join(current_app.config['RESULTS_ANONY_FOLDER'],anony_id,other_task_id))        
            db.session.commit()
    job_files=Tasks.query.filter_by(owners=anony_id).all()
    return render_template('show_job_anony.html',job_files=job_files,anony_id=anony_id)

@main.route('/<anony_id>/terminate_job', methods=['GET', 'POST'])
def terminate_job(anony_id):
    anony_id=session.get('anony_id')
    params = request.args.items()
    terminate_items={}
    for i in params:
        j,k=i
        if re.search('checkbox_',j):
             terminate_items[j]=k
    print terminate_items
    if terminate_items:
        for i,j in terminate_items.items():
            running_tasks=Tasks.query.filter_by(id=j).filter_by(status='Running').all()
            if running_tasks:
                for running_task in running_tasks:
                    running_task_id=running_task.token_id
                    Control(app=celery).revoke(running_task_id,  terminate=True)
                    task_db=Tasks.query.filter_by(owners=anony_id).filter_by(token_id=running_task_id).first()
                    if task_db:
                        task_db.status='Failed'        
                    Files.query.filter_by(owners=anony_id).filter_by(ffolder=running_task_id).delete()        
                    db.session.add(task_db)
            db.session.commit()
    job_files=Tasks.query.filter_by(owners=anony_id).all()
    return render_template('show_job_anony.html',job_files=job_files,anony_id=anony_id)
        
@main.route('/<anony_id>/refresh_job', methods=['GET', 'POST'])
def refresh_job(anony_id):
    anony_id=session.get('anony_id')
    job_files=Tasks.query.filter_by(owners=anony_id).all()
    return render_template('show_job_anony.html',job_files=job_files,anony_id=anony_id)
       
@main.route('/<anony_id>/count_job_running', methods=['GET', 'POST'])
def count_job_running(anony_id):
    anony_id=session.get('anony_id')
    n=Tasks.query.filter_by(owners=anony_id).filter_by(status ='Running').count()
    return str(n)

@main.route('/<anony_id>/count_job_done', methods=['GET', 'POST'])
def count_job_done(anony_id):
    anony_id=session.get('anony_id')
    n=Tasks.query.filter_by(owners=anony_id).filter_by(status ='Done').count()
    return str(n)

@main.route('/<anony_id>/count_job_failed', methods=['GET', 'POST'])
def count_job_failed(anony_id):
    anony_id=session.get('anony_id')
    n=Tasks.query.filter_by(owners=anony_id).filter_by(status ='Failed').count()
    return str(n)
    
@main.route('/<anony_id>/count_free_upload', methods=['GET', 'POST'])
def count_free_upload(anony_id):
    anony_id=session.get('anony_id')
    free_upload=count_free_upload_api()
    if free_upload<1000 and free_upload>0:
        return '<span  class="badge badge-danger">'+str(free_upload)+'B'+'</span>'
    elif free_upload >= 1000 and free_upload<1000000:
        return '<span  class="badge badge-warning">'+str(free_upload/1000)+'K'+'</span>'
    elif free_upload >= 1000000 and free_upload<1000000000:
        return '<span  class="badge badge-success">'+str(free_upload/1000000)+'M'+'</span>'
    elif free_upload >= 1000000000 and free_upload<1000000000000:
        return '<span  class="badge badge-success">'+str(free_upload/1000000000)+'G'+'</span>'
    else:
        return '<span  class="badge badge-danger">'+'NA'+'</span>'

@main.route('/<anony_id>/view_free_upload', methods=['GET', 'POST'])
def view_free_upload(anony_id):
    anony_id=session.get('anony_id')
    used_upload=get_folder_size(os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'],anony_id))
    total_space=int(current_app.config['CUSTOM_UPLOADS_FOLDER_SIZE_LIMIT'])
    free_upload=int(current_app.config['CUSTOM_UPLOADS_FOLDER_SIZE_LIMIT'])-int(used_upload)
    if free_upload<0:
        free_upload=0
    return render_template('view_free_upload.html',total= total_space,used=used_upload,free=free_upload)

@main.route('/<anony_id>/view_free_result', methods=['GET', 'POST'])
def view_free_result(anony_id):
    anony_id=session.get('anony_id')
    used_result=get_folder_size(os.path.join(current_app.config['RESULTS_ANONY_FOLDER'],anony_id))
    total_space=int(current_app.config['CUSTOM_RESULTS_FOLDER_SIZE_LIMIT'])
    free_result=int(current_app.config['CUSTOM_RESULTS_FOLDER_SIZE_LIMIT'])-int(used_result)
    if free_result<0:
        free_result=0
    return render_template('view_free_result.html',total= total_space,used=used_result,free=free_result,anony_id=anony_id)

@main.route('/<anony_id>/count_free_result', methods=['GET', 'POST'])
def count_free_result(anony_id):
    anony_id=session.get('anony_id')
    free_result=count_free_result_api()
    if free_result<1000 and free_result>0:
        return '<span  class="badge badge-danger">'+str(free_result)+'B'+'</span>'
    elif free_result >= 1000 and free_result<1000000:
        return '<span  class="badge badge-warning">'+str(free_result/1000)+'K'+'</span>'
    elif free_result >= 1000000 and free_result<1000000000:
        return '<span  class="badge badge-success">'+str(free_result/1000000)+'M'+'</span>'
    elif free_result >= 1000000000 and free_result<1000000000000:
        return '<span  class="badge badge-success">'+str(free_result/1000000000)+'G'+'</span>'
    else:
        return '<span  class="badge badge-danger">'+'NA'+'</span>'
        
@main.route('/<anony_id>/guideline_anony', methods=['GET', 'POST'])
def guideline_anony(anony_id):
    return render_template('guideline_anony.html',anony_id=anony_id)
    
@main.route('/<anony_id>/result_format_anony', methods=['GET', 'POST'])
def result_format_anony(anony_id):
    return render_template('result_format_anony.html',anony_id=anony_id)

@main.route('/<anony_id>/get_chr', methods=['GET', 'POST'])
def get_chr(anony_id):
    filename=request.json['chr_file_name']
    print filename
    nline=len(open(os.path.join(current_app.config['UPLOADS_ANONY_FOLDER'], anony_id,filename)).readlines(  ))        
    chr_choice=OrderedDict()
    chr_choice['all']='all'
    for i in range(1,nline+1):
        chr_choice[i]=i
    return jsonify(chr_choice)




















