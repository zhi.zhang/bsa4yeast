from flask import Flask
from flask_bootstrap import Bootstrap
from flask_mail import Mail
from flask_moment import Moment
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.schema import UniqueConstraint
from flask_login import LoginManager
from config import config,Config
from celery import Celery
from flask_security import Security, SQLAlchemyUserDatastore,UserMixin, RoleMixin, login_required
#from flask_dropzone import Dropzone

bootstrap = Bootstrap()
mail = Mail()
moment = Moment()
db = SQLAlchemy()
#dropzone = Dropzone()
login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'auth.login'

celery = Celery(__name__, broker=Config.CELERY_BROKER_URL,backend=Config.CELERY_RESULT_BACKEND)

def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)
    bootstrap.init_app(app)
# add dropzone
#    dropzone.init_app(app)
    mail.init_app(app)
    moment.init_app(app)
    db.init_app(app)
    celery.conf.update(app.config)
    login_manager.init_app(app)

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)
    
    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint, url_prefix='/auth')

    return app

