import sys

class Solution(object):
    def intToRoman(self, num):
        """
        :type num: int
        :rtype: str
        """
        M = ["", "M", "MM", "MMM"]
        C = ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"]
        X = ["", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"]
        I = ["", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"]
        return M[num/1000] + C[(num%1000)/100] + X[(num%100)/10] + I[num%10]
        
inFile = open(sys.argv[1],'r')
line=inFile.readline().strip().split('\t')
print '\t'.join([str(x) for x in line])
#chr_data={'I':[],'II':[],'III':[],'IV':[],'V':[],'VI':[],'VII':[],'VIII':[],'IX':[],'X':[],'XI':[],'XII':[],'XIII':[],'XIV':[],'XV':[],'XVI':[]}
chr_data={}
tmpchr=[]
for i in range(1,51):
    chr_data[Solution().intToRoman(i)]=[]
    tmpchr.append(Solution().intToRoman(i))
    
for line in inFile:
    data = line.strip().split('\t')
    if data[0] in chr_data.keys():
        chr_data[data[0]].append(line.strip())

#for i in ['I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII','XIII','XIV','XV','XVI']:
for i in tmpchr:
    if len(chr_data[i])>0 :
        print '\n'.join([str(x) for x in chr_data[i]])

