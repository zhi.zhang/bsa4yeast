from flask import current_app,url_for,redirect,render_template,flash,session
from flask_login import login_user, logout_user, login_required, current_user
from celery.utils.log import get_task_logger
import time
from app import celery
from ..bsaseq import bsacalc,bsadraw,bsautil
import os
import re
import pandas as pd
import os.path
import subprocess
from celery import current_task,Task
from ..models import User,Role,Files,Tasks
from ..email import send_email
from .. import db
from datetime import datetime
import json
import shutil
import glob
import gzip
# catch task exception
class LogErrorsTask(Task):
    def on_failure(self, exc, task_id, args, kwargs, einfo):
        self.save_failed_task(exc, task_id, args, kwargs, einfo)
        super(LogErrorsTask, self).on_failure(exc, task_id, args, kwargs, einfo)

    def save_failed_task(self, exc, task_id, args, kwargs, traceback):
        """
        :type exc: Exception
        """
#        print "$"*100
#        print  args
#        print  kwargs
#        print "$"*100
#        print json.dumps(list(args))
#        print json.dumps(kwargs)
        user_email=os.path.basename(str(args[0]))
        url_server=str(args[1])
#        print "^"*100
#        print user_email
#        print task_id
#        print Tasks.query.filter_by(owners=User.query.filter_by(email=user_email).first().id).all()
        task_db=Tasks.query.filter_by(owners=User.query.filter_by(email=user_email).first().id).filter_by(token_id=task_id).first()
        if task_db:
            task_db.status='Failed'
            task_db.end_time=datetime.utcnow()
            duration_time_in_secondes=(task_db.end_time-task_db.create_time).total_seconds()
            task_db.duration_time=str('%d:%d:%d'%(duration_time_in_secondes/3600,duration_time_in_secondes% 3600/60,duration_time_in_secondes% 3600%60))
            db.session.add(task_db)
        db.session.commit()
        send_email(user_email, 'Your job (%s) failed!'%(str(task_id)),\
                   'auth/email/task_failed', url_server=url_server,user=user_email, token=str(task_id))
        return {'current': 'Task failed!','result':{'input_type':'NA','pcutoff':'NA','gcutoff':'NA','token':'NA','G_file': 'NA', 'pdf_file': 'NA', 'peak_anno_file': 'NA','region_anno_file':'NA','freq_pdf_file':'NA','P1_P2_combined_exonic_variant_function_file':'NA','freq_file':'NA','peak_file':'NA','region_file':'NA','threshold_file':'NA'},'status':'Task failed!'}

def get_file_size(f):
    if os.path.isfile(f):
        file_size=os.path.getsize(f)
        if file_size<1000:
            return str(file_size)+'B'
        elif file_size >= 1000 and file_size<1000000:
            return str(file_size/1000)+'K'
        elif file_size >= 1000000 and file_size<1000000000:
            return str(file_size/1000000)+'M'
        elif file_size >= 1000000000 and file_size<1000000000000:
            return str(file_size/1000000000)+'G'
        else:
            return 'Not Available'
    else:
        return 'Not A File'

def TIMEOUT_COMMAND(command, timeout):  
    """call shell-command and either return its output or kill it 
    if it doesn't normally exit within timeout seconds and return None"""  
    import subprocess, datetime, os, time, signal  
    cmd = command.split(" ")  
    start = datetime.datetime.now()  
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)  
    while process.poll() is None:  
        time.sleep(0.2)  
        now = datetime.datetime.now()  
        if (now - start).seconds> timeout:  
            os.kill(process.pid, signal.SIGKILL)  
            os.waitpid(-1, os.WNOHANG)  
            return None  
    return process.stdout.readlines() 

def getFile(path, regex):
    pattern='%s/*%s'%(path,regex)
    files = glob.glob(pattern)
    return files
    
Q20_1_path=os.path.join(os.path.dirname(os.path.dirname(__file__)),'bsaseq/Q20_1.py')
rmN_path=os.path.join(os.path.dirname(os.path.dirname(__file__)),'bsaseq/rmN.py')
ReadMap_path = os.path.join(os.path.dirname(os.path.dirname(__file__)),'bsaseq/ReadMap.R')
sortCover_path=os.path.join(os.path.dirname(os.path.dirname(__file__)),'bsaseq/sortCover2.py')
matePair_path=os.path.join(os.path.dirname(os.path.dirname(__file__)),'bsaseq/rmUnpaired1.py')
cutadapt_path=os.path.join(os.path.dirname(os.path.dirname(__file__)),'bsaseq/cutadapt')
GATK_path=os.path.join(os.path.dirname(os.path.dirname(__file__)),'bsaseq/GenomeAnalysisTK.jar')
editPlot_path=os.path.join(os.path.dirname(os.path.dirname(__file__)),'bsaseq/editplot.py')

bsacalc_path=os.path.join(os.path.dirname(os.path.dirname(__file__)),'bsaseq/bsacalc.py')
bsapeak_path=os.path.join(os.path.dirname(os.path.dirname(__file__)),'bsaseq/bsapeak.py')
bsadraw_path=os.path.join(os.path.dirname(os.path.dirname(__file__)),'bsaseq/bsadraw.py')
count_allele_for4grp_bsa_path=os.path.join(os.path.dirname(os.path.dirname(__file__)),'bsaseq/count_allele_for4grp_bsa.py')
find_hom_nX_path=os.path.join(os.path.dirname(os.path.dirname(__file__)),'bsaseq/find_hom_nX.py')
find_marker_path=os.path.join(os.path.dirname(os.path.dirname(__file__)),'bsaseq/find_marker.py')
sort_chr_path=os.path.join(os.path.dirname(os.path.dirname(__file__)),'bsaseq/sort_chr.py')
find_4cell_path=os.path.join(os.path.dirname(os.path.dirname(__file__)),'bsaseq/find_4cell.py')
freq_diff_path=os.path.join(os.path.dirname(os.path.dirname(__file__)), 'bsaseq/freq_diff.r')
pileup_to_vcf_path=os.path.join(os.path.dirname(os.path.dirname(__file__)), 'bsaseq/pileup_to_vcf.py')
#combine_avinput_function3_path=os.path.join(os.path.dirname(os.path.dirname(__file__)), 'bsaseq/combine_avinput_function3.py')
annotate_variation_path=os.path.join(os.path.dirname(os.path.dirname(__file__)), 'bsaseq/annotate_variation.pl')
convert2annovar_path=os.path.join(os.path.dirname(os.path.dirname(__file__)), 'bsaseq/convert2annovar.pl')
bsa_ann_path=os.path.join(os.path.dirname(os.path.dirname(__file__)), 'bsaseq/bsa_ann3.py')
SNAP_path=os.path.join(os.path.dirname(os.path.dirname(__file__)), 'bsaseq/scoreNonSynSNPS.pl')
create_peak_ann_path=os.path.join(os.path.dirname(os.path.dirname(__file__)), 'bsaseq/create_peak_ann.py')
create_region_ann_path=os.path.join(os.path.dirname(os.path.dirname(__file__)), 'bsaseq/create_region_ann.py')
sc_chr_length_path=os.path.join(os.path.dirname(os.path.dirname(__file__)),'genome/sc_chr.length')
sc64_gtf_path= os.path.join(os.path.dirname(os.path.dirname(__file__)),'genome/sc64.gtf')
sc64_fa_path= os.path.join(os.path.dirname(os.path.dirname(__file__)),'genome/sc64.fa')
sc64_bed_path= os.path.join(os.path.dirname(os.path.dirname(__file__)),'genome/sc64.bed')
sacCer3_folder_path=os.path.join(os.path.dirname(os.path.dirname(__file__)),'genome/sacCer3')
resequencing6_path=os.path.join(os.path.dirname(os.path.dirname(__file__)),'bsaseq/resequencing6.py')

@celery.task(base=LogErrorsTask,bind=True)
def long_task_bam2(self,output_folder,url_server,token,input_type,numbermap,prefix,bulk_design,P1bamfile,P2bamfile,Hbamlist,Lbamlist,cov_depth,kernel,width,chrnum,noraw):
    os.mkdir(os.path.join(output_folder,token)) 
    print os.path.join(output_folder,token)
    user_email=os.path.basename(output_folder)
#identify genetic marker and make Map file
    mpileup_file=os.path.join(output_folder,token,prefix+'.mpileup')
    cmd1='samtools_0.1.18 mpileup -P ILLUMINA  -f  %s %s %s %s %s >%s'%(sc64_fa_path,P1bamfile,P2bamfile,Hbamlist,Lbamlist,mpileup_file)
    print cmd1
    os.system(cmd1)
    self.update_state(state='PROGRESS',meta={'current': cmd1, 'status':'PROGRESS at cmd1','progress_bar':0.05})
    count_file=mpileup_file+'.count'
    cmd2='python2.7 %s %s %s >%s'%(count_allele_for4grp_bsa_path,mpileup_file,2+2*numbermap,count_file)
    print cmd2
    os.system(cmd2)
    self.update_state(state='PROGRESS',meta={'current': cmd2, 'status':'PROGRESS at cmd2','progress_bar':0.1})
    os.remove(mpileup_file)
    hom_file=count_file+'.'+str(cov_depth)+'Xhom'
    cmd3='python2.7 %s %s %s >%s'%(find_hom_nX_path,count_file,str(cov_depth),hom_file)
    print cmd3
    os.system(cmd3)
    self.update_state(state='PROGRESS',meta={'current': cmd3, 'status':'PROGRESS at cmd3','progress_bar':0.15})
    os.remove(count_file)
    marker_file=hom_file+'.marker'
    cmd4='python2.7 %s %s >%s'%(find_marker_path,hom_file,marker_file)
    print cmd4
    os.system(cmd4)
    self.update_state(state='PROGRESS',meta={'current': cmd4, 'status':'PROGRESS at cmd4','progress_bar':0.20})
    os.remove(hom_file)
    chr_sorted_file=marker_file+'.sorted'
    cmd5='python2.7 %s %s >%s'%(sort_chr_path,marker_file,chr_sorted_file)
    print cmd5
    os.system(cmd5)
    self.update_state(state='PROGRESS',meta={'current': cmd5, 'status':'PROGRESS at cmd5','progress_bar':0.25})
    os.remove(marker_file)
    four_cell_file=chr_sorted_file+'.4cell'
    cmd6='python2.7 %s %s %d %d >%s'%(find_4cell_path,chr_sorted_file,int(numbermap)*2+2,int(bulk_design),four_cell_file)
    print cmd6
    os.system(cmd6)
    self.update_state(state='PROGRESS',meta={'current': cmd6, 'status':'PROGRESS at cmd6','progress_bar':0.30})
    os.remove(chr_sorted_file)
    MPass_file=four_cell_file+'.MPass'
    cmd7='grep "MPass" %s >%s'%(four_cell_file,MPass_file)
    print cmd7
    os.system(cmd7)
    self.update_state(state='PROGRESS',meta={'current': cmd7, 'status':'PROGRESS at cmd7','progress_bar':0.33})
    os.remove(four_cell_file)
    Hmaplist=[]
    Lmaplist=[]
    for i in range(1,numbermap+1):
        out_HMap_file=MPass_file+'.H'+str(i)+'.map'
        cmd8='cut -f1,2,%d,%d %s> %s'%((int(numbermap)+int(numbermap)+2)*8+7+2*i-1,(int(numbermap)+int(numbermap)+2)*8+7+2*i,MPass_file,out_HMap_file)
        print cmd8
        os.system(cmd8)
        self.update_state(state='PROGRESS',meta={'current': cmd8, 'status':'PROGRESS at cmd8','progress_bar':0.35})
        Hmaplist.append(out_HMap_file)
    for i in range(1,numbermap+1):
        out_LMap_file=MPass_file+'.L'+str(i)+'.map'
        cmd9='cut -f1,2,%d,%d %s> %s'%((int(numbermap)+int(numbermap)+2)*8+7+2*int(numbermap)+2*i-1,(int(numbermap)+int(numbermap)+2)*8+7+2*int(numbermap)+2*i,MPass_file,out_LMap_file)
        print cmd9
        os.system(cmd9)
        self.update_state(state='PROGRESS',meta={'current': cmd9, 'status':'PROGRESS at cmd9','progress_bar':0.38})
        Lmaplist.append(out_LMap_file)
    os.remove(MPass_file)
#Calculate G
    G_file=os.path.join(output_folder,token,prefix+'.G')
    cmd10='python2.7 %s -L %s -H %s -w %d -k %s -o %s'%(bsacalc_path,' '.join(Lmaplist),' '.join(Hmaplist),int(width),kernel,G_file)
    print cmd10
    output10=TIMEOUT_COMMAND(cmd10, 60)
    if not output10:
        self.update_state(state='FAILED',meta={'current': cmd10, 'status': 'FAILED at cmd10'})
    else:
        output10=' '.join(output10)
    self.update_state(state='PROGRESS',meta={'current': cmd10, 'status':'PROGRESS at cmd10','progress_bar':0.4})
    cmd11='python2.7 %s %s %s %s'%(bsapeak_path,G_file,os.path.join(output_folder,token),prefix)
    print cmd11
    output11=TIMEOUT_COMMAND(cmd11, 60)
    if not output11:
        self.update_state(state='FAILED',meta={'current': cmd11, 'status': 'FAILED at cmd11'})
    else:
        output11=' '.join(output11)
    self.update_state(state='PROGRESS',meta={'current': cmd11, 'status':'PROGRESS at cmd11','progress_bar':0.43})
    pcutoff, gcutoff,peak_file,region_file= output11.rstrip().split(' ')
    pcutoff=float(pcutoff)
    gcutoff=float(gcutoff)
    print pcutoff, gcutoff
    drawed_pdf=os.path.join(os.path.join(output_folder,token),prefix+'.pdf')
    if noraw=='no':
        cmd12='python2.7 %s -g %s -c %s --ylim 0 30  -o %s --noraw --threshold %d -n %s'%(bsadraw_path,G_file,sc_chr_length_path,drawed_pdf,gcutoff,chrnum)
    else:
        cmd12='python2.7 %s -g %s -c %s --ylim 0 30  -o %s --threshold %d -n %s'%(bsadraw_path,G_file,sc_chr_length_path,drawed_pdf,gcutoff,chrnum)
    print cmd12
    os.system(cmd12)
    self.update_state(state='PROGRESS',meta={'current': cmd12, 'status':'PROGRESS at cmd12','progress_bar':0.45})
    Hmap_freq_file=os.path.join(os.path.join(output_folder,token),prefix+'_Hmap_freq.txt')
    Lmap_freq_file=os.path.join(os.path.join(output_folder,token),prefix+'_Lmap_freq.txt')
    freq_file= os.path.join(os.path.join(output_folder,token),prefix+'_freq.txt')
    cmd15="paste %s | awk '{ s=$3;p=$4; for(i=2; i<=NF/4; i+=1) {s+=$i*4-1;p+=$i*4}; print $1,$2, s,p }'|awk '{if ($3+$4==0)print $1,$2,$3/($3+$4+1);else print $1,$2,$3/($3+$4);}'>%s"%(' '.join(Hmaplist),Hmap_freq_file)
    print cmd15
    os.system(cmd15)
    self.update_state(state='PROGRESS',meta={'current': cmd15, 'status':'PROGRESS at cmd15','progress_bar':0.48})
    cmd16="paste %s | awk '{ s=$3;p=$4; for(i=2; i<=NF/4; i+=1) {s+=$i*4-1;p+=$i*4}; print $1,$2, s,p }'|awk '{if ($3+$4==0)print $1,$2,$3/($3+$4+1);else print $1,$2,$3/($3+$4);}'>%s"%(' '.join(Lmaplist),Lmap_freq_file)
    print cmd16
    os.system(cmd16)
    self.update_state(state='PROGRESS',meta={'current': cmd16, 'status':'PROGRESS at cmd16','progress_bar':0.50})
    cmd17="paste %s  %s  |awk '{print $1,$2,$3-$6}'>%s"%(Hmap_freq_file,Lmap_freq_file,freq_file)
    print cmd17
    os.system(cmd17)
    self.update_state(state='PROGRESS',meta={'current': cmd17, 'status':'PROGRESS at cmd17','progress_bar':0.55})
    os.remove(Hmap_freq_file)
    os.remove(Lmap_freq_file)
    freq_pdf_file= os.path.join(os.path.join(output_folder,token),prefix+'_freq.pdf')
    cmd18="Rscript --vanilla %s %s %s" %(freq_diff_path,freq_file,freq_pdf_file)
    print cmd18
    os.system(cmd18)
    self.update_state(state='PROGRESS',meta={'current': cmd18, 'status':'PROGRESS at cmd18','progress_bar':0.58})
# Annotation here
    P1_mpileup_file= os.path.join(os.path.join(output_folder,token),prefix+'_P1.mpileup')
    cmd19="samtools_0.1.18 mpileup -E -P ILLUMINA  -f %s %s >%s" %(sc64_fa_path,P1bamfile,P1_mpileup_file)
    print cmd19
    os.system(cmd19)
    self.update_state(state='PROGRESS',meta={'current': cmd19, 'status':'PROGRESS at cmd19','progress_bar':0.60})
    P2_mpileup_file= os.path.join(os.path.join(output_folder,token),prefix+'_P2.mpileup')
    cmd20="samtools_0.1.18 mpileup -E -P ILLUMINA  -f %s %s >%s" %(sc64_fa_path,P2bamfile,P2_mpileup_file)
    print cmd20
    os.system(cmd20)
    self.update_state(state='PROGRESS',meta={'current': cmd20, 'status':'PROGRESS at cmd20','progress_bar':0.63})
    P1_vcf_file= os.path.join(os.path.join(output_folder,token),prefix+'_P1.vcf')
    cmd21="python2.7 %s -i %s -o %s" %(pileup_to_vcf_path,P1_mpileup_file,P1_vcf_file)
    print cmd21
    os.system(cmd21)
    self.update_state(state='PROGRESS',meta={'current': cmd21, 'status':'PROGRESS at cmd21','progress_bar':0.65})
    os.remove(P1_mpileup_file)
    P2_vcf_file= os.path.join(os.path.join(output_folder,token),prefix+'_P2.vcf')
    cmd22="python2.7 %s -i %s -o %s" %(pileup_to_vcf_path,P2_mpileup_file,P2_vcf_file)
    print cmd22
    os.system(cmd22)
    self.update_state(state='PROGRESS',meta={'current': cmd22, 'status':'PROGRESS at cmd22','progress_bar':0.68})
    os.remove(P2_mpileup_file)
    P1_avinput_file= os.path.join(os.path.join(output_folder,token),prefix+'_P1.avinput')
    cmd23="perl %s -format vcf4 %s >%s" %(convert2annovar_path,P1_vcf_file,P1_avinput_file)
    print cmd23
    os.system(cmd23)
    self.update_state(state='PROGRESS',meta={'current': cmd23, 'status':'PROGRESS at cmd23','progress_bar':0.7})
    os.remove(P1_vcf_file)
    P2_avinput_file= os.path.join(os.path.join(output_folder,token),prefix+'_P2.avinput')
    cmd24="perl %s -format vcf4 %s >%s" %(convert2annovar_path,P2_vcf_file,P2_avinput_file)
    print cmd24
    os.system(cmd24)
    self.update_state(state='PROGRESS',meta={'current': cmd24, 'status':'PROGRESS at cmd24','progress_bar':0.73})
    os.remove(P2_vcf_file)
    P1_variant_function_file= P1_avinput_file+'.variant_function'
    P1_exonic_variant_function_file= P1_avinput_file+'.exonic_variant_function'    
    cmd25="perl %s -buildver sacCer3 %s %s" %(annotate_variation_path,P1_avinput_file,sacCer3_folder_path)
    print cmd25
    os.system(cmd25)
    self.update_state(state='PROGRESS',meta={'current': cmd25, 'status':'PROGRESS at cmd25','progress_bar':0.75})
    os.remove(P1_avinput_file)
    P2_variant_function_file= P2_avinput_file+'.variant_function'
    P2_exonic_variant_function_file= P2_avinput_file+'.exonic_variant_function'    
    cmd26="perl %s -buildver sacCer3 %s %s" %(annotate_variation_path,P2_avinput_file,sacCer3_folder_path)
    print cmd26
    os.system(cmd26)
    self.update_state(state='PROGRESS',meta={'current': cmd26, 'status':'PROGRESS at cmd26','progress_bar':0.80})
    os.remove(P2_avinput_file)
    P1_P2_combined_variant_function_file= os.path.join(os.path.join(output_folder,token),prefix+'_P1_P2_combined_variant_function.txt')
    P1_P2_combined_exonic_variant_function_file= os.path.join(os.path.join(output_folder,token),prefix+'_P1_P2_combined_exonic_variant_function.txt')    
    snapinputfile=os.path.join(os.path.join(output_folder,token),prefix+'.SNAP')
    snapoutputfile=os.path.join(os.path.join(output_folder,token),prefix+'.SNAP.out')
    cmd27="python2.7 %s -i %s -j %s -se %s -si %s -so %s -o %s" %(bsa_ann_path,P1_exonic_variant_function_file,P2_exonic_variant_function_file,SNAP_path,snapinputfile,snapoutputfile,P1_P2_combined_exonic_variant_function_file)
    print cmd27
    os.system(cmd27)
    os.remove(snapinputfile)
    os.remove(snapoutputfile)
    self.update_state(state='PROGRESS',meta={'current': cmd27, 'status':'PROGRESS at cmd27','progress_bar':0.85})
    os.remove(P1_variant_function_file)
    os.remove(P2_variant_function_file)
    os.remove(P1_exonic_variant_function_file)
    os.remove(P2_exonic_variant_function_file)
    peak_anno_file=os.path.join(os.path.join(output_folder,token),prefix+'_peak_ann.txt')
    cmd28="python2.7 %s -i %s -j %s  -o %s" %(create_peak_ann_path,P1_P2_combined_exonic_variant_function_file,peak_file,peak_anno_file)
    print cmd28
    os.system(cmd28)
    self.update_state(state='PROGRESS',meta={'current': cmd28, 'status':'PROGRESS at cmd28','progress_bar':0.88})
    region_anno_file=os.path.join(os.path.join(output_folder,token),prefix+'_region_ann.txt')
    cmd29="python2.7 %s -i %s -j %s  -o %s" %(create_region_ann_path,P1_P2_combined_exonic_variant_function_file,region_file,region_anno_file)
    print cmd29
    os.system(cmd29)
    self.update_state(state='PROGRESS',meta={'current': cmd29, 'status':'PROGRESS at cmd29','progress_bar':0.90})
# remake annotation table for show  
    pd.set_option('display.max_colwidth', -1)  
    files1=Files(ftype1='R',\
            ftype2='Bam',\
            ftype3='Txt',\
            ffolder=token,\
            fname=os.path.basename(G_file),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(G_file))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(G_file)),\
            view_link=os.path.join(os.path.dirname(url_server),'show_G_file_page',token,  os.path.basename(G_file),prefix+'_threshold.txt') )
    files2=Files(ftype1='R',\
            ftype2='Bam',\
            ftype3='Txt',\
            ffolder=token,\
            fname=os.path.basename(freq_file),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(freq_file))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(freq_file)),\
            view_link=os.path.join(os.path.dirname(url_server),'show_freq_file_page', token, os.path.basename(freq_file)) )
    files3=Files(ftype1='R',\
            ftype2='Bam',\
            ftype3='Txt',\
            ffolder=token,\
            fname=os.path.basename(peak_file),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(peak_file))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(peak_file)),\
            view_link=os.path.join(os.path.dirname(url_server),'show_peak_page' ,token, os.path.basename(peak_file)))
    files4=Files(ftype1='R',\
            ftype2='Bam',\
            ftype3='Txt',\
            ffolder=token,\
            fname=os.path.basename(region_file),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(region_file))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(region_file)),\
            view_link=os.path.join(os.path.dirname(url_server),'show_region_page' ,token, os.path.basename(region_file)))
    files5=Files(ftype1='R',\
            ftype2='Bam',\
            ftype3='Txt',\
            ffolder=token,\
            fname=prefix+'_threshold.txt',\
            fsize=get_file_size(os.path.join(output_folder,token,prefix+'_threshold.txt')),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, prefix+'_threshold.txt'),\
            view_link=os.path.join(os.path.dirname(url_server),'show_threshold_page' ,token, prefix+'_threshold.txt'))
    files6=Files(ftype1='R',\
        ftype2='Bam',\
        ftype3='Pdf',\
        ffolder=token,\
        fname=os.path.basename(drawed_pdf),\
        fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(drawed_pdf))),\
        owners_name=User.query.filter_by(email=user_email).first().username,\
        owners=User.query.filter_by(email=user_email).first().id,\
        link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(drawed_pdf)),\
        view_link=os.path.join(os.path.dirname(url_server), 'downloadresults',token, os.path.basename(drawed_pdf)) )
    files7=Files(ftype1='R',\
        ftype2='Bam',\
        ftype3='Pdf',\
        ffolder=token,\
        fname=os.path.basename(freq_pdf_file),\
        fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(freq_pdf_file))),\
        owners_name=User.query.filter_by(email=user_email).first().username,\
        owners=User.query.filter_by(email=user_email).first().id,\
        link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(freq_pdf_file)),\
        view_link=os.path.join(os.path.dirname(url_server),'downloadresults',token,os.path.basename(freq_pdf_file)) )
    files8=Files(ftype1='R',\
        ftype2='Bam',\
        ftype3='Txt',\
        ffolder=token,\
        fname=os.path.basename(peak_anno_file),\
        fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(peak_anno_file))),\
        owners_name=User.query.filter_by(email=user_email).first().username,\
        owners=User.query.filter_by(email=user_email).first().id,\
        link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(peak_anno_file)),\
        view_link=os.path.join(os.path.dirname(url_server), 'show_peak_anno_tables',token, os.path.basename(peak_anno_file)))            
    files9=Files(ftype1='R',\
        ftype2='Bam',\
        ftype3='Txt',\
        ffolder=token,\
        fname=os.path.basename(region_anno_file),\
        fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(region_anno_file))),\
        owners_name=User.query.filter_by(email=user_email).first().username,\
        owners=User.query.filter_by(email=user_email).first().id,\
        link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(region_anno_file)),\
        view_link=os.path.join(os.path.dirname(url_server), 'show_region_anno_tables',token, os.path.basename(region_anno_file)))
    files10=Files(ftype1='R',\
        ftype2='Bam',\
        ftype3='Txt',\
        ffolder=token,\
        fname=os.path.basename(P1_P2_combined_exonic_variant_function_file),\
        fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(P1_P2_combined_exonic_variant_function_file))),\
        owners_name=User.query.filter_by(email=user_email).first().username,\
        owners=User.query.filter_by(email=user_email).first().id,\
        link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(P1_P2_combined_exonic_variant_function_file)),\
        view_link=os.path.join(os.path.dirname(url_server), 'show_tables',token, os.path.basename(P1_P2_combined_exonic_variant_function_file)) ) 
    for i in Lmaplist:
        files11=Files(ftype1='R',\
        ftype2='Bam',\
        ftype3='Txt',\
        ffolder=token,\
        fname=os.path.basename(i),\
        fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(i))),\
        owners_name=User.query.filter_by(email=user_email).first().username,\
        owners=User.query.filter_by(email=user_email).first().id,\
        link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(i)),\
        view_link=os.path.join(os.path.dirname(url_server), 'show_mapfile_tables_page',token, os.path.basename(i)))
        db.session.add(files11)  
    for i in Hmaplist:
        files12=Files(ftype1='R',\
        ftype2='Bam',\
        ftype3='Txt',\
        ffolder=token,\
        fname=os.path.basename(i),\
        fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(i))),\
        owners_name=User.query.filter_by(email=user_email).first().username,\
        owners=User.query.filter_by(email=user_email).first().id,\
        link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(i)),\
        view_link=os.path.join(os.path.dirname(url_server), 'show_mapfile_tables_page',token, os.path.basename(i)) )
        db.session.add(files12)  
    db.session.add(files1)
    db.session.add(files2)
    db.session.add(files3)
    db.session.add(files4)
    db.session.add(files5)
    db.session.add(files6)
    db.session.add(files7)
    db.session.add(files8)
    db.session.add(files9)
    db.session.add(files10)
    task_db=Tasks.query.filter_by(owners=User.query.filter_by(email=user_email).first().id).filter_by(token_id=token).first()
    if task_db:
        task_db.status='Done'
        task_db.end_time=datetime.utcnow()
        duration_time_in_secondes=(task_db.end_time-task_db.create_time).total_seconds()
        task_db.duration_time=str('%d:%d:%d'%(duration_time_in_secondes/3600,duration_time_in_secondes% 3600/60,duration_time_in_secondes% 3600%60))
        db.session.add(task_db)
    db.session.commit()
    send_email(user_email, 'Your job (%s) done!'%(str(token)),\
                   'auth/email/task_done', url_server=url_server,user=user_email, token=str(token))    
    return {'current': 'Task completed!','result':{'input_type':input_type,'pcutoff':pcutoff,'gcutoff':gcutoff,'token':token,'G_file': os.path.basename(G_file), 'pdf_file': os.path.basename(drawed_pdf), 'peak_anno_file': os.path.basename(peak_anno_file),'region_anno_file':os.path.basename(region_anno_file),'freq_pdf_file':os.path.basename(freq_pdf_file),'P1_P2_combined_exonic_variant_function_file':os.path.basename(P1_P2_combined_exonic_variant_function_file),'freq_file':os.path.basename(freq_file),'peak_file':os.path.basename(peak_file),'region_file':os.path.basename(region_file),'threshold_file':prefix+'_threshold.txt'},'status':'Task completed!','progress_bar':1}

    
@celery.task(base=LogErrorsTask,bind=True)
def long_task_bam1(self,output_folder,url_server,token,input_type,numbermap,prefix,bulk_design,P1bamfile,P2bamfile,bamlist,cov_depth,kernel,width,chrnum,noraw):
    os.mkdir(os.path.join(output_folder,token)) 
    user_email=os.path.basename(output_folder)
#identify genetic marker and make Map file
    mpileup_file=os.path.join(output_folder,token,prefix+'.mpileup')
    cmd1='samtools_0.1.18 mpileup -P ILLUMINA  -f  %s %s %s %s >%s'%(sc64_fa_path,P1bamfile,P2bamfile,bamlist,mpileup_file)
    print cmd1
    os.system(cmd1)
    self.update_state(state='PROGRESS',meta={'current': cmd1, 'status':'PROGRESS at cmd1','progress_bar':0.05})
    count_file=mpileup_file+'.count'
    cmd2='python2.7 %s %s %s >%s'%(count_allele_for4grp_bsa_path,mpileup_file,2+numbermap,count_file)
    print cmd2
    os.system(cmd2)
    self.update_state(state='PROGRESS',meta={'current': cmd2, 'status':'PROGRESS at cmd2','progress_bar':0.1})
    os.remove(mpileup_file)
    hom_file=count_file+'.'+str(cov_depth)+'Xhom'
    cmd3='python2.7 %s %s %s >%s'%(find_hom_nX_path,count_file,str(cov_depth),hom_file)
    print cmd3
    os.system(cmd3)
    self.update_state(state='PROGRESS',meta={'current': cmd3, 'status':'PROGRESS at cmd3','progress_bar':0.15})
    os.remove(count_file)
    marker_file=hom_file+'.marker'
    cmd4='python2.7 %s %s >%s'%(find_marker_path,hom_file,marker_file)
    print cmd4
    os.system(cmd4)
    self.update_state(state='PROGRESS',meta={'current': cmd4, 'status':'PROGRESS at cmd4','progress_bar':0.20})
    os.remove(hom_file)
    chr_sorted_file=marker_file+'.sorted'
    cmd5='python2.7 %s %s >%s'%(sort_chr_path,marker_file,chr_sorted_file)
    print cmd5
    os.system(cmd5)
    self.update_state(state='PROGRESS',meta={'current': cmd5, 'status':'PROGRESS at cmd5','progress_bar':0.25})
    os.remove(marker_file)
    four_cell_file=chr_sorted_file+'.4cell'
    cmd6='python2.7 %s %s %d %d >%s'%(find_4cell_path,chr_sorted_file,int(numbermap)+2,int(bulk_design),four_cell_file)
    print cmd6
    os.system(cmd6)
    self.update_state(state='PROGRESS',meta={'current': cmd6, 'status':'PROGRESS at cmd6','progress_bar':0.30})
    os.remove(chr_sorted_file)
    MPass_file=four_cell_file+'.MPass'
    cmd7='grep "MPass" %s >%s'%(four_cell_file,MPass_file)
    print cmd7
    os.system(cmd7)
    self.update_state(state='PROGRESS',meta={'current': cmd7, 'status':'PROGRESS at cmd7','progress_bar':0.33})
    os.remove(four_cell_file)
    maplist=[]
    for i in range(1,numbermap+1):
        out_Map_file=MPass_file+'.B'+str(i)+'.map'
        cmd8='cut -f1,2,%d,%d %s> %s'%((int(numbermap)+2)*8+7+2*i-1,(int(numbermap)+2)*8+7+2*i,MPass_file,out_Map_file)
        print cmd8
        os.system(cmd8)
        self.update_state(state='PROGRESS',meta={'current': cmd8, 'status':'PROGRESS at cmd8','progress_bar':0.38})
        maplist.append(out_Map_file)
    os.remove(MPass_file)
#Calculate G
    G_file=os.path.join(output_folder,token,prefix+'.G')
    cmd10='python2.7 %s -L %s  -H %s -w %d -k %s --onepool -o %s'%(bsacalc_path,' '.join(maplist),' '.join(maplist),int(width),kernel,G_file)
    print cmd10
    os.system(cmd10)
    self.update_state(state='PROGRESS',meta={'current': cmd10, 'status':'PROGRESS at cmd10','progress_bar':0.40})
    cmd11='python2.7 %s %s %s %s'%(bsapeak_path,G_file,os.path.join(output_folder,token),prefix)
    print cmd11
    output11=TIMEOUT_COMMAND(cmd11, 300)
    if not output11:
        self.update_state(state='FAILED',meta={'current': cmd11, 'status': 'FAILED at cmd11'})
    else:
        output11=' '.join(output11)
    self.update_state(state='PROGRESS',meta={'current': cmd11, 'status':'PROGRESS at cmd11','progress_bar':0.42})
    pcutoff, gcutoff,peak_file,region_file= output11.rstrip().split(' ')
    pcutoff=float(pcutoff)
    gcutoff=float(gcutoff)
    print pcutoff, gcutoff
    drawed_pdf=os.path.join(output_folder,token,prefix+'.pdf')
    print drawed_pdf
    if noraw=='no':
        cmd12='python2.7 %s -g %s -c %s --ylim 0 30  -o %s --noraw --threshold %d -n %s'%(bsadraw_path,G_file,sc_chr_length_path,drawed_pdf,gcutoff,chrnum)
    else:
        cmd12='python2.7 %s -g %s -c %s --ylim 0 30  -o %s --threshold %d -n %s'%(bsadraw_path,G_file,sc_chr_length_path,drawed_pdf,gcutoff,chrnum)
    print cmd12
    os.system(cmd12)
    self.update_state(state='PROGRESS',meta={'current': cmd12, 'status':'PROGRESS at cmd12','progress_bar':0.45})
    freq_file= os.path.join(output_folder,token,prefix+'_freq.txt')
    cmd15="paste %s | awk '{ s=$3;p=$4; for(i=2; i<=NF/4; i+=1) {s+=$i*4-1;p+=$i*4}; print $1,$2, s,p }'|awk '{if ($3+$4==0)print $1,$2,$3/($3+$4+1);else print $1,$2,$3/($3+$4);}'>%s"%(' '.join(maplist),freq_file)
    print cmd15
    os.system(cmd15)
    self.update_state(state='PROGRESS',meta={'current': cmd15, 'status':'PROGRESS at cmd15','progress_bar':0.48})
    freq_pdf_file= os.path.join(output_folder,token,prefix+'_freq.pdf')
    cmd18="Rscript --vanilla %s %s %s" %(freq_diff_path,freq_file,freq_pdf_file)
    print cmd18
    os.system(cmd18)
    self.update_state(state='PROGRESS',meta={'current': cmd18, 'status':'PROGRESS at cmd18','progress_bar':0.50})
# Annotation here
    P1_mpileup_file= os.path.join(output_folder,token,prefix+'_P1.mpileup')
    cmd19="samtools_0.1.18 mpileup -E -P ILLUMINA  -f %s %s >%s" %(sc64_fa_path,P1bamfile,P1_mpileup_file)
    print cmd19
    os.system(cmd19)
    self.update_state(state='PROGRESS',meta={'current': cmd19, 'status':'PROGRESS at cmd19','progress_bar':0.53})
    P2_mpileup_file= os.path.join(output_folder,token,prefix+'_P2.mpileup')
    cmd20="samtools_0.1.18 mpileup -E -P ILLUMINA  -f %s %s >%s" %(sc64_fa_path,P2bamfile,P2_mpileup_file)
    print cmd20
    os.system(cmd20)
    self.update_state(state='PROGRESS',meta={'current': cmd20, 'status':'PROGRESS at cmd20','progress_bar':0.55})
    P1_vcf_file= os.path.join(output_folder,token,prefix+'_P1.vcf')
    cmd21="python2.7 %s -i %s -o %s" %(pileup_to_vcf_path,P1_mpileup_file,P1_vcf_file)
    print cmd21
    os.system(cmd21)
    self.update_state(state='PROGRESS',meta={'current': cmd21, 'status':'PROGRESS at cmd21','progress_bar':0.58})
    os.remove(P1_mpileup_file)
    P2_vcf_file= os.path.join(output_folder,token,prefix+'_P2.vcf')
    cmd22="python2.7 %s -i %s -o %s" %(pileup_to_vcf_path,P2_mpileup_file,P2_vcf_file)
    print cmd22
    os.system(cmd22)
    self.update_state(state='PROGRESS',meta={'current': cmd22, 'status':'PROGRESS at cmd22','progress_bar':0.60})
    os.remove(P2_mpileup_file)
    P1_avinput_file= os.path.join(output_folder,token,prefix+'_P1.avinput')
    cmd23="perl %s -format vcf4 %s >%s" %(convert2annovar_path,P1_vcf_file,P1_avinput_file)
    print cmd23
    os.system(cmd23)
    self.update_state(state='PROGRESS',meta={'current': cmd23, 'status':'PROGRESS at cmd23','progress_bar':0.62})
    os.remove(P1_vcf_file)
    P2_avinput_file= os.path.join(output_folder,token,prefix+'_P2.avinput')
    cmd24="perl %s -format vcf4 %s >%s" %(convert2annovar_path,P2_vcf_file,P2_avinput_file)
    print cmd24
    os.system(cmd24)
    self.update_state(state='PROGRESS',meta={'current': cmd24, 'status':'PROGRESS at cmd24','progress_bar':0.65})
    os.remove(P2_vcf_file)
    P1_variant_function_file= P1_avinput_file+'.variant_function'
    P1_exonic_variant_function_file= P1_avinput_file+'.exonic_variant_function'    
    cmd25="perl %s -buildver sacCer3 %s %s" %(annotate_variation_path,P1_avinput_file,sacCer3_folder_path)
    print cmd25
    os.system(cmd25)
    self.update_state(state='PROGRESS',meta={'current': cmd25, 'status':'PROGRESS at cmd25','progress_bar':0.68})
    os.remove(P1_avinput_file)
    P2_variant_function_file= P2_avinput_file+'.variant_function'
    P2_exonic_variant_function_file= P2_avinput_file+'.exonic_variant_function'    
    cmd26="perl %s -buildver sacCer3 %s %s" %(annotate_variation_path,P2_avinput_file,sacCer3_folder_path)
    print cmd26
    os.system(cmd26)
    self.update_state(state='PROGRESS',meta={'current': cmd26, 'status':'PROGRESS at cmd26','progress_bar':0.70})
    os.remove(P2_avinput_file)
    P1_P2_combined_variant_function_file= os.path.join(output_folder,token,prefix+'_P1_P2_combined_variant_function.txt')
    P1_P2_combined_exonic_variant_function_file= os.path.join(output_folder,token,prefix+'_P1_P2_combined_exonic_variant_function.txt')    
    snapinputfile=os.path.join(os.path.join(output_folder,token),prefix+'.SNAP')
    snapoutputfile=os.path.join(os.path.join(output_folder,token),prefix+'.SNAP.out')
    cmd27="python2.7 %s -i %s -j %s -se %s -si %s -so %s -o %s" %(bsa_ann_path,P1_exonic_variant_function_file,P2_exonic_variant_function_file,SNAP_path,snapinputfile,snapoutputfile,P1_P2_combined_exonic_variant_function_file)
    print cmd27
    os.system(cmd27)
    os.remove(snapinputfile)
    os.remove(snapoutputfile)
    self.update_state(state='PROGRESS',meta={'current': cmd27, 'status':'PROGRESS at cmd27','progress_bar':0.75})
    os.remove(P1_variant_function_file)
    os.remove(P2_variant_function_file)
    os.remove(P1_exonic_variant_function_file)
    os.remove(P2_exonic_variant_function_file)
    peak_anno_file=os.path.join(os.path.join(output_folder,token),prefix+'_peak_ann.txt')
    cmd28="python2.7 %s -i %s -j %s  -o %s" %(create_peak_ann_path,P1_P2_combined_exonic_variant_function_file,peak_file,peak_anno_file)
    print cmd28
    os.system(cmd28)
    self.update_state(state='PROGRESS',meta={'current': cmd28, 'status':'PROGRESS at cmd28','progress_bar':0.80})
    region_anno_file=os.path.join(os.path.join(output_folder,token),prefix+'_region_ann.txt')
    cmd29="python2.7 %s -i %s -j %s  -o %s" %(create_region_ann_path,P1_P2_combined_exonic_variant_function_file,region_file,region_anno_file)
    print cmd29
    os.system(cmd29)
    self.update_state(state='PROGRESS',meta={'current': cmd29, 'status':'PROGRESS at cmd29','progress_bar':0.85})
# remake annotation table for show 
    pd.set_option('display.max_colwidth', -1)  
    files1=Files(ftype1='R',\
            ftype2='Bam',\
            ftype3='Txt',\
            ffolder=token,\
            fname=os.path.basename(G_file),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(G_file))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(G_file)),\
            view_link=os.path.join(os.path.dirname(url_server),'show_G_file_page',token,  os.path.basename(G_file),prefix+'_threshold.txt')  )
    files2=Files(ftype1='R',\
            ftype2='Bam',\
            ftype3='Txt',\
            ffolder=token,\
            fname=os.path.basename(freq_file),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(freq_file))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(freq_file)),\
            view_link=os.path.join(os.path.dirname(url_server),'show_freq_file_page', token, os.path.basename(freq_file)) )
    files3=Files(ftype1='R',\
            ftype2='Bam',\
            ftype3='Txt',\
            ffolder=token,\
            fname=os.path.basename(peak_file),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(peak_file))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(peak_file)),\
            view_link=os.path.join(os.path.dirname(url_server),'show_peak_page' ,token, os.path.basename(peak_file)))
    files4=Files(ftype1='R',\
            ftype2='Bam',\
            ftype3='Txt',\
            ffolder=token,\
            fname=os.path.basename(region_file),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(region_file))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server), 'downloadresults',token,os.path.basename(region_file)),\
            view_link=os.path.join(os.path.dirname(url_server), 'show_region_page' ,token, os.path.basename(region_file)))
    files5=Files(ftype1='R',\
            ftype2='Bam',\
            ftype3='Txt',\
            ffolder=token,\
            fname=prefix+'_threshold.txt',\
            fsize=get_file_size(os.path.join(output_folder,token,prefix+'_threshold.txt')),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, prefix+'_threshold.txt'),\
            view_link=os.path.join(os.path.dirname(url_server),'show_threshold_page' ,token, prefix+'_threshold.txt'))
    files6=Files(ftype1='R',\
        ftype2='Bam',\
        ftype3='Pdf',\
        ffolder=token,\
        fname=os.path.basename(drawed_pdf),\
        fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(drawed_pdf))),\
        owners_name=User.query.filter_by(email=user_email).first().username,\
        owners=User.query.filter_by(email=user_email).first().id,\
        link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(drawed_pdf)),\
        view_link=os.path.join(os.path.dirname(url_server),'downloadresults',token,  os.path.basename(drawed_pdf)) )
    files7=Files(ftype1='R',\
        ftype2='Bam',\
        ftype3='Pdf',\
        ffolder=token,\
        fname=os.path.basename(freq_pdf_file),\
        fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(freq_pdf_file))),\
        owners_name=User.query.filter_by(email=user_email).first().username,\
        owners=User.query.filter_by(email=user_email).first().id,\
        link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(freq_pdf_file)),\
        view_link=os.path.join(os.path.dirname(url_server),'downloadresults',token,os.path.basename(freq_pdf_file)) )
    files8=Files(ftype1='R',\
        ftype2='Bam',\
        ftype3='Txt',\
        ffolder=token,\
        fname=os.path.basename(peak_anno_file),\
        fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(peak_anno_file))),\
        owners_name=User.query.filter_by(email=user_email).first().username,\
        owners=User.query.filter_by(email=user_email).first().id,\
        link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(peak_anno_file)),\
        view_link=os.path.join(os.path.dirname(url_server), 'show_peak_anno_tables',token, os.path.basename(peak_anno_file)))            
    files9=Files(ftype1='R',\
        ftype2='Bam',\
        ftype3='Txt',\
        ffolder=token,\
        fname=os.path.basename(region_anno_file),\
        fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(region_anno_file))),\
        owners_name=User.query.filter_by(email=user_email).first().username,\
        owners=User.query.filter_by(email=user_email).first().id,\
        link=os.path.join(os.path.dirname(url_server), 'downloadresults',token,os.path.basename(region_anno_file)),\
        view_link=os.path.join(os.path.dirname(url_server),'show_region_anno_tables',token,  os.path.basename(region_anno_file)))
    files10=Files(ftype1='R',\
        ftype2='Bam',\
        ftype3='Txt',\
        ffolder=token,\
        fname=os.path.basename(P1_P2_combined_exonic_variant_function_file),
        fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(P1_P2_combined_exonic_variant_function_file))),\
        owners_name=User.query.filter_by(email=user_email).first().username,\
        owners=User.query.filter_by(email=user_email).first().id,\
        link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(P1_P2_combined_exonic_variant_function_file)),\
        view_link=os.path.join(os.path.dirname(url_server), 'show_tables',token, os.path.basename(P1_P2_combined_exonic_variant_function_file)) ) 
    for i in maplist:
        files11=Files(ftype1='R',\
        ftype2='Bam',\
        ftype3='Txt',\
        ffolder=token,\
        fname=os.path.basename(i),\
        fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(i))),\
        owners_name=User.query.filter_by(email=user_email).first().username,\
        owners=User.query.filter_by(email=user_email).first().id,\
        link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(i)),\
        view_link=os.path.join(os.path.dirname(url_server), 'show_mapfile_tables_page',token, os.path.basename(i)))
        db.session.add(files11)
    db.session.add(files1)
    db.session.add(files2)
    db.session.add(files3)
    db.session.add(files4)
    db.session.add(files5)
    db.session.add(files6)
    db.session.add(files7)
    db.session.add(files8)
    db.session.add(files9)
    db.session.add(files10)
    task_db=Tasks.query.filter_by(owners=User.query.filter_by(email=user_email).first().id).filter_by(token_id=token).first()
    if task_db:
        task_db.status='Done'
        task_db.end_time=datetime.utcnow()
        duration_time_in_secondes=(task_db.end_time-task_db.create_time).total_seconds()
        task_db.duration_time=str('%d:%d:%d'%(duration_time_in_secondes/3600,duration_time_in_secondes% 3600/60,duration_time_in_secondes% 3600%60))
        db.session.add(task_db)
    db.session.commit()
    send_email(user_email, 'Your job (%s) done!'%(str(token)),\
                   'auth/email/task_done', url_server=url_server,user=user_email, token=str(token))
    return {'current': 'Task completed!','result':{'input_type':input_type,'pcutoff':pcutoff,'gcutoff':gcutoff,'token':token,'G_file': os.path.basename(G_file), 'pdf_file': os.path.basename(drawed_pdf), 'peak_anno_file': os.path.basename(peak_anno_file),'region_anno_file':os.path.basename(region_anno_file),'freq_pdf_file':os.path.basename(freq_pdf_file),'P1_P2_combined_exonic_variant_function_file':os.path.basename(P1_P2_combined_exonic_variant_function_file),'freq_file':os.path.basename(freq_file),'peak_file':os.path.basename(peak_file),'region_file':os.path.basename(region_file),'threshold_file':prefix+'_threshold.txt'},'status':'Task completed!','progress_bar':1}

    
@celery.task(base=LogErrorsTask,bind=True)
def long_task_fastq2(self,output_folder,url_server,token,input_type,numbermap,prefix,bulk_design,P1fastqfile,P2fastqfile,Hfastqlist,Lfastqlist,readlen,phred,qccheck,trim,lastkeep,rmadapt,ladapter,radapter,minlen,removen,ncutoff,filtq,minq,pminq,rmunpaired,qcstat,insertsize,windowsize,cov_depth,kernel,width,chrnum,noraw):
    user_email=os.path.basename(output_folder)    
    os.mkdir(os.path.join(output_folder,token)) 
    P1fastqfile=json.loads(P1fastqfile)
    P2fastqfile=json.loads(P2fastqfile)
    Hfastqlist=json.loads(Hfastqlist)
    for i in range(len(Hfastqlist)):
        Hfastqlist[i]=[str(j) for j in Hfastqlist[i]]
    Lfastqlist=json.loads(Lfastqlist)
    for i in range(len(Lfastqlist)):
        Lfastqlist[i]=[str(j) for j in Lfastqlist[i]]
    P1samfile=os.path.join(output_folder,token,prefix+'_P1.sam')
    P1bamfile=os.path.join(output_folder,token,prefix+'_P1.bam')
    P1bamfile_sort=os.path.join(output_folder,token,prefix+'_P1.sort.bam')
    tmpsai1=tmpsai2=''
    if len(P1fastqfile)==2:
        if P1fastqfile[0].endswith('.fastq.gz'):
            with gzip.open(P1fastqfile[0], 'rb') as f_in:
                with open(os.path.join(output_folder,token,os.path.basename(os.path.splitext(P1fastqfile[0])[0])), 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)
#            os.remove(P1fastqfile[0])
#            P1fastqfile[0]=os.path.splitext(P1fastqfile[0])[0]
            P1fastqfile[0]=os.path.join(output_folder,token,os.path.basename(os.path.splitext(P1fastqfile[0])[0]))
        if P1fastqfile[1].endswith('.fastq.gz'):
            with gzip.open(P1fastqfile[1], 'rb') as f_in:
                with open(os.path.join(output_folder,token,os.path.basename(os.path.splitext(P1fastqfile[1])[0])), 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)
#            os.remove(P1fastqfile[1])
#            P1fastqfile[1]=os.path.splitext(P1fastqfile[1])[0]
            P1fastqfile[1]=os.path.join(output_folder,token,os.path.basename(os.path.splitext(P1fastqfile[1])[0]))
        print '1.Trim start'
        cmd_qc= 'cd %s\n'%(os.path.join(output_folder,token))
        cmd_qc+='fastx_trimmer -v -f 1 -l 75 -i %s -o %s -Q 33\n'%(P1fastqfile[0],os.path.join(output_folder,token,'trimmed_'+os.path.basename(P1fastqfile[0])))
        print cmd_qc
        os.system(cmd_qc)
        m=re.search('app/results',P1fastqfile[0])
        if m:
            os.remove(P1fastqfile[0])
        P1fastqfile[0]=os.path.join(output_folder,token,'trimmed_'+os.path.basename(P1fastqfile[0]))
        cmd_qc= 'cd %s\n'%(os.path.join(output_folder,token))
        cmd_qc+='fastx_trimmer -v -f 1 -l 75 -i %s -o %s -Q 33\n'%(P1fastqfile[1],os.path.join(output_folder,token,'trimmed_'+os.path.basename(P1fastqfile[1])))
        print cmd_qc
        os.system(cmd_qc)
        m=re.search('app/results',P1fastqfile[1])
        if m:
            os.remove(P1fastqfile[1])
        P1fastqfile[1]=os.path.join(output_folder,token,'trimmed_'+os.path.basename(P1fastqfile[1]))
        print '2.Trim finished'
        print '3.removeN starting'
        cmd_rmN= 'cd %s\n'%(os.path.join(output_folder,token))
        cmd_rmN+='python2.7 %s %s 0.1 %s \n'%(rmN_path,P1fastqfile[0],os.path.dirname(P1fastqfile[0]))
        print cmd_rmN
        os.system(cmd_rmN)
        os.remove(P1fastqfile[0])
        os.remove(os.path.join(os.path.dirname(P1fastqfile[0]),'removed_N_'+os.path.basename(P1fastqfile[0])+'.log'))
        P1fastqfile[0]=os.path.join(os.path.dirname(P1fastqfile[0]),'remained_'+os.path.basename(P1fastqfile[0]))
        cmd_rmN= 'cd %s\n'%(os.path.join(output_folder,token))
        cmd_rmN+='python2.7 %s %s 0.1 %s \n'%(rmN_path,P1fastqfile[1],os.path.dirname(P1fastqfile[1]))
        print cmd_rmN
        os.system(cmd_rmN)
        os.remove(P1fastqfile[1])
        os.remove(os.path.join(os.path.dirname(P1fastqfile[1]),'removed_N_'+os.path.basename(P1fastqfile[1])+'.log'))
        P1fastqfile[1]=os.path.join(os.path.dirname(P1fastqfile[1]),'remained_'+os.path.basename(P1fastqfile[1]))
        print '4.rmoveN finished'
        print '5.filterQ starting'
        cmd_filterQ= 'cd %s\n'%(os.path.join(output_folder,token))
        cmd_filterQ+='fastq_quality_filter -v -q 20 -p 80 -i %s -o %s  -Q 33 \n'%(P1fastqfile[0],os.path.join(os.path.dirname(P1fastqfile[0]),'filtQ_'+os.path.basename(P1fastqfile[0])))
        print cmd_filterQ
        os.system(cmd_filterQ)
        os.remove(P1fastqfile[0])
        P1fastqfile[0]=os.path.join(os.path.dirname(P1fastqfile[0]),'filtQ_'+os.path.basename(P1fastqfile[0]))
        cmd_filterQ= 'cd %s\n'%(os.path.join(output_folder,token))
        cmd_filterQ+='fastq_quality_filter -v -q 20 -p 80 -i %s -o %s  -Q 33 \n'%(P1fastqfile[1],os.path.join(os.path.dirname(P1fastqfile[1]),'filtQ_'+os.path.basename(P1fastqfile[1])))
        print cmd_filterQ
        os.system(cmd_filterQ)
        os.remove(P1fastqfile[1])
        P1fastqfile[1]=os.path.join(os.path.dirname(P1fastqfile[1]),'filtQ_'+os.path.basename(P1fastqfile[1]))
        print '6.filterQ finished'
        print '7.rmUnpaired starting'
        cmd_rmUnpaired= 'cd %s\n'%(os.path.join(output_folder,token))
        cmd_rmUnpaired+='python2.7 %s %s %s %s %s \n'%(matePair_path,P1fastqfile[0],P1fastqfile[1],os.path.join(os.path.dirname(P1fastqfile[0]),'clean_'+os.path.basename(P1fastqfile[0])),os.path.join(os.path.dirname(P1fastqfile[1]),'clean_'+os.path.basename(P1fastqfile[1])))
        print cmd_rmUnpaired
        os.system(cmd_rmUnpaired)
        os.remove(P1fastqfile[0])
        os.remove(P1fastqfile[1])
        P1fastqfile[0]=os.path.join(os.path.dirname(P1fastqfile[0]),'clean_'+os.path.basename(P1fastqfile[0]))
        P1fastqfile[1]=os.path.join(os.path.dirname(P1fastqfile[1]),'clean_'+os.path.basename(P1fastqfile[1]))
        print  '8.rmUnpaired finished'
        tmpsai1=os.path.splitext(P1fastqfile[0])[0]+'.sai'
        tmpsai2=os.path.splitext(P1fastqfile[1])[0]+'.sai'        
        cmd_bwa= 'cd %s\n'%(os.path.join(output_folder,token))
#        cmd_bwa+='bwa mem  -R \"@RG\\tID:USER\\tLB:USER\\tPL:illumina\\tSM:USER\\tPI:USER\" %s %s %s >%s\n'%(sc64_fa_path,P1fastqfile[0],P1fastqfile[1],P1samfile)
        cmd_bwa+='bwa_0_7_13 aln  -n 0.04 -o 1 -e -1 -m 100000 -t 6 -q 20  %s %s>%s \n'%(sc64_fa_path,P1fastqfile[0],tmpsai1)
        cmd_bwa+='bwa_0_7_13 aln  -n 0.04 -o 1 -e -1 -m 100000 -t 6 -q 20  %s %s>%s \n'%(sc64_fa_path,P1fastqfile[1],tmpsai2)
#        print sc64_fa_path,tmpsai1,tmpsai2,P1fastqfile[0],P1fastqfile[1],P1samfile
        cmd_bwa+='bwa_0_7_13 sampe -r "@RG\tID:YO486\tLB:YO486\tPL:illumina\tSM:YO486\tPI:400" -a 400 %s %s %s %s %s >%s \n'%(sc64_fa_path,tmpsai1,tmpsai2,P1fastqfile[0],P1fastqfile[1],P1samfile)        
        cmd_bwa+='samtools_0.1.18 view -Sb %s > %s\n'%(P1samfile,P1bamfile)
        cmd_bwa+='samtools_0.1.18 sort %s %s \n'%(P1bamfile,prefix+'_P1.sort')
    else:
        if P1fastqfile[0].endswith('.fastq.gz'):
            with gzip.open(P1fastqfile[0], 'rb') as f_in:
                with open(os.path.join(output_folder,token,os.path.basename(os.path.splitext(P1fastqfile[0])[0])), 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)
#            os.remove(P1fastqfile[0])
#            P1fastqfile[0]=os.path.splitext(P1fastqfile[0])[0]
            P1fastqfile[0]=os.path.join(output_folder,token,os.path.basename(os.path.splitext(P1fastqfile[0])[0])) 
        cmd_bwa= 'cd %s\n'%(os.path.join(output_folder,token))
        cmd_bwa+='bwa mem -R \"@RG\\tID:USER\\tLB:USER\\tPL:illumina\\tSM:USER\\tPI:USER\" %s %s >%s\n'%(sc64_fa_path,P1fastqfile[0],P1samfile)
        cmd_bwa+='samtools_0.1.18 view -Sb %s > %s\n'%(P1samfile,P1bamfile)
        cmd_bwa+='samtools_0.1.18 sort %s %s \n'%(P1bamfile,prefix+'_P1.sort')
    print cmd_bwa
    os.system(cmd_bwa)
    for i in P1fastqfile:
        os.remove(i)
    os.remove(P1samfile)
    os.remove(P1bamfile)
    if os.path.isfile(tmpsai1):
        os.remove(tmpsai1)
    if os.path.isfile(tmpsai2):
        os.remove(tmpsai2)
    P1bamfile=P1bamfile_sort
    print '9.rmdup start'
    cmd_rmdup='cd %s\n'%(os.path.join(output_folder,token))
    cmd_rmdup+='samtools_0.1.18 rmdup -sS %s %s \n'%(P1bamfile,os.path.join(os.path.dirname(P1bamfile),'rmdup_'+os.path.basename(P1bamfile)))
    print cmd_rmdup
    os.system(cmd_rmdup)
    os.remove(P1bamfile)
    P1bamfile=os.path.join(os.path.dirname(P1bamfile),'rmdup_'+os.path.basename(P1bamfile))
    print '10.rmdup finish'    
    print '11.uniq start'
    cmd_uniq='cd %s\n'%(os.path.join(output_folder,token))
    cmd_uniq+="samtools_0.1.18 view -h %s |awk '$0~/^@/||$0~/XT:A:U/||$0~/XT:A:M/{print $0}'| samtools_0.1.18 view -Sb - > %s \n"%(P1bamfile,os.path.join(os.path.dirname(P1bamfile),'uniq_'+os.path.basename(P1bamfile)))
    print cmd_uniq
    os.system(cmd_uniq)
    os.remove(P1bamfile)
    P1bamfile=os.path.join(os.path.dirname(P1bamfile),'uniq_'+os.path.basename(P1bamfile))
    print '12.uniq finish'
    self.update_state(state='PROGRESS',meta={'current': cmd_bwa, 'status':'PROGRESS at cmd_bwa','progress_bar':0.05})
    
    P2samfile=os.path.join(output_folder,token,prefix+'_P2.sam')
    P2bamfile=os.path.join(output_folder,token,prefix+'_P2.bam')
    P2bamfile_sort=os.path.join(output_folder,token,prefix+'_P2.sort.bam')
    tmpsai1=tmpsai2=''
    if len(P2fastqfile)==2:
        if P2fastqfile[0].endswith('.fastq.gz'):
            with gzip.open(P2fastqfile[0], 'rb') as f_in:
                with open(os.path.join(output_folder,token,os.path.basename(os.path.splitext(P2fastqfile[0])[0])), 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)
#            os.remove(P2fastqfile[0])
#            P2fastqfile[0]=os.path.splitext(P2fastqfile[0])[0]
            P2fastqfile[0]=os.path.join(output_folder,token,os.path.basename(os.path.splitext(P2fastqfile[0])[0]))
        if P2fastqfile[1].endswith('.fastq.gz'):
            with gzip.open(P2fastqfile[1], 'rb') as f_in:
                with open(os.path.join(output_folder,token,os.path.basename(os.path.splitext(P2fastqfile[1])[0])), 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)
#            os.remove(P2fastqfile[1])
#            P2fastqfile[1]=os.path.splitext(P2fastqfile[1])[0]
            P2fastqfile[1]=os.path.join(output_folder,token,os.path.basename(os.path.splitext(P2fastqfile[1])[0]))
        print '13.Trim start'
        cmd_qc= 'cd %s\n'%(os.path.join(output_folder,token))
        cmd_qc+='fastx_trimmer -v -f 1 -l 75 -i %s -o %s -Q 33\n'%(P2fastqfile[0],os.path.join(output_folder,token,'trimmed_'+os.path.basename(P2fastqfile[0])))
        print cmd_qc
        os.system(cmd_qc)
        m=re.search('app/results',P2fastqfile[0])
        if m:
            os.remove(P2fastqfile[0])
        P2fastqfile[0]=os.path.join(output_folder,token,'trimmed_'+os.path.basename(P2fastqfile[0]))
        cmd_qc= 'cd %s\n'%(os.path.join(output_folder,token))
        cmd_qc+='fastx_trimmer -v -f 1 -l 75 -i %s -o %s -Q 33\n'%(P2fastqfile[1],os.path.join(output_folder,token,'trimmed_'+os.path.basename(P2fastqfile[1])))
        print cmd_qc
        os.system(cmd_qc)
        m=re.search('app/results',P2fastqfile[1])
        if m:
            os.remove(P2fastqfile[1])
        P2fastqfile[1]=os.path.join(output_folder,token,'trimmed_'+os.path.basename(P2fastqfile[1]))
        print '14.Trim finished'
        print '15.removeN starting'
        cmd_rmN= 'cd %s\n'%(os.path.join(output_folder,token))
        cmd_rmN+='python2.7 %s %s 0.1 %s \n'%(rmN_path,P2fastqfile[0],os.path.dirname(P2fastqfile[0]))
        print cmd_rmN
        os.system(cmd_rmN)
        os.remove(P2fastqfile[0])
        os.remove(os.path.join(os.path.dirname(P2fastqfile[0]),'removed_N_'+os.path.basename(P2fastqfile[0])+'.log'))
        P2fastqfile[0]=os.path.join(os.path.dirname(P2fastqfile[0]),'remained_'+os.path.basename(P2fastqfile[0]))
        cmd_rmN= 'cd %s\n'%(os.path.join(output_folder,token))
        cmd_rmN+='python2.7 %s %s 0.1 %s \n'%(rmN_path,P2fastqfile[1],os.path.dirname(P2fastqfile[1]))
        print cmd_rmN
        os.system(cmd_rmN)
        os.remove(P2fastqfile[1])
        os.remove(os.path.join(os.path.dirname(P2fastqfile[1]),'removed_N_'+os.path.basename(P2fastqfile[1])+'.log'))
        P2fastqfile[1]=os.path.join(os.path.dirname(P2fastqfile[1]),'remained_'+os.path.basename(P2fastqfile[1]))
        print '16.rmoveN finished'
        print '17.filterQ starting'
        cmd_filterQ= 'cd %s\n'%(os.path.join(output_folder,token))
        cmd_filterQ+='fastq_quality_filter -v -q 20 -p 80 -i %s -o %s  -Q 33 \n'%(P2fastqfile[0],os.path.join(os.path.dirname(P2fastqfile[0]),'filtQ_'+os.path.basename(P2fastqfile[0])))
        print cmd_filterQ
        os.system(cmd_filterQ)
        os.remove(P2fastqfile[0])
        P2fastqfile[0]=os.path.join(os.path.dirname(P2fastqfile[0]),'filtQ_'+os.path.basename(P2fastqfile[0]))
        cmd_filterQ= 'cd %s\n'%(os.path.join(output_folder,token))
        cmd_filterQ+='fastq_quality_filter -v -q 20 -p 80 -i %s -o %s  -Q 33 \n'%(P2fastqfile[1],os.path.join(os.path.dirname(P2fastqfile[1]),'filtQ_'+os.path.basename(P2fastqfile[1])))
        print cmd_filterQ
        os.system(cmd_filterQ)
        os.remove(P2fastqfile[1])
        P2fastqfile[1]=os.path.join(os.path.dirname(P2fastqfile[1]),'filtQ_'+os.path.basename(P2fastqfile[1]))
        print '18.filterQ finished'
        print '19.rmUnpaired starting'
        cmd_rmUnpaired= 'cd %s\n'%(os.path.join(output_folder,token))
        cmd_rmUnpaired+='python2.7  %s %s %s %s %s \n'%(matePair_path,P2fastqfile[0],P2fastqfile[1],os.path.join(os.path.dirname(P2fastqfile[0]),'clean_'+os.path.basename(P2fastqfile[0])),os.path.join(os.path.dirname(P2fastqfile[1]),'clean_'+os.path.basename(P2fastqfile[1])))
        print cmd_rmUnpaired
        os.system(cmd_rmUnpaired)
        os.remove(P2fastqfile[0])
        os.remove(P2fastqfile[1])
        P2fastqfile[0]=os.path.join(os.path.dirname(P2fastqfile[0]),'clean_'+os.path.basename(P2fastqfile[0]))
        P2fastqfile[1]=os.path.join(os.path.dirname(P2fastqfile[1]),'clean_'+os.path.basename(P2fastqfile[1]))
        print  '20.rmUnpaired finished'
        tmpsai1=os.path.splitext(P2fastqfile[0])[0]+'.sai'
        tmpsai2=os.path.splitext(P2fastqfile[1])[0]+'.sai'
        cmd_bwa= 'cd %s\n'%(os.path.join(output_folder,token))
#        cmd_bwa+='bwa mem -R \"@RG\\tID:USER\\tLB:USER\\tPL:illumina\\tSM:USER\\tPI:USER\" %s %s %s >%s\n'%(sc64_fa_path,P2fastqfile[0],P2fastqfile[1],P2samfile)
        cmd_bwa+='bwa_0_7_13 aln  -n 0.04 -o 1 -e -1 -m 100000 -t 6 -q 20  %s %s>%s \n'%(sc64_fa_path,P2fastqfile[0],tmpsai1)
        cmd_bwa+='bwa_0_7_13 aln  -n 0.04 -o 1 -e -1 -m 100000 -t 6 -q 20  %s %s>%s \n'%(sc64_fa_path,P2fastqfile[1],tmpsai2)
        cmd_bwa+='bwa_0_7_13 sampe -r "@RG\tID:YO486\tLB:YO486\tPL:illumina\tSM:YO486\tPI:400" -a 400 %s %s %s %s %s>%s \n'%(sc64_fa_path,tmpsai1,tmpsai2,P2fastqfile[0],P2fastqfile[1],P2samfile)
        cmd_bwa+='samtools_0.1.18 view -Sb %s > %s\n'%(P2samfile,P2bamfile)
        cmd_bwa+='samtools_0.1.18 sort %s %s \n'%(P2bamfile,prefix+'_P2.sort')
    else:
        if P2fastqfile[0].endswith('.fastq.gz'):
            with gzip.open(P2fastqfile[0], 'rb') as f_in:
                with open(os.path.join(output_folder,token,os.path.basename(os.path.splitext(P2fastqfile[0])[0])), 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)
#            os.remove(P2fastqfile[0])
#            P2fastqfile[0]=os.path.splitext(P2fastqfile[0])[0]
            P2fastqfile[0]=os.path.join(output_folder,token,os.path.basename(os.path.splitext(P2fastqfile[0])[0])) 
        cmd_bwa= 'cd %s\n'%(os.path.join(output_folder,token))
        cmd_bwa+='bwa mem -R \"@RG\\tID:USER\\tLB:USER\\tPL:illumina\\tSM:USER\\tPI:USER\" %s %s >%s\n'%(sc64_fa_path,P2fastqfile[0],P2samfile)
        cmd_bwa+='samtools_0.1.18 view -Sb %s > %s\n'%(P2samfile,P2bamfile)
        cmd_bwa+='samtools_0.1.18 sort %s %s \n'%(P2bamfile,prefix+'_P2.sort')
    print cmd_bwa
    os.system(cmd_bwa)
    for i in P2fastqfile:
        os.remove(i)
    os.remove(P2samfile)
    os.remove(P2bamfile)
    if os.path.isfile(tmpsai1):
        os.remove(tmpsai1)
    if os.path.isfile(tmpsai2):
        os.remove(tmpsai2)
    P2bamfile=P2bamfile_sort
    print '21.rmdup start'
    cmd_rmdup='cd %s\n'%(os.path.join(output_folder,token))
    cmd_rmdup+='samtools_0.1.18 rmdup -sS %s %s \n'%(P2bamfile,os.path.join(os.path.dirname(P2bamfile),'rmdup_'+os.path.basename(P2bamfile)))
    print cmd_rmdup
    os.system(cmd_rmdup)
    os.remove(P2bamfile)
    P2bamfile=os.path.join(os.path.dirname(P2bamfile),'rmdup_'+os.path.basename(P2bamfile))
    print '22.rmdup finish'    
    print '23.uniq start'
    cmd_uniq='cd %s\n'%(os.path.join(output_folder,token))
    cmd_uniq+="samtools_0.1.18 view -h %s |awk '$0~/^@/||$0~/XT:A:U/||$0~/XT:A:M/{print $0}'| samtools_0.1.18 view -Sb - > %s \n"%(P2bamfile,os.path.join(os.path.dirname(P2bamfile),'uniq_'+os.path.basename(P2bamfile)))
    print cmd_uniq
    os.system(cmd_uniq)
    os.remove(P2bamfile)
    P2bamfile=os.path.join(os.path.dirname(P2bamfile),'uniq_'+os.path.basename(P2bamfile))
    print '24.uniq finish'
    self.update_state(state='PROGRESS',meta={'current': cmd_bwa, 'status':'PROGRESS at cmd_bwa','progress_bar':0.10})

    Hbamlist=[]
    for i in range(1,len(Hfastqlist)+1):
        tmpsamfile=os.path.join(output_folder,token,prefix+'_H'+str(i)+'.sam')
        tmpbamfile=os.path.join(output_folder,token,prefix+'_H'+str(i)+'.bam')
        tmpbamfile_sort=os.path.join(output_folder,token,prefix+'_H'+str(i)+'.sort.bam')
        tmpsai1=tmpsai2=''
        if len(Hfastqlist[i-1])==2:
            if Hfastqlist[i-1][0].endswith('.fastq.gz'):
                with gzip.open(Hfastqlist[i-1][0], 'rb') as f_in:
                    with open(os.path.join(output_folder,token,os.path.basename(os.path.splitext(Hfastqlist[i-1][0])[0])), 'wb') as f_out:
                        shutil.copyfileobj(f_in, f_out)
#                os.remove(Hfastqlist[i-1][0])
#                Hfastqlist[i-1][0]=os.path.splitext(Hfastqlist[i-1][0])[0]
                Hfastqlist[i-1][0]=os.path.join(output_folder,token,os.path.basename(os.path.splitext(Hfastqlist[i-1][0])[0]))
            if Hfastqlist[i-1][1].endswith('.fastq.gz'):
                with gzip.open(Hfastqlist[i-1][1], 'rb') as f_in:
                    with open(os.path.join(output_folder,token,os.path.basename(os.path.splitext(Hfastqlist[i-1][1])[0])), 'wb') as f_out:
                        shutil.copyfileobj(f_in, f_out)
#                os.remove(Hfastqlist[i-1][1])
#                Hfastqlist[i-1][1]=os.path.splitext(Hfastqlist[i-1][1])[0]
                Hfastqlist[i-1][1]=os.path.join(output_folder,token,os.path.basename(os.path.splitext(Hfastqlist[i-1][1])[0]))
            cmd_bwa= 'cd %s\n'%(os.path.join(output_folder,token))
            cmd_bwa+='bwa mem -R \"@RG\\tID:USER\\tLB:USER\\tPL:illumina\\tSM:USER\\tPI:USER\" %s %s %s >%s\n'%(sc64_fa_path,Hfastqlist[i-1][0],Hfastqlist[i-1][1],tmpsamfile)
            cmd_bwa+='samtools_0.1.18 view -Sb %s > %s\n'%(tmpsamfile,tmpbamfile)
            cmd_bwa+='samtools_0.1.18 sort %s %s \n'%(tmpbamfile,prefix+'_H'+str(i)+'.sort')
        else:
            if Hfastqlist[i-1][0].endswith('.fastq.gz'):
                with gzip.open(Hfastqlist[i-1][0], 'rb') as f_in:
                    with open(os.path.join(output_folder,token,os.path.basename(os.path.splitext(Hfastqlist[i-1][0])[0])), 'wb') as f_out:
                        shutil.copyfileobj(f_in, f_out)
#                os.remove(Hfastqlist[i-1][0])
#                Hfastqlist[i-1][0]=os.path.splitext(Hfastqlist[i-1][0])[0]
                Hfastqlist[i-1][0]=os.path.join(output_folder,token,os.path.basename(os.path.splitext(Hfastqlist[i-1][0])[0]))
            print '25.Trim start'
            cmd_qc= 'cd %s\n'%(os.path.join(output_folder,token))
            cmd_qc+='fastx_trimmer -v -f 1 -l 75 -i %s -o %s -Q 33\n'%(Hfastqlist[i-1][0],os.path.join(output_folder,token,'trimmed_'+os.path.basename(Hfastqlist[i-1][0])))
            print cmd_qc
            os.system(cmd_qc)
            m=re.search('app/results',Hfastqlist[i-1][0])
            if m:
                os.remove(Hfastqlist[i-1][0])
            Hfastqlist[i-1][0]=os.path.join(output_folder,token,'trimmed_'+os.path.basename(Hfastqlist[i-1][0]))
            print '26.Trim finished'
            print '27.removeN starting'
            cmd_rmN= 'cd %s\n'%(os.path.join(output_folder,token))
            cmd_rmN+='python2.7 %s %s 0.1 %s \n'%(rmN_path,Hfastqlist[i-1][0],os.path.dirname(Hfastqlist[i-1][0]))
            print cmd_rmN
            os.system(cmd_rmN)
            os.remove(Hfastqlist[i-1][0])
            os.remove(os.path.join(os.path.dirname(Hfastqlist[i-1][0]),'removed_N_'+os.path.basename(Hfastqlist[i-1][0])+'.log'))
            Hfastqlist[i-1][0]=os.path.join(os.path.dirname(Hfastqlist[i-1][0]),'remained_'+os.path.basename(Hfastqlist[i-1][0]))
            print '28.rmoveN finished'
            print '29.filterQ starting'
            cmd_filterQ= 'cd %s\n'%(os.path.join(output_folder,token))
            cmd_filterQ+='fastq_quality_filter -v -q 20 -p 80 -i %s -o %s  -Q 33 \n'%(Hfastqlist[i-1][0],os.path.join(os.path.dirname(Hfastqlist[i-1][0]),'filtQ_'+os.path.basename(Hfastqlist[i-1][0])))
            print cmd_filterQ
            os.system(cmd_filterQ)
            os.remove(Hfastqlist[i-1][0])
            Hfastqlist[i-1][0]=os.path.join(os.path.dirname(Hfastqlist[i-1][0]),'filtQ_'+os.path.basename(Hfastqlist[i-1][0]))
            print '30.filterQ finished'  
            tmpsai1=os.path.splitext(Hfastqlist[i-1][0])[0]+'.sai'   
            cmd_bwa= 'cd %s\n'%(os.path.join(output_folder,token))
#            cmd_bwa+='bwa mem -R \"@RG\\tID:USER\\tLB:USER\\tPL:illumina\\tSM:USER\\tPI:USER\" %s %s >%s\n'%(sc64_fa_path,Hfastqlist[i-1][0],tmpsamfile)
            cmd_bwa+='bwa aln  -n 0.04 -o 1 -e -1 -m 100000 -t 6 -q 20  %s %s>%s \n'%(sc64_fa_path,Hfastqlist[i-1][0],tmpsai1)
            cmd_bwa+='bwa samse -r "@RG\tID:YCR_Glu_H\tLB:YCR_Glu_H\tPL:illumina\tSM:YCR_Glu_H\tPI:400"  %s %s %s >%s \n'%(sc64_fa_path,tmpsai1,Hfastqlist[i-1][0],tmpsamfile)
            cmd_bwa+='samtools_0.1.18 view -Sb %s > %s\n'%(tmpsamfile,tmpbamfile)
            cmd_bwa+='samtools_0.1.18 sort %s %s \n'%(tmpbamfile,prefix+'_H'+str(i)+'.sort')
        print cmd_bwa
        os.system(cmd_bwa)
        for i in Hfastqlist[i-1]:
            os.remove(i)
        os.remove(tmpsamfile)
        os.remove(tmpbamfile)
        if os.path.isfile(tmpsai1):
            os.remove(tmpsai1)
        if os.path.isfile(tmpsai2):
            os.remove(tmpsai2)
        tmpbamfile=tmpbamfile_sort
        print '31.rmdup start'
        cmd_rmdup='cd %s\n'%(os.path.join(output_folder,token))
        cmd_rmdup+='samtools_0.1.18 rmdup -sS %s %s \n'%(tmpbamfile_sort,os.path.join(os.path.dirname(tmpbamfile_sort),'rmdup_'+os.path.basename(tmpbamfile_sort)))
        print cmd_rmdup
        os.system(cmd_rmdup)
        os.remove(tmpbamfile_sort)
        tmpbamfile_sort=os.path.join(os.path.dirname(tmpbamfile_sort),'rmdup_'+os.path.basename(tmpbamfile_sort))
        print '32.rmdup finish'
        print '33.uniq start'
        cmd_uniq='cd %s\n'%(os.path.join(output_folder,token))
        cmd_uniq+="samtools_0.1.18 view -h %s |awk '$0~/^@/||$0~/XT:A:U/||$0~/XT:A:M/{print $0}'| samtools_0.1.18 view -Sb - > %s \n"%(tmpbamfile_sort,os.path.join(os.path.dirname(tmpbamfile_sort),'uniq_'+os.path.basename(tmpbamfile_sort)))
        print cmd_uniq
        os.system(cmd_uniq)
        os.remove(tmpbamfile_sort)
        tmpbamfile_sort=os.path.join(os.path.dirname(tmpbamfile_sort),'uniq_'+os.path.basename(tmpbamfile_sort))
        print '34.uniq finish'
        Hbamlist.append(tmpbamfile_sort)
        self.update_state(state='PROGRESS',meta={'current': cmd_bwa, 'status':'PROGRESS at cmd_bwa','progress_bar':0.15})
#    Hbamlist=' '.join(Hbamlist)
    
    Lbamlist=[]
    for i in range(1,len(Lfastqlist)+1):
        tmpsamfile=os.path.join(output_folder,token,prefix+'_L'+str(i)+'.sam')
        tmpbamfile=os.path.join(output_folder,token,prefix+'_L'+str(i)+'.bam')
        tmpbamfile_sort=os.path.join(output_folder,token,prefix+'_L'+str(i)+'.sort.bam')
        tmpsai1=tmpsai2=''
        if len(Lfastqlist[i-1])==2:
            if Lfastqlist[i-1][0].endswith('.fastq.gz'):
                with gzip.open(Lfastqlist[i-1][0], 'rb') as f_in:
                    with open(os.path.join(output_folder,token,os.path.basename(os.path.splitext(Lfastqlist[i-1][0])[0])), 'wb') as f_out:
                        shutil.copyfileobj(f_in, f_out)
#                os.remove(Lfastqlist[i-1][0])
#                Lfastqlist[i-1][0]=os.path.splitext(Lfastqlist[i-1][0])[0]
                Lfastqlist[i-1][0]=os.path.join(output_folder,token,os.path.basename(os.path.splitext(Lfastqlist[i-1][0])[0]))
            if Lfastqlist[i-1][1].endswith('.fastq.gz'):
                with gzip.open(Lfastqlist[i-1][1], 'rb') as f_in:
                    with open(os.path.join(output_folder,token,os.path.basename(os.path.splitext(Lfastqlist[i-1][1])[0])), 'wb') as f_out:
                        shutil.copyfileobj(f_in, f_out)
#                os.remove(Lfastqlist[i-1][1])
#                Lfastqlist[i-1][1]=os.path.splitext(Lfastqlist[i-1][1])[0]
                Lfastqlist[i-1][1]=os.path.join(output_folder,token,os.path.basename(os.path.splitext(Lfastqlist[i-1][1])[0]))
            cmd_bwa= 'cd %s\n'%(os.path.join(output_folder,token))
            cmd_bwa+='bwa mem -R \"@RG\\tID:USER\\tLB:USER\\tPL:illumina\\tSM:USER\\tPI:USER\" %s %s %s >%s\n'%(sc64_fa_path,Lfastqlist[i-1][0],Lfastqlist[i-1][1],tmpsamfile)
            cmd_bwa+='samtools_0.1.18 view -Sb %s > %s\n'%(tmpsamfile,tmpbamfile)
            cmd_bwa+='samtools_0.1.18 sort %s %s \n'%(tmpbamfile,prefix+'_L'+str(i)+'.sort')
        else:
            if Lfastqlist[i-1][0].endswith('.fastq.gz'):
                with gzip.open(Lfastqlist[i-1][0], 'rb') as f_in:
                    with open(os.path.join(output_folder,token,os.path.basename(os.path.splitext(Lfastqlist[i-1][0])[0])), 'wb') as f_out:
                        shutil.copyfileobj(f_in, f_out)
#                os.remove(Lfastqlist[i-1][0])
#                Lfastqlist[i-1][0]=os.path.splitext(Lfastqlist[i-1][0])[0]
                Lfastqlist[i-1][0]=os.path.join(output_folder,token,os.path.basename(os.path.splitext(Lfastqlist[i-1][0])[0]))
            print '35.Trim start'
            cmd_qc= 'cd %s\n'%(os.path.join(output_folder,token))
            cmd_qc+='fastx_trimmer -v -f 1 -l 75 -i %s -o %s -Q 33\n'%(Lfastqlist[i-1][0],os.path.join(output_folder,token,'trimmed_'+os.path.basename(Lfastqlist[i-1][0])))
            print cmd_qc
            os.system(cmd_qc)
            m=re.search('app/results',Lfastqlist[i-1][0])
            if m:
                os.remove(Lfastqlist[i-1][0]) 
            Lfastqlist[i-1][0]=os.path.join(output_folder,token,'trimmed_'+os.path.basename(Lfastqlist[i-1][0]))
            print '36.Trim finished'
            print '37.removeN starting'
            cmd_rmN= 'cd %s\n'%(os.path.join(output_folder,token))
            cmd_rmN+='python2.7 %s %s 0.1 %s \n'%(rmN_path,Lfastqlist[i-1][0],os.path.dirname(Lfastqlist[i-1][0]))
            print cmd_rmN
            os.system(cmd_rmN)
            os.remove(Lfastqlist[i-1][0])
            os.remove(os.path.join(os.path.dirname(Lfastqlist[i-1][0]),'removed_N_'+os.path.basename(Lfastqlist[i-1][0])+'.log'))
            Lfastqlist[i-1][0]=os.path.join(output_folder,token,'remained_'+os.path.basename(Lfastqlist[i-1][0]))
            print '38.rmoveN finished'
            print '39.filterQ starting'
            cmd_filterQ= 'cd %s\n'%(os.path.join(output_folder,token))
            cmd_filterQ+='fastq_quality_filter -v -q 20 -p 80 -i %s -o %s  -Q 33 \n'%(Lfastqlist[i-1][0],os.path.join(os.path.dirname(Lfastqlist[i-1][0]),'filtQ_'+os.path.basename(Lfastqlist[i-1][0])))
            print cmd_filterQ
            os.system(cmd_filterQ)
            os.remove(Lfastqlist[i-1][0])
            Lfastqlist[i-1][0]=os.path.join(os.path.dirname(Lfastqlist[i-1][0]),'filtQ_'+os.path.basename(Lfastqlist[i-1][0]))
            print '40.filterQ finished'
            tmpsai1=os.path.splitext(Lfastqlist[i-1][0])[0]+'.sai'
            cmd_bwa= 'cd %s\n'%(os.path.join(output_folder,token))
#            cmd_bwa+='bwa mem -R \"@RG\\tID:USER\\tLB:USER\\tPL:illumina\\tSM:USER\\tPI:USER\" %s %s >%s\n'%(sc64_fa_path,Lfastqlist[i-1][0],tmpsamfile)
            cmd_bwa+='bwa aln  -n 0.04 -o 1 -e -1 -m 100000 -t 6 -q 20  %s %s > %s \n'%(sc64_fa_path,Lfastqlist[i-1][0],tmpsai1)
            cmd_bwa+='bwa samse -r "@RG\tID:YCR_Glu_H\tLB:YCR_Glu_H\tPL:illumina\tSM:YCR_Glu_H\tPI:400"  %s %s %s >%s \n'%(sc64_fa_path,tmpsai1,Lfastqlist[i-1][0],tmpsamfile)
            cmd_bwa+='samtools_0.1.18 view -Sb %s > %s\n'%(tmpsamfile,tmpbamfile)
            cmd_bwa+='samtools_0.1.18 sort %s %s \n'%(tmpbamfile,prefix+'_L'+str(i)+'.sort')
        print cmd_bwa
        os.system(cmd_bwa)
        for i in Lfastqlist[i-1]:
            os.remove(i)
        os.remove(tmpsamfile)
        os.remove(tmpbamfile)
        if os.path.isfile(tmpsai1):
            os.remove(tmpsai1)
        if os.path.isfile(tmpsai2):
            os.remove(tmpsai2)
        tmpbamfile=tmpbamfile_sort
        print '41.rmdup start'
        cmd_rmdup='cd %s\n'%(os.path.join(output_folder,token))
        cmd_rmdup+='samtools_0.1.18 rmdup -sS %s %s \n'%(tmpbamfile_sort,os.path.join(os.path.dirname(tmpbamfile_sort),'rmdup_'+os.path.basename(tmpbamfile_sort)))
        print cmd_rmdup
        os.system(cmd_rmdup)
        os.remove(tmpbamfile_sort)
        tmpbamfile_sort=os.path.join(os.path.dirname(tmpbamfile_sort),'rmdup_'+os.path.basename(tmpbamfile_sort))
        print '42.rmdup finish'
        print '43.uniq start'
        cmd_uniq='cd %s\n'%(os.path.join(output_folder,token))
        cmd_uniq+="samtools_0.1.18 view -h %s |awk '$0~/^@/||$0~/XT:A:U/||$0~/XT:A:M/{print $0}'| samtools_0.1.18 view -Sb - > %s \n"%(tmpbamfile_sort,os.path.join(os.path.dirname(tmpbamfile_sort),'uniq_'+os.path.basename(tmpbamfile_sort)))
        print cmd_uniq
        os.system(cmd_uniq)
        os.remove(tmpbamfile_sort)
        tmpbamfile_sort=os.path.join(os.path.dirname(tmpbamfile_sort),'uniq_'+os.path.basename(tmpbamfile_sort))
        print '44.uniq finish'
        Lbamlist.append(tmpbamfile_sort)
        self.update_state(state='PROGRESS',meta={'current': cmd_bwa, 'status':'PROGRESS at cmd_bwa','progress_bar':0.20})
#    Lbamlist=' '.join(Lbamlist) 
#identify genetic marker and make Map file
    mpileup_file=os.path.join(output_folder,token,prefix+'.mpileup')
    cmd1='samtools_0.1.18 mpileup -P ILLUMINA  -f  %s %s %s %s %s >%s'%(sc64_fa_path,P1bamfile,P2bamfile,' '.join(Hbamlist),' '.join(Lbamlist),mpileup_file)
    print cmd1
    os.system(cmd1)
    self.update_state(state='PROGRESS',meta={'current': cmd1, 'status':'PROGRESS at cmd1','progress_bar':0.25})
    for i in Hbamlist:
        os.remove(i)
    for i in Lbamlist:
        os.remove(i)
    count_file=mpileup_file+'.count'
    cmd2='python2.7 %s %s %s >%s'%(count_allele_for4grp_bsa_path,mpileup_file,2+2*numbermap,count_file)
    print cmd2
    os.system(cmd2)
    self.update_state(state='PROGRESS',meta={'current': cmd2, 'status':'PROGRESS at cmd2','progress_bar':0.30})
    os.remove(mpileup_file)
    hom_file=count_file+'.'+str(cov_depth)+'Xhom'
    cmd3='python2.7 %s %s %s >%s'%(find_hom_nX_path,count_file,str(cov_depth),hom_file)
    print cmd3
    os.system(cmd3)
    self.update_state(state='PROGRESS',meta={'current': cmd3, 'status':'PROGRESS at cmd3','progress_bar':0.33})
    os.remove(count_file)
    marker_file=hom_file+'.marker'
    cmd4='python2.7 %s %s >%s'%(find_marker_path,hom_file,marker_file)
    print cmd4
    os.system(cmd4)
    self.update_state(state='PROGRESS',meta={'current': cmd4, 'status':'PROGRESS at cmd4','progress_bar':0.35})
    os.remove(hom_file)
    chr_sorted_file=marker_file+'.sorted'
    cmd5='python2.7 %s %s >%s'%(sort_chr_path,marker_file,chr_sorted_file)
    print cmd5
    os.system(cmd5)
    self.update_state(state='PROGRESS',meta={'current': cmd5, 'status':'PROGRESS at cmd5','progress_bar':0.38})
    os.remove(marker_file)
    four_cell_file=chr_sorted_file+'.4cell'
    cmd6='python2.7 %s %s %d %d >%s'%(find_4cell_path,chr_sorted_file,int(numbermap)*2+2,int(bulk_design),four_cell_file)
    print cmd6
    os.system(cmd6)
    self.update_state(state='PROGRESS',meta={'current': cmd6, 'status':'PROGRESS at cmd6','progress_bar':0.40})
    os.remove(chr_sorted_file)
    MPass_file=four_cell_file+'.MPass'
    cmd7='grep "MPass" %s >%s'%(four_cell_file,MPass_file)
    print cmd7
    os.system(cmd7)
    self.update_state(state='PROGRESS',meta={'current': cmd7, 'status':'PROGRESS at cmd7','progress_bar':0.43})
    os.remove(four_cell_file)
    Hmaplist=[]
    Lmaplist=[]
    for i in range(1,numbermap+1):
        out_HMap_file=MPass_file+'.H'+str(i)+'.map'
        cmd8='cut -f1,2,%d,%d %s> %s'%((int(numbermap)+int(numbermap)+2)*8+7+2*i-1,(int(numbermap)+int(numbermap)+2)*8+7+2*i,MPass_file,out_HMap_file)
        print cmd8
        os.system(cmd8)
        self.update_state(state='PROGRESS',meta={'current': cmd8, 'status':'PROGRESS at cmd8','progress_bar':0.45})
        Hmaplist.append(out_HMap_file)
    for i in range(1,numbermap+1):
        out_LMap_file=MPass_file+'.L'+str(i)+'.map'
        cmd9='cut -f1,2,%d,%d %s> %s'%((int(numbermap)+int(numbermap)+2)*8+7+2*int(numbermap)+2*i-1,(int(numbermap)+int(numbermap)+2)*8+7+2*int(numbermap)+2*i,MPass_file,out_LMap_file)
        print cmd9
        os.system(cmd9)
        self.update_state(state='PROGRESS',meta={'current': cmd9, 'status':'PROGRESS at cmd9','progress_bar':0.48})
        Lmaplist.append(out_LMap_file)
    os.remove(MPass_file)
#Calculate G
    G_file=os.path.join(output_folder,token,prefix+'.G')
    cmd10='python2.7 %s -L %s -H %s -w %d -k %s -o %s'%(bsacalc_path,' '.join(Lmaplist),' '.join(Hmaplist),int(width),kernel,G_file)
    print cmd10
    output10=TIMEOUT_COMMAND(cmd10, 60)
    if not output10:
        self.update_state(state='FAILED',meta={'current': cmd10, 'status': 'FAILED at cmd10'})
    else:
        output10=' '.join(output10)
    self.update_state(state='PROGRESS',meta={'current': cmd10, 'status':'PROGRESS at cmd10','progress_bar':0.50})
    cmd11='python2.7 %s %s %s %s'%(bsapeak_path,G_file,os.path.join(output_folder,token),prefix)
    print cmd11
    output11=TIMEOUT_COMMAND(cmd11, 60)
    if not output11:
        self.update_state(state='FAILED',meta={'current': cmd11, 'status': 'FAILED at cmd11'})
    else:
        output11=' '.join(output11)
    self.update_state(state='PROGRESS',meta={'current': cmd11, 'status':'PROGRESS at cmd11','progress_bar':0.55})
    pcutoff, gcutoff,peak_file,region_file= output11.rstrip().split(' ')
    pcutoff=float(pcutoff)
    gcutoff=float(gcutoff)
    print pcutoff, gcutoff
    drawed_pdf=os.path.join(os.path.join(output_folder,token),prefix+'.pdf')
    if noraw=='no':
        cmd12='python2.7 %s -g %s -c %s --ylim 0 30  -o %s --noraw --threshold %d -n %s'%(bsadraw_path,G_file,sc_chr_length_path,drawed_pdf,gcutoff,chrnum)
    else:
        cmd12='python2.7 %s -g %s -c %s --ylim 0 30  -o %s --threshold %d -n %s'%(bsadraw_path,G_file,sc_chr_length_path,drawed_pdf,gcutoff,chrnum)
    print cmd12
    os.system(cmd12)
    self.update_state(state='PROGRESS',meta={'current': cmd12, 'status':'PROGRESS at cmd12','progress_bar':0.58})
    Hmap_freq_file=os.path.join(os.path.join(output_folder,token),prefix+'_Hmap_freq.txt')
    Lmap_freq_file=os.path.join(os.path.join(output_folder,token),prefix+'_Lmap_freq.txt')
    freq_file= os.path.join(os.path.join(output_folder,token),prefix+'_freq.txt')
    cmd15="paste %s | awk '{ s=$3;p=$4; for(i=2; i<=NF/4; i+=1) {s+=$i*4-1;p+=$i*4}; print $1,$2, s,p }'|awk '{if ($3+$4==0)print $1,$2,$3/($3+$4+1);else print $1,$2,$3/($3+$4);}'>%s"%(' '.join(Hmaplist),Hmap_freq_file)
    print cmd15
    os.system(cmd15)
    self.update_state(state='PROGRESS',meta={'current': cmd15, 'status':'PROGRESS at cmd15','progress_bar':0.60})
    cmd16="paste %s | awk '{ s=$3;p=$4; for(i=2; i<=NF/4; i+=1) {s+=$i*4-1;p+=$i*4}; print $1,$2, s,p }'|awk '{if ($3+$4==0)print $1,$2,$3/($3+$4+1);else print $1,$2,$3/($3+$4);}'>%s"%(' '.join(Lmaplist),Lmap_freq_file)
    print cmd16
    os.system(cmd16)
    self.update_state(state='PROGRESS',meta={'current': cmd16, 'status':'PROGRESS at cmd16','progress_bar':0.63})
    cmd17="paste %s  %s  |awk '{print $1,$2,$3-$6}'>%s"%(Hmap_freq_file,Lmap_freq_file,freq_file)
    print cmd17
    os.system(cmd17)
    self.update_state(state='PROGRESS',meta={'current': cmd17, 'status':'PROGRESS at cmd17','progress_bar':0.65})
    os.remove(Hmap_freq_file)
    os.remove(Lmap_freq_file)
    freq_pdf_file= os.path.join(os.path.join(output_folder,token),prefix+'_freq.pdf')
    cmd18="Rscript --vanilla %s %s %s" %(freq_diff_path,freq_file,freq_pdf_file)
    print cmd18
    os.system(cmd18)
    self.update_state(state='PROGRESS',meta={'current': cmd18, 'status':'PROGRESS at cmd18','progress_bar':0.68})
# Annotation here
    P1_mpileup_file= os.path.join(os.path.join(output_folder,token),prefix+'_P1.mpileup')
    cmd19="samtools_0.1.18 mpileup -E -P ILLUMINA  -f %s %s >%s" %(sc64_fa_path,P1bamfile,P1_mpileup_file)
    print cmd19
    os.system(cmd19)
    self.update_state(state='PROGRESS',meta={'current': cmd19, 'status':'PROGRESS at cmd19','progress_bar':0.70})
    P2_mpileup_file= os.path.join(os.path.join(output_folder,token),prefix+'_P2.mpileup')
    cmd20="samtools_0.1.18 mpileup -E -P ILLUMINA  -f %s %s >%s" %(sc64_fa_path,P2bamfile,P2_mpileup_file)
    print cmd20
    os.system(cmd20)
    self.update_state(state='PROGRESS',meta={'current': cmd20, 'status':'PROGRESS at cmd20','progress_bar':0.73})
    os.remove(P1bamfile)
    os.remove(P2bamfile)
    P1_vcf_file= os.path.join(os.path.join(output_folder,token),prefix+'_P1.vcf')
    cmd21="python2.7 %s -i %s -o %s" %(pileup_to_vcf_path,P1_mpileup_file,P1_vcf_file)
    print cmd21
    os.system(cmd21)
    self.update_state(state='PROGRESS',meta={'current': cmd21, 'status':'PROGRESS at cmd21','progress_bar':0.75})
    os.remove(P1_mpileup_file)
    P2_vcf_file= os.path.join(os.path.join(output_folder,token),prefix+'_P2.vcf')
    cmd22="python2.7 %s -i %s -o %s" %(pileup_to_vcf_path,P2_mpileup_file,P2_vcf_file)
    print cmd22
    os.system(cmd22)
    self.update_state(state='PROGRESS',meta={'current': cmd22, 'status':'PROGRESS at cmd22','progress_bar':0.78})
    os.remove(P2_mpileup_file)
    P1_avinput_file= os.path.join(os.path.join(output_folder,token),prefix+'_P1.avinput')
    cmd23="perl %s -format vcf4 %s >%s" %(convert2annovar_path,P1_vcf_file,P1_avinput_file)
    print cmd23
    os.system(cmd23)
    self.update_state(state='PROGRESS',meta={'current': cmd23, 'status':'PROGRESS at cmd23','progress_bar':0.80})
    os.remove(P1_vcf_file)
    P2_avinput_file= os.path.join(os.path.join(output_folder,token),prefix+'_P2.avinput')
    cmd24="perl %s -format vcf4 %s >%s" %(convert2annovar_path,P2_vcf_file,P2_avinput_file)
    print cmd24
    os.system(cmd24)
    self.update_state(state='PROGRESS',meta={'current': cmd24, 'status':'PROGRESS at cmd24','progress_bar':0.84})
    os.remove(P2_vcf_file)
    P1_variant_function_file= P1_avinput_file+'.variant_function'
    P1_exonic_variant_function_file= P1_avinput_file+'.exonic_variant_function'    
    cmd25="perl %s -buildver sacCer3 %s %s" %(annotate_variation_path,P1_avinput_file,sacCer3_folder_path)
    print cmd25
    os.system(cmd25)
    self.update_state(state='PROGRESS',meta={'current': cmd25, 'status':'PROGRESS at cmd25','progress_bar':0.86})
    os.remove(P1_avinput_file)
    P2_variant_function_file= P2_avinput_file+'.variant_function'
    P2_exonic_variant_function_file= P2_avinput_file+'.exonic_variant_function'    
    cmd26="perl %s -buildver sacCer3 %s %s" %(annotate_variation_path,P2_avinput_file,sacCer3_folder_path)
    print cmd26
    os.system(cmd26)
    self.update_state(state='PROGRESS',meta={'current': cmd26, 'status':'PROGRESS at cmd26','progress_bar':0.90})
    os.remove(P2_avinput_file)
    P1_P2_combined_variant_function_file= os.path.join(os.path.join(output_folder,token),prefix+'_P1_P2_combined_variant_function.txt')
    P1_P2_combined_exonic_variant_function_file= os.path.join(os.path.join(output_folder,token),prefix+'_P1_P2_combined_exonic_variant_function.txt')    
    snapinputfile=os.path.join(os.path.join(output_folder,token),prefix+'.SNAP')
    snapoutputfile=os.path.join(os.path.join(output_folder,token),prefix+'.SNAP.out')
    cmd27="python2.7 %s -i %s -j %s -se %s -si %s -so %s -o %s" %(bsa_ann_path,P1_exonic_variant_function_file,P2_exonic_variant_function_file,SNAP_path,snapinputfile,snapoutputfile,P1_P2_combined_exonic_variant_function_file)
    print cmd27
    os.system(cmd27)
    os.remove(snapinputfile)
    os.remove(snapoutputfile)
    self.update_state(state='PROGRESS',meta={'current': cmd27, 'status':'PROGRESS at cmd27','progress_bar':0.93})
    os.remove(P1_variant_function_file)
    os.remove(P2_variant_function_file)
    os.remove(P1_exonic_variant_function_file)
    os.remove(P2_exonic_variant_function_file)
    peak_anno_file=os.path.join(os.path.join(output_folder,token),prefix+'_peak_ann.txt')
    cmd28="python2.7 %s -i %s -j %s  -o %s" %(create_peak_ann_path,P1_P2_combined_exonic_variant_function_file,peak_file,peak_anno_file)
    print cmd28
    os.system(cmd28)
    self.update_state(state='PROGRESS',meta={'current': cmd28, 'status':'PROGRESS at cmd28','progress_bar':0.95})
    region_anno_file=os.path.join(os.path.join(output_folder,token),prefix+'_region_ann.txt')
    cmd29="python2.7 %s -i %s -j %s  -o %s" %(create_region_ann_path,P1_P2_combined_exonic_variant_function_file,region_file,region_anno_file)
    print cmd29
    os.system(cmd29)
    self.update_state(state='PROGRESS',meta={'current': cmd29, 'status':'PROGRESS at cmd29','progress_bar':0.98})
# remake annotation table for show  
    pd.set_option('display.max_colwidth', -1)  
    files1=Files(ftype1='R',\
            ftype2='Fastq',\
            ftype3='Txt',\
            ffolder=token,\
            fname=os.path.basename(G_file),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(G_file))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(G_file)),\
            view_link=os.path.join(os.path.dirname(url_server),'show_G_file_page',token,  os.path.basename(G_file),prefix+'_threshold.txt') )
    files2=Files(ftype1='R',\
            ftype2='Fastq',\
            ftype3='Txt',\
            ffolder=token,\
            fname=os.path.basename(freq_file),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(freq_file))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(freq_file)),\
            view_link=os.path.join(os.path.dirname(url_server),'show_freq_file_page', token, os.path.basename(freq_file)) )
    files3=Files(ftype1='R',\
            ftype2='Fastq',\
            ftype3='Txt',\
            ffolder=token,\
            fname=os.path.basename(peak_file),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(peak_file))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(peak_file)),\
            view_link=os.path.join(os.path.dirname(url_server),'show_peak_page' ,token, os.path.basename(peak_file)))
    files4=Files(ftype1='R',\
            ftype2='Fastq',\
            ftype3='Txt',\
            ffolder=token,\
            fname=os.path.basename(region_file),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(region_file))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(region_file)),\
            view_link=os.path.join(os.path.dirname(url_server),'show_region_page' ,token, os.path.basename(region_file)))
    files5=Files(ftype1='R',\
            ftype2='Fastq',\
            ftype3='Txt',\
            ffolder=token,\
            fname=prefix+'_threshold.txt',\
            fsize=get_file_size(os.path.join(output_folder,token,prefix+'_threshold.txt')),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, prefix+'_threshold.txt'),\
            view_link=os.path.join(os.path.dirname(url_server),'show_threshold_page' ,token, prefix+'_threshold.txt'))
    files6=Files(ftype1='R',\
        ftype2='Fastq',\
        ftype3='Pdf',\
        ffolder=token,\
        fname=os.path.basename(drawed_pdf),\
        fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(drawed_pdf))),\
        owners_name=User.query.filter_by(email=user_email).first().username,\
        owners=User.query.filter_by(email=user_email).first().id,\
        link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(drawed_pdf)),\
        view_link=os.path.join(os.path.dirname(url_server), 'downloadresults',token, os.path.basename(drawed_pdf)) )
    files7=Files(ftype1='R',\
        ftype2='Fastq',\
        ftype3='Pdf',\
        ffolder=token,\
        fname=os.path.basename(freq_pdf_file),\
        fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(freq_pdf_file))),\
        owners_name=User.query.filter_by(email=user_email).first().username,\
        owners=User.query.filter_by(email=user_email).first().id,\
        link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(freq_pdf_file)),\
        view_link=os.path.join(os.path.dirname(url_server),'downloadresults',token,os.path.basename(freq_pdf_file)) )
    files8=Files(ftype1='R',\
        ftype2='Fastq',\
        ftype3='Txt',\
        ffolder=token,\
        fname=os.path.basename(peak_anno_file),\
        fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(peak_anno_file))),\
        owners_name=User.query.filter_by(email=user_email).first().username,\
        owners=User.query.filter_by(email=user_email).first().id,\
        link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(peak_anno_file)),\
        view_link=os.path.join(os.path.dirname(url_server), 'show_peak_anno_tables',token, os.path.basename(peak_anno_file)))            
    files9=Files(ftype1='R',\
        ftype2='Fastq',\
        ftype3='Txt',\
        ffolder=token,\
        fname=os.path.basename(region_anno_file),\
        fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(region_anno_file))),\
        owners_name=User.query.filter_by(email=user_email).first().username,\
        owners=User.query.filter_by(email=user_email).first().id,\
        link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(region_anno_file)),\
        view_link=os.path.join(os.path.dirname(url_server), 'show_region_anno_tables',token, os.path.basename(region_anno_file)))
    files10=Files(ftype1='R',\
        ftype2='Fastq',\
        ftype3='Txt',\
        ffolder=token,\
        fname=os.path.basename(P1_P2_combined_exonic_variant_function_file),\
        fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(P1_P2_combined_exonic_variant_function_file))),\
        owners_name=User.query.filter_by(email=user_email).first().username,\
        owners=User.query.filter_by(email=user_email).first().id,\
        link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(P1_P2_combined_exonic_variant_function_file)),\
        view_link=os.path.join(os.path.dirname(url_server), 'show_tables',token, os.path.basename(P1_P2_combined_exonic_variant_function_file)) ) 
    for i in Lmaplist:
        files11=Files(ftype1='R',\
        ftype2='Fastq',\
        ftype3='Txt',\
        ffolder=token,\
        fname=os.path.basename(i),\
        fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(i))),\
        owners_name=User.query.filter_by(email=user_email).first().username,\
        owners=User.query.filter_by(email=user_email).first().id,\
        link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(i)),\
        view_link=os.path.join(os.path.dirname(url_server), 'show_mapfile_tables_page',token, os.path.basename(i)))
        db.session.add(files11)  
    for i in Hmaplist:
        files12=Files(ftype1='R',\
        ftype2='Fastq',\
        ftype3='Txt',\
        ffolder=token,\
        fname=os.path.basename(i),\
        fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(i))),\
        owners_name=User.query.filter_by(email=user_email).first().username,\
        owners=User.query.filter_by(email=user_email).first().id,\
        link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(i)),\
        view_link=os.path.join(os.path.dirname(url_server), 'show_mapfile_tables_page',token, os.path.basename(i)) )
        db.session.add(files12)  
    db.session.add(files1)
    db.session.add(files2)
    db.session.add(files3)
    db.session.add(files4)
    db.session.add(files5)
    db.session.add(files6)
    db.session.add(files7)
    db.session.add(files8)
    db.session.add(files9)
    db.session.add(files10)
    task_db=Tasks.query.filter_by(owners=User.query.filter_by(email=user_email).first().id).filter_by(token_id=token).first()
    if task_db:
        task_db.status='Done'
        task_db.end_time=datetime.utcnow()
        duration_time_in_secondes=(task_db.end_time-task_db.create_time).total_seconds()
        task_db.duration_time=str('%d:%d:%d'%(duration_time_in_secondes/3600,duration_time_in_secondes% 3600/60,duration_time_in_secondes% 3600%60))
        db.session.add(task_db)
    db.session.commit()
    send_email(user_email, 'Your job (%s) done!'%(str(token)),\
                   'auth/email/task_done', url_server=url_server,user=user_email, token=str(token))    
    return {'current': 'Task completed!','result':{'input_type':input_type,'pcutoff':pcutoff,'gcutoff':gcutoff,'token':token,'G_file': os.path.basename(G_file), 'pdf_file': os.path.basename(drawed_pdf), 'peak_anno_file': os.path.basename(peak_anno_file),'region_anno_file':os.path.basename(region_anno_file),'freq_pdf_file':os.path.basename(freq_pdf_file),'P1_P2_combined_exonic_variant_function_file':os.path.basename(P1_P2_combined_exonic_variant_function_file),'freq_file':os.path.basename(freq_file),'peak_file':os.path.basename(peak_file),'region_file':os.path.basename(region_file),'threshold_file':prefix+'_threshold.txt'},'status':'Task completed!','progress_bar':1}

@celery.task(base=LogErrorsTask,bind=True)
def long_task_fastq1(self,output_folder,url_server,token,input_type,numbermap,prefix,bulk_design,P1fastqfile,P2fastqfile,fastqlist,readlen,phred,qccheck,trim,lastkeep,rmadapt,ladapter,radapter,minlen,removen,ncutoff,filtq,minq,pminq,rmunpaired,qcstat,insertsize,windowsize,cov_depth,kernel,width,chrnum,noraw):
    user_email=os.path.basename(output_folder)    
    os.mkdir(os.path.join(output_folder,token)) 
    P1fastqfile=json.loads(P1fastqfile)
    P2fastqfile=json.loads(P2fastqfile)
    fastqlist=json.loads(fastqlist)
    for i in range(len(fastqlist)):
        fastqlist[i]=[str(j) for j in fastqlist[i]]
    P1samfile=os.path.join(output_folder,token,prefix+'_P1.sam')
    P1bamfile=os.path.join(output_folder,token,prefix+'_P1.bam')
    P1bamfile_sort=os.path.join(output_folder,token,prefix+'_P1.sort.bam')
    if len(P1fastqfile)==2:
        if P1fastqfile[0].endswith('.fastq.gz'):
            with gzip.open(P1fastqfile[0], 'rb') as f_in:
                with open(os.path.splitext(P1fastqfile[0])[0], 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)
            os.remove(P1fastqfile[0])
            P1fastqfile[0]=os.path.splitext(P1fastqfile[0])[0]
        if P1fastqfile[1].endswith('.fastq.gz'):
            with gzip.open(P1fastqfile[1], 'rb') as f_in:
                with open(os.path.splitext(P1fastqfile[1])[0], 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)
            os.remove(P1fastqfile[1])
            P1fastqfile[1]=os.path.splitext(P1fastqfile[1])[0]
        cmd_bwa= 'cd %s\n'%(os.path.join(output_folder,token))
        cmd_bwa+='bwa mem  -R \"@RG\\tID:USER\\tLB:USER\\tPL:illumina\\tSM:USER\\tPI:USER\" %s %s %s >%s\n'%(sc64_fa_path,P1fastqfile[0],P1fastqfile[1],P1samfile)
        cmd_bwa+='samtools_0.1.18 view -Sb %s > %s\n'%(P1samfile,P1bamfile)
        cmd_bwa+='samtools_0.1.18 sort %s %s \n'%(P1bamfile,prefix+'_P1.sort')
    else:
        if P1fastqfile[0].endswith('.fastq.gz'):
            with gzip.open(P1fastqfile[0], 'rb') as f_in:
                with open(os.path.splitext(P1fastqfile[0])[0], 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)
            os.remove(P1fastqfile[0])
            P1fastqfile[0]=os.path.splitext(P1fastqfile[0])[0] 
        cmd_bwa= 'cd %s\n'%(os.path.join(output_folder,token))
        cmd_bwa+='bwa mem -R \"@RG\\tID:USER\\tLB:USER\\tPL:illumina\\tSM:USER\\tPI:USER\" %s %s >%s\n'%(sc64_fa_path,P1fastqfile[0],P1samfile)
        cmd_bwa+='samtools_0.1.18 view -Sb %s > %s\n'%(P1samfile,P1bamfile)
        cmd_bwa+='samtools_0.1.18 sort %s %s \n'%(P1bamfile,prefix+'_P1.sort')
    print cmd_bwa
    os.system(cmd_bwa)
    os.remove(P1samfile)
    os.remove(P1bamfile)
    P1bamfile=P1bamfile_sort
    self.update_state(state='PROGRESS',meta={'current': cmd_bwa, 'status':'PROGRESS at cmd_bwa'})
    
    P2samfile=os.path.join(output_folder,token,prefix+'_P2.sam')
    P2bamfile=os.path.join(output_folder,token,prefix+'_P2.bam')
    P2bamfile_sort=os.path.join(output_folder,token,prefix+'_P2.sort.bam')
    if len(P2fastqfile)==2:
        if P2fastqfile[0].endswith('.fastq.gz'):
            with gzip.open(P2fastqfile[0], 'rb') as f_in:
                with open(os.path.splitext(P2fastqfile[0])[0], 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)
            os.remove(P2fastqfile[0])
            P2fastqfile[0]=os.path.splitext(P2fastqfile[0])[0]
        if P2fastqfile[1].endswith('.fastq.gz'):
            with gzip.open(P2fastqfile[1], 'rb') as f_in:
                with open(os.path.splitext(P2fastqfile[1])[0], 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)
            os.remove(P2fastqfile[1])
            P2fastqfile[1]=os.path.splitext(P2fastqfile[1])[0]
        cmd_bwa= 'cd %s\n'%(os.path.join(output_folder,token))
        cmd_bwa+='bwa mem -R \"@RG\\tID:USER\\tLB:USER\\tPL:illumina\\tSM:USER\\tPI:USER\" %s %s %s >%s\n'%(sc64_fa_path,P2fastqfile[0],P2fastqfile[1],P2samfile)
        cmd_bwa+='samtools_0.1.18 view -Sb %s > %s\n'%(P2samfile,P2bamfile)
        cmd_bwa+='samtools_0.1.18 sort %s %s \n'%(P2bamfile,prefix+'_P2.sort')
    else:
        if P2fastqfile[0].endswith('.fastq.gz'):
            with gzip.open(P2fastqfile[0], 'rb') as f_in:
                with open(os.path.splitext(P2fastqfile[0])[0], 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)
            os.remove(P2fastqfile[0])
            P2fastqfile[0]=os.path.splitext(P2fastqfile[0])[0] 
        cmd_bwa= 'cd %s\n'%(os.path.join(output_folder,token))
        cmd_bwa+='bwa mem -R \"@RG\\tID:USER\\tLB:USER\\tPL:illumina\\tSM:USER\\tPI:USER\" %s %s >%s\n'%(sc64_fa_path,P2fastqfile[0],P2samfile)
        cmd_bwa+='samtools_0.1.18 view -Sb %s > %s\n'%(P2samfile,P2bamfile)
        cmd_bwa+='samtools_0.1.18 sort %s %s \n'%(P2bamfile,prefix+'_P2.sort')
    print cmd_bwa
    os.system(cmd_bwa)
    os.remove(P2samfile)
    os.remove(P2bamfile)
    P2bamfile=P2bamfile_sort
    self.update_state(state='PROGRESS',meta={'current': cmd_bwa, 'status':'PROGRESS at cmd_bwa'})

    bamlist=[]
    for i in range(1,len(fastqlist)+1):
        tmpsamfile=os.path.join(output_folder,token,prefix+'_'+str(i)+'.sam')
        tmpbamfile=os.path.join(output_folder,token,prefix+'_'+str(i)+'.bam')
        tmpbamfile_sort=os.path.join(output_folder,token,prefix+'_'+str(i)+'.sort.bam')
        if len(fastqlist[i-1])==2:
            if fastqlist[i-1][0].endswith('.fastq.gz'):
                with gzip.open(fastqlist[i-1][0], 'rb') as f_in:
                    with open(os.path.splitext(fastqlist[i-1][0])[0], 'wb') as f_out:
                        shutil.copyfileobj(f_in, f_out)
                os.remove(fastqlist[i-1][0])
                fastqlist[i-1][0]=os.path.splitext(fastqlist[i-1][0])[0]
            if fastqlist[i-1][1].endswith('.fastq.gz'):
                with gzip.open(fastqlist[i-1][1], 'rb') as f_in:
                    with open(os.path.splitext(fastqlist[i-1][1])[0], 'wb') as f_out:
                        shutil.copyfileobj(f_in, f_out)
                os.remove(fastqlist[i-1][1])
                fastqlist[i-1][1]=os.path.splitext(fastqlist[i-1][1])[0]
            cmd_bwa= 'cd %s\n'%(os.path.join(output_folder,token))
            cmd_bwa+='bwa mem -R \"@RG\\tID:USER\\tLB:USER\\tPL:illumina\\tSM:USER\\tPI:USER\" %s %s %s >%s\n'%(sc64_fa_path,fastqlist[i-1][0],fastqlist[i-1][1],tmpsamfile)
            cmd_bwa+='samtools_0.1.18 view -Sb %s > %s\n'%(tmpsamfile,tmpbamfile)
            cmd_bwa+='samtools_0.1.18 sort %s %s \n'%(tmpbamfile,prefix+'_'+str(i)+'.sort')
        else:
            if fastqlist[i-1][0].endswith('.fastq.gz'):
                with gzip.open(fastqlist[i-1][0], 'rb') as f_in:
                    with open(os.path.splitext(fastqlist[i-1][0])[0], 'wb') as f_out:
                        shutil.copyfileobj(f_in, f_out)
                os.remove(fastqlist[i-1][0])
                fastqlist[i-1][0]=os.path.splitext(fastqlist[i-1][0])[0] 
            cmd_bwa= 'cd %s\n'%(os.path.join(output_folder,token))
            cmd_bwa+='bwa mem -R \"@RG\\tID:USER\\tLB:USER\\tPL:illumina\\tSM:USER\\tPI:USER\" %s %s >%s\n'%(sc64_fa_path,fastqlist[i-1][0],tmpsamfile)
            cmd_bwa+='samtools_0.1.18 view -Sb %s > %s\n'%(tmpsamfile,tmpbamfile)
            cmd_bwa+='samtools_0.1.18 sort %s %s \n'%(tmpbamfile,prefix+'_'+str(i)+'.sort')
        print cmd_bwa
        os.system(cmd_bwa)
        bamlist.append(tmpbamfile_sort)
        os.remove(tmpsamfile)
        os.remove(tmpbamfile)
        self.update_state(state='PROGRESS',meta={'current': cmd_bwa, 'status':'PROGRESS at cmd_bwa'})
#    bamlist=' '.join(bamlist)

#identify genetic marker and make Map file
    mpileup_file=os.path.join(output_folder,token,prefix+'.mpileup')
    cmd1='samtools_0.1.18 mpileup -P ILLUMINA  -f  %s %s %s %s >%s'%(sc64_fa_path,P1bamfile,P2bamfile,' '.join(bamlist),mpileup_file)
    print cmd1
    os.system(cmd1)
    self.update_state(state='PROGRESS',meta={'current': cmd1, 'status':'PROGRESS at cmd1','progress_bar':0.05})
    for i in bamlist:
        os.remove(i)
    count_file=mpileup_file+'.count'
    cmd2='python2.7 %s %s %s >%s'%(count_allele_for4grp_bsa_path,mpileup_file,2+numbermap,count_file)
    print cmd2
    os.system(cmd2)
    self.update_state(state='PROGRESS',meta={'current': cmd2, 'status':'PROGRESS at cmd2','progress_bar':0.10})
    os.remove(mpileup_file)
    hom_file=count_file+'.'+str(cov_depth)+'Xhom'
    cmd3='python2.7 %s %s %s >%s'%(find_hom_nX_path,count_file,str(cov_depth),hom_file)
    print cmd3
    os.system(cmd3)
    self.update_state(state='PROGRESS',meta={'current': cmd3, 'status':'PROGRESS at cmd3','progress_bar':0.15})
    os.remove(count_file)
    marker_file=hom_file+'.marker'
    cmd4='python2.7 %s %s >%s'%(find_marker_path,hom_file,marker_file)
    print cmd4
    os.system(cmd4)
    self.update_state(state='PROGRESS',meta={'current': cmd4, 'status':'PROGRESS at cmd4','progress_bar':0.20})
    os.remove(hom_file)
    chr_sorted_file=marker_file+'.sorted'
    cmd5='python2.7 %s %s >%s'%(sort_chr_path,marker_file,chr_sorted_file)
    print cmd5
    os.system(cmd5)
    self.update_state(state='PROGRESS',meta={'current': cmd5, 'status':'PROGRESS at cmd5','progress_bar':0.25})
    os.remove(marker_file)
    four_cell_file=chr_sorted_file+'.4cell'
    cmd6='python2.7 %s %s %d %d >%s'%(find_4cell_path,chr_sorted_file,int(numbermap)+2,int(bulk_design),four_cell_file)
    print cmd6
    os.system(cmd6)
    self.update_state(state='PROGRESS',meta={'current': cmd6, 'status':'PROGRESS at cmd6','progress_bar':0.30})
    os.remove(chr_sorted_file)
    MPass_file=four_cell_file+'.MPass'
    cmd7='grep "MPass" %s >%s'%(four_cell_file,MPass_file)
    print cmd7
    os.system(cmd7)
    self.update_state(state='PROGRESS',meta={'current': cmd7, 'status':'PROGRESS at cmd7','progress_bar':0.33})
    os.remove(four_cell_file)
    maplist=[]
    for i in range(1,numbermap+1):
        out_Map_file=MPass_file+'.B'+str(i)+'.map'
        cmd8='cut -f1,2,%d,%d %s> %s'%((int(numbermap)+2)*8+7+2*i-1,(int(numbermap)+2)*8+7+2*i,MPass_file,out_Map_file)
        print cmd8
        os.system(cmd8)
        self.update_state(state='PROGRESS',meta={'current': cmd8, 'status':'PROGRESS at cmd8','progress_bar':0.35})
        maplist.append(out_Map_file)
    os.remove(MPass_file)
#Calculate G
    G_file=os.path.join(output_folder,token,prefix+'.G')
    cmd10='python2.7 %s -L %s  -H %s -w %d -k %s --onepool -o %s'%(bsacalc_path,' '.join(maplist),' '.join(maplist),int(width),kernel,G_file)
    print cmd10
    os.system(cmd10)
    self.update_state(state='PROGRESS',meta={'current': cmd10, 'status':'PROGRESS at cmd10','progress_bar':0.40})
    cmd11='python2.7 %s %s %s %s'%(bsapeak_path,G_file,os.path.join(output_folder,token),prefix)
    print cmd11
    output11=TIMEOUT_COMMAND(cmd11, 300)
    if not output11:
        self.update_state(state='FAILED',meta={'current': cmd11, 'status': 'FAILED at cmd11'})
    else:
        output11=' '.join(output11)
    self.update_state(state='PROGRESS',meta={'current': cmd11, 'status':'PROGRESS at cmd11','progress_bar':0.50})
    pcutoff, gcutoff,peak_file,region_file= output11.rstrip().split(' ')
    pcutoff=float(pcutoff)
    gcutoff=float(gcutoff)
    print pcutoff, gcutoff
    drawed_pdf=os.path.join(output_folder,token,prefix+'.pdf')
    if noraw=='no':
        cmd12='python2.7 %s -g %s -c %s --ylim 0 30  -o %s --noraw --threshold %d -n %s'%(bsadraw_path,G_file,sc_chr_length_path,drawed_pdf,gcutoff,chrnum)
    else:
        cmd12='python2.7 %s -g %s -c %s --ylim 0 30  -o %s --threshold %d -n %s'%(bsadraw_path,G_file,sc_chr_length_path,drawed_pdf,gcutoff,chrnum)
    print cmd12
    os.system(cmd12)
    self.update_state(state='PROGRESS',meta={'current': cmd12, 'status':'PROGRESS at cmd12','progress_bar':0.60})
    freq_file= os.path.join(output_folder,token,prefix+'_freq.txt')
    cmd15="paste %s | awk '{ s=$3;p=$4; for(i=2; i<=NF/4; i+=1) {s+=$i*4-1;p+=$i*4}; print $1,$2, s,p }'|awk '{if ($3+$4==0)print $1,$2,$3/($3+$4+1);else print $1,$2,$3/($3+$4);}'>%s"%(' '.join(maplist),freq_file)
    print cmd15
    os.system(cmd15)
    self.update_state(state='PROGRESS',meta={'current': cmd15, 'status':'PROGRESS at cmd15','progress_bar':0.70})
    freq_pdf_file= os.path.join(output_folder,token,prefix+'_freq.pdf')
    cmd18="Rscript --vanilla %s %s %s" %(freq_diff_path,freq_file,freq_pdf_file)
    print cmd18
    os.system(cmd18)
    self.update_state(state='PROGRESS',meta={'current': cmd18, 'status':'PROGRESS at cmd18','progress_bar':0.75})
# Annotation here
    P1_mpileup_file= os.path.join(output_folder,token,prefix+'_P1.mpileup')
    cmd19="samtools_0.1.18 mpileup -E -P ILLUMINA  -f %s %s >%s" %(sc64_fa_path,P1bamfile,P1_mpileup_file)
    print cmd19
    os.system(cmd19)
    self.update_state(state='PROGRESS',meta={'current': cmd19, 'status':'PROGRESS at cmd19','progress_bar':0.78})
    P2_mpileup_file= os.path.join(output_folder,token,prefix+'_P2.mpileup')
    cmd20="samtools_0.1.18 mpileup -E -P ILLUMINA  -f %s %s >%s" %(sc64_fa_path,P2bamfile,P2_mpileup_file)
    print cmd20
    os.system(cmd20)
    self.update_state(state='PROGRESS',meta={'current': cmd20, 'status':'PROGRESS at cmd20','progress_bar':0.80})
    P1_vcf_file= os.path.join(output_folder,token,prefix+'_P1.vcf')
    cmd21="python2.7 %s -i %s -o %s" %(pileup_to_vcf_path,P1_mpileup_file,P1_vcf_file)
    print cmd21
    os.system(cmd21)
    self.update_state(state='PROGRESS',meta={'current': cmd21, 'status':'PROGRESS at cmd21','progress_bar':0.83})
    os.remove(P1_mpileup_file)
    P2_vcf_file= os.path.join(output_folder,token,prefix+'_P2.vcf')
    cmd22="python2.7 %s -i %s -o %s" %(pileup_to_vcf_path,P2_mpileup_file,P2_vcf_file)
    print cmd22
    os.system(cmd22)
    self.update_state(state='PROGRESS',meta={'current': cmd22, 'status':'PROGRESS at cmd22','progress_bar':0.85})
    os.remove(P2_mpileup_file)
    P1_avinput_file= os.path.join(output_folder,token,prefix+'_P1.avinput')
    cmd23="perl %s -format vcf4 %s >%s" %(convert2annovar_path,P1_vcf_file,P1_avinput_file)
    print cmd23
    os.system(cmd23)
    self.update_state(state='PROGRESS',meta={'current': cmd23, 'status':'PROGRESS at cmd23','progress_bar':0.88})
    os.remove(P1_vcf_file)
    P2_avinput_file= os.path.join(output_folder,token,prefix+'_P2.avinput')
    cmd24="perl %s -format vcf4 %s >%s" %(convert2annovar_path,P2_vcf_file,P2_avinput_file)
    print cmd24
    os.system(cmd24)
    self.update_state(state='PROGRESS',meta={'current': cmd24, 'status':'PROGRESS at cmd24','progress_bar':0.90})
    os.remove(P2_vcf_file)
    P1_variant_function_file= P1_avinput_file+'.variant_function'
    P1_exonic_variant_function_file= P1_avinput_file+'.exonic_variant_function'    
    cmd25="perl %s -buildver sacCer3 %s %s" %(annotate_variation_path,P1_avinput_file,sacCer3_folder_path)
    print cmd25
    os.system(cmd25)
    self.update_state(state='PROGRESS',meta={'current': cmd25, 'status':'PROGRESS at cmd25','progress_bar':0.92})
    os.remove(P1_avinput_file)
    P2_variant_function_file= P2_avinput_file+'.variant_function'
    P2_exonic_variant_function_file= P2_avinput_file+'.exonic_variant_function'    
    cmd26="perl %s -buildver sacCer3 %s %s" %(annotate_variation_path,P2_avinput_file,sacCer3_folder_path)
    print cmd26
    os.system(cmd26)
    self.update_state(state='PROGRESS',meta={'current': cmd26, 'status':'PROGRESS at cmd26','progress_bar':0.94})
    os.remove(P2_avinput_file)
    P1_P2_combined_variant_function_file= os.path.join(output_folder,token,prefix+'_P1_P2_combined_variant_function.txt')
    P1_P2_combined_exonic_variant_function_file= os.path.join(output_folder,token,prefix+'_P1_P2_combined_exonic_variant_function.txt')    
#    cmd27="python2.7 %s -i %s -j %s  -o %s" %(bsa_ann_path,P1_exonic_variant_function_file,P2_exonic_variant_function_file,P1_P2_combined_exonic_variant_function_file)
    snapinputfile=os.path.join(os.path.join(output_folder,token),prefix+'.SNAP')
    snapoutputfile=os.path.join(os.path.join(output_folder,token),prefix+'.SNAP.out')
    cmd27="python2.7 %s -i %s -j %s -se %s -si %s -so %s -o %s" %(bsa_ann_path,P1_exonic_variant_function_file,P2_exonic_variant_function_file,SNAP_path,snapinputfile,snapoutputfile,P1_P2_combined_exonic_variant_function_file)
    print cmd27
    os.system(cmd27)
    os.remove(snapinputfile)
    os.remove(snapoutputfile)
    self.update_state(state='PROGRESS',meta={'current': cmd27, 'status':'PROGRESS at cmd27','progress_bar':0.96})
    os.remove(P1_variant_function_file)
    os.remove(P2_variant_function_file)
    os.remove(P1_exonic_variant_function_file)
    os.remove(P2_exonic_variant_function_file)
    peak_anno_file=os.path.join(os.path.join(output_folder,token),prefix+'_peak_ann.txt')
    cmd28="python2.7 %s -i %s -j %s  -o %s" %(create_peak_ann_path,P1_P2_combined_exonic_variant_function_file,peak_file,peak_anno_file)
    print cmd28
    os.system(cmd28)
    self.update_state(state='PROGRESS',meta={'current': cmd28, 'status':'PROGRESS at cmd28'})
    region_anno_file=os.path.join(os.path.join(output_folder,token),prefix+'_region_ann.txt')
    cmd29="python2.7 %s -i %s -j %s  -o %s" %(create_region_ann_path,P1_P2_combined_exonic_variant_function_file,region_file,region_anno_file)
    print cmd29
    os.system(cmd29)
    self.update_state(state='PROGRESS',meta={'current': cmd29, 'status':'PROGRESS at cmd29','progress_bar':0.98})
    os.remove(P1bamfile)
    os.remove(P2bamfile)
# remake annotation table for show 
    pd.set_option('display.max_colwidth', -1)  
    files1=Files(ftype1='R',\
            ftype2='Fastq',\
            ftype3='Txt',\
            ffolder=token,\
            fname=os.path.basename(G_file),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(G_file))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(G_file)),\
            view_link=os.path.join(os.path.dirname(url_server),'show_G_file_page',token,  os.path.basename(G_file),prefix+'_threshold.txt')  )
    files2=Files(ftype1='R',\
            ftype2='Fastq',\
            ftype3='Txt',\
            ffolder=token,\
            fname=os.path.basename(freq_file),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(freq_file))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(freq_file)),\
            view_link=os.path.join(os.path.dirname(url_server),'show_freq_file_page', token, os.path.basename(freq_file)) )
    files3=Files(ftype1='R',\
            ftype2='Fastq',\
            ftype3='Txt',\
            ffolder=token,\
            fname=os.path.basename(peak_file),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(peak_file))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(peak_file)),\
            view_link=os.path.join(os.path.dirname(url_server),'show_peak_page' ,token, os.path.basename(peak_file)))
    files4=Files(ftype1='R',\
            ftype2='Fastq',\
            ftype3='Txt',\
            ffolder=token,\
            fname=os.path.basename(region_file),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(region_file))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server), 'downloadresults',token,os.path.basename(region_file)),\
            view_link=os.path.join(os.path.dirname(url_server), 'show_region_page' ,token, os.path.basename(region_file)))
    files5=Files(ftype1='R',\
            ftype2='Fastq',\
            ftype3='Txt',\
            ffolder=token,\
            fname=prefix+'_threshold.txt',\
            fsize=get_file_size(os.path.join(output_folder,token,prefix+'_threshold.txt')),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, prefix+'_threshold.txt'),\
            view_link=os.path.join(os.path.dirname(url_server),'show_threshold_page' ,token, prefix+'_threshold.txt'))
    files6=Files(ftype1='R',\
        ftype2='Fastq',\
        ftype3='Pdf',\
        ffolder=token,\
        fname=os.path.basename(drawed_pdf),\
        fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(drawed_pdf))),\
        owners_name=User.query.filter_by(email=user_email).first().username,\
        owners=User.query.filter_by(email=user_email).first().id,\
        link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(drawed_pdf)),\
        view_link=os.path.join(os.path.dirname(url_server),'downloadresults',token,  os.path.basename(drawed_pdf)) )
    files7=Files(ftype1='R',\
        ftype2='Fastq',\
        ftype3='Pdf',\
        ffolder=token,\
        fname=os.path.basename(freq_pdf_file),\
        fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(freq_pdf_file))),\
        owners_name=User.query.filter_by(email=user_email).first().username,\
        owners=User.query.filter_by(email=user_email).first().id,\
        link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(freq_pdf_file)),\
        view_link=os.path.join(os.path.dirname(url_server),'downloadresults',token,os.path.basename(freq_pdf_file)) )
    files8=Files(ftype1='R',\
        ftype2='Fastq',\
        ftype3='Txt',\
        ffolder=token,\
        fname=os.path.basename(peak_anno_file),\
        fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(peak_anno_file))),\
        owners_name=User.query.filter_by(email=user_email).first().username,\
        owners=User.query.filter_by(email=user_email).first().id,\
        link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(peak_anno_file)),\
        view_link=os.path.join(os.path.dirname(url_server), 'show_peak_anno_tables',token, os.path.basename(peak_anno_file)))            
    files9=Files(ftype1='R',\
        ftype2='Fastq',\
        ftype3='Txt',\
        ffolder=token,\
        fname=os.path.basename(region_anno_file),\
        fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(region_anno_file))),\
        owners_name=User.query.filter_by(email=user_email).first().username,\
        owners=User.query.filter_by(email=user_email).first().id,\
        link=os.path.join(os.path.dirname(url_server), 'downloadresults',token,os.path.basename(region_anno_file)),\
        view_link=os.path.join(os.path.dirname(url_server),'show_region_anno_tables',token,  os.path.basename(region_anno_file)))
    files10=Files(ftype1='R',\
        ftype2='Fastq',\
        ftype3='Txt',\
        ffolder=token,\
        fname=os.path.basename(P1_P2_combined_exonic_variant_function_file),
        fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(P1_P2_combined_exonic_variant_function_file))),\
        owners_name=User.query.filter_by(email=user_email).first().username,\
        owners=User.query.filter_by(email=user_email).first().id,\
        link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(P1_P2_combined_exonic_variant_function_file)),\
        view_link=os.path.join(os.path.dirname(url_server), 'show_tables',token, os.path.basename(P1_P2_combined_exonic_variant_function_file)) ) 
    for i in maplist:
        files11=Files(ftype1='R',\
        ftype2='Fastq',\
        ftype3='Txt',\
        ffolder=token,\
        fname=os.path.basename(i),\
        fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(i))),\
        owners_name=User.query.filter_by(email=user_email).first().username,\
        owners=User.query.filter_by(email=user_email).first().id,\
        link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(i)),\
        view_link=os.path.join(os.path.dirname(url_server), 'show_mapfile_tables_page',token, os.path.basename(i)))
        db.session.add(files11)
    db.session.add(files1)
    db.session.add(files2)
    db.session.add(files3)
    db.session.add(files4)
    db.session.add(files5)
    db.session.add(files6)
    db.session.add(files7)
    db.session.add(files8)
    db.session.add(files9)
    db.session.add(files10)
    task_db=Tasks.query.filter_by(owners=User.query.filter_by(email=user_email).first().id).filter_by(token_id=token).first()
    if task_db:
        task_db.status='Done'
        task_db.end_time=datetime.utcnow()
        duration_time_in_secondes=(task_db.end_time-task_db.create_time).total_seconds()
        task_db.duration_time=str('%d:%d:%d'%(duration_time_in_secondes/3600,duration_time_in_secondes% 3600/60,duration_time_in_secondes% 3600%60))
        db.session.add(task_db)
    db.session.commit()
    send_email(user_email, 'Your job (%s) done!'%(str(token)),\
                   'auth/email/task_done', url_server=url_server,user=user_email, token=str(token))
    return {'current': 'Task completed!','result':{'input_type':input_type,'pcutoff':pcutoff,'gcutoff':gcutoff,'token':token,'G_file': os.path.basename(G_file), 'pdf_file': os.path.basename(drawed_pdf), 'peak_anno_file': os.path.basename(peak_anno_file),'region_anno_file':os.path.basename(region_anno_file),'freq_pdf_file':os.path.basename(freq_pdf_file),'P1_P2_combined_exonic_variant_function_file':os.path.basename(P1_P2_combined_exonic_variant_function_file),'freq_file':os.path.basename(freq_file),'peak_file':os.path.basename(peak_file),'region_file':os.path.basename(region_file),'threshold_file':prefix+'_threshold.txt'},'status':'Task completed!','progress_bar':1}
    
@celery.task(base=LogErrorsTask,bind=True)
def long_task_map2(self,output_folder,url_server,token,input_type,numbermap,prefix,bulk_design,Hmaplist,Lmaplist,kernel,width,chrnum,noraw,length_input_file):
    user_email=os.path.basename(output_folder)
    os.mkdir(os.path.join(output_folder,token)) 
    G_file=os.path.join(output_folder,token,prefix+'.G')
    cmd10='python2.7 %s -L %s -H %s -w %d -k %s -o %s'%(bsacalc_path,Lmaplist,Hmaplist,int(width),kernel,G_file)
    print cmd10
    output10=TIMEOUT_COMMAND(cmd10, 600)
    if not output10:
        self.update_state(state='FAILED',meta={'current': cmd10, 'status': 'FAILED at cmd10'})
    else:
        output10=' '.join(output10)
    self.update_state(state='PROGRESS',meta={'current': cmd10, 'status':'PROGRESS at cmd10','progress_bar':0.15})
    cmd11='python2.7 %s %s %s %s'%(bsapeak_path,G_file,os.path.join(output_folder,token),prefix)
    print cmd11
    output11=TIMEOUT_COMMAND(cmd11, 600)
    if not output11:
        self.update_state(state='FAILED',meta={'current': cmd11, 'status': 'FAILED at cmd11'})
    else:
        output11=' '.join(output11)
    self.update_state(state='PROGRESS',meta={'current': cmd11, 'status':'PROGRESS at cmd11','progress_bar':0.30})
    pcutoff, gcutoff,peak_file,region_file= output11.rstrip().split(' ')
    if pcutoff=='None':
        pcutoff=0
    else:
        pcutoff=float(pcutoff)
    if gcutoff=='None':
        gcutoff=0
    else:
        gcutoff=float(gcutoff)
    print pcutoff, gcutoff
    drawed_pdf=os.path.join(os.path.join(output_folder,token),prefix+'.pdf')
    if noraw=='no':
        cmd12='python2.7 %s -g %s -c %s --ylim 0 30  -o %s --noraw --threshold %d -n %s'%(bsadraw_path,G_file,length_input_file,drawed_pdf,gcutoff,chrnum)
    else:
        cmd12='python2.7 %s -g %s -c %s --ylim 0 30  -o %s --threshold %d -n %s'%(bsadraw_path,G_file,length_input_file,drawed_pdf,gcutoff,chrnum)
    print cmd12
    os.system(cmd12)
    self.update_state(state='PROGRESS',meta={'current': cmd12, 'status':'PROGRESS at cmd12','progress_bar':0.45})
    Hmap_freq_file=os.path.join(os.path.join(output_folder,token),prefix+'_Hmap_freq.txt')
    Lmap_freq_file=os.path.join(os.path.join(output_folder,token),prefix+'_Lmap_freq.txt')
    freq_file= os.path.join(os.path.join(output_folder,token),prefix+'_freq.txt')
    cmd15="paste %s | awk '{ s=$3;p=$4; for(i=2; i<=NF/4; i+=1) {s+=$i*4-1;p+=$i*4}; print $1,$2, s,p }'|awk '{if ($3+$4==0)print $1,$2,$3/($3+$4+1);else print $1,$2,$3/($3+$4);}'>%s"%(Hmaplist,Hmap_freq_file)
    print cmd15
    os.system(cmd15)
    self.update_state(state='PROGRESS',meta={'current': cmd15, 'status':'PROGRESS at cmd15','progress_bar':0.60})
    cmd16="paste %s | awk '{ s=$3;p=$4; for(i=2; i<=NF/4; i+=1) {s+=$i*4-1;p+=$i*4}; print $1,$2, s,p }'|awk '{if ($3+$4==0)print $1,$2,$3/($3+$4+1);else print $1,$2,$3/($3+$4);}'>%s"%(Lmaplist,Lmap_freq_file)
    print cmd16
    os.system(cmd16)
    self.update_state(state='PROGRESS',meta={'current': cmd16, 'status':'PROGRESS at cmd16','progress_bar':0.75})
    cmd17="paste %s  %s  |awk '{print $1,$2,$3-$6}'>%s"%(Hmap_freq_file,Lmap_freq_file,freq_file)
    print cmd17
    os.system(cmd17)
    self.update_state(state='PROGRESS',meta={'current': cmd17, 'status':'PROGRESS at cmd17','progress_bar':0.85})
    os.remove(Hmap_freq_file)
    os.remove(Lmap_freq_file)
    freq_pdf_file= os.path.join(os.path.join(output_folder,token),prefix+'_freq.pdf')
    cmd18="Rscript --vanilla %s %s %s" %(freq_diff_path,freq_file,freq_pdf_file)
    print cmd18
    os.system(cmd18)
    self.update_state(state='PROGRESS',meta={'current': cmd18, 'status':'PROGRESS at cmd18','progress_bar':0.95})
    files1=Files(ftype1='R',\
            ftype2='Map',\
            ftype3='Txt',\
            ffolder=token,\
            fname=os.path.basename(G_file),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(G_file))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(G_file)),\
            view_link=os.path.join(os.path.dirname(url_server),'show_G_file_page',token,  os.path.basename(G_file),prefix+'_threshold.txt')  \
              )
    files2=Files(ftype1='R',\
            ftype2='Map',\
            ftype3='Txt',\
            ffolder=token,\
            fname=os.path.basename(freq_file),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(freq_file))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(freq_file)),\
            view_link=os.path.join(os.path.dirname(url_server),'show_freq_file_page', token, os.path.basename(freq_file)) \
              )
    files3=Files(ftype1='R',\
            ftype2='Map',\
            ftype3='Txt',\
            ffolder=token,\
            fname=os.path.basename(peak_file),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(peak_file))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(peak_file)),\
            view_link=os.path.join(os.path.dirname(url_server),'show_peak_page' ,token, os.path.basename(peak_file)) \
              )
    files4=Files(ftype1='R',\
            ftype2='Map',\
            ftype3='Txt',\
            ffolder=token,\
            fname=os.path.basename(region_file),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(region_file))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(region_file)),\
            view_link=os.path.join(os.path.dirname(url_server), 'show_region_page' ,token, os.path.basename(region_file))\
               )
    files5=Files(ftype1='R',\
            ftype2='Map',\
            ftype3='Txt',\
            ffolder=token,\
            fname=prefix+'_threshold.txt',\
            fsize=get_file_size(os.path.join(output_folder,token,prefix+'_threshold.txt')),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, prefix+'_threshold.txt'),\
            view_link=os.path.join(os.path.dirname(url_server),'show_threshold_page' ,token, prefix+'_threshold.txt') )
    files6=Files(ftype1='R',\
            ftype2='Map',\
            ftype3='Pdf',\
            ffolder=token,\
            fname=os.path.basename(drawed_pdf),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(drawed_pdf))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(drawed_pdf)),\
            view_link=os.path.join(os.path.dirname(url_server), 'downloadresults',token, os.path.basename(drawed_pdf)) )               
    files7=Files(ftype1='R',\
            ftype2='Map',\
            ftype3='Pdf',\
            ffolder=token,\
            fname=os.path.basename(freq_pdf_file),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(freq_pdf_file))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server), 'downloadresults',token,os.path.basename(freq_pdf_file)),\
            view_link=os.path.join(os.path.dirname(url_server),'downloadresults',token,os.path.basename(freq_pdf_file)) )
    db.session.add(files1)
    db.session.add(files2)
    db.session.add(files3)
    db.session.add(files4)
    db.session.add(files5)
    db.session.add(files6)
    db.session.add(files7)
    task_db=Tasks.query.filter_by(owners=User.query.filter_by(email=user_email).first().id).filter_by(token_id=token).first()
    if task_db:
        task_db.status='Done'
        task_db.end_time=datetime.utcnow()
        duration_time_in_secondes=(task_db.end_time-task_db.create_time).total_seconds()
        task_db.duration_time=str('%d:%d:%d'%(duration_time_in_secondes/3600,duration_time_in_secondes% 3600/60,duration_time_in_secondes% 3600%60))
        db.session.add(task_db)
    db.session.commit()
    send_email(user_email, 'Your job (%s) done!'%(str(token)),\
                   'auth/email/task_done', url_server=url_server,user=user_email, token=str(token))
    return {'current': 'Task completed!','result':{'input_type':input_type,'G_file': os.path.basename(G_file), 'pdf_file': os.path.basename(drawed_pdf), 'pcutoff':pcutoff,'gcutoff':gcutoff,'token':token,'freq_pdf_file':os.path.basename(freq_pdf_file),'peak_file':os.path.basename(peak_file),'region_file':os.path.basename(region_file),'freq_file':os.path.basename(freq_file),'threshold_file':prefix+'_threshold.txt'},'status':'Task completed!','progress_bar':1}
    
@celery.task(base=LogErrorsTask,bind=True)
def long_task_map1(self,output_folder,url_server,token,input_type,numbermap,prefix,bulk_design,maplist,kernel,width,chrnum,noraw,length_input_file):
    user_email=os.path.basename(output_folder)
    os.mkdir(os.path.join(output_folder,token)) 
    G_file=os.path.join(output_folder,token,prefix+'.G')
    cmd10='python2.7 %s -L %s  -H %s -w %d -k %s --onepool -o %s'%(bsacalc_path,maplist,maplist,int(width),kernel,G_file)
    print cmd10
    os.system(cmd10)
    self.update_state(state='PROGRESS',meta={'current': cmd10, 'status':'PROGRESS at cmd10','progress_bar':0.15})
    cmd11='python2.7 %s %s %s %s'%(bsapeak_path,G_file,os.path.join(output_folder,token),prefix)
    print cmd11
    output11=TIMEOUT_COMMAND(cmd11, 30)
    if not output11:
        self.update_state(state='FAILED',meta={'current': cmd11, 'status': 'FAILED at cmd11'})
    else:
        output11=' '.join(output11)
    self.update_state(state='PROGRESS',meta={'current': cmd11, 'status':'PROGRESS at cmd11','progress_bar':0.30})
    pcutoff, gcutoff,peak_file,region_file= output11.rstrip().split(' ')
    if pcutoff=='None':
        pcutoff=0
    else:
        pcutoff=float(pcutoff)
    if gcutoff=='None':
        gcutoff=0
    else:
        gcutoff=float(gcutoff)
    print pcutoff, gcutoff
    drawed_pdf=os.path.join(os.path.join(output_folder,token),prefix+'.pdf')
    if noraw=='no':
        cmd12='python2.7 %s -g %s -c %s --ylim 0 30  -o %s --noraw --threshold %d -n %s'%(bsadraw_path,G_file,length_input_file,drawed_pdf,gcutoff,chrnum)
    else:
        cmd12='python2.7 %s -g %s -c %s --ylim 0 30  -o %s --threshold %d -n %s'%(bsadraw_path,G_file,length_input_file,drawed_pdf,gcutoff,chrnum)
    print cmd12
    os.system(cmd12)
    self.update_state(state='PROGRESS',meta={'current': cmd12, 'status':'PROGRESS at cmd12','progress_bar':0.45})
    freq_file=os.path.join(os.path.join(output_folder,token),prefix+'_freq.txt')
    cmd15="paste %s | awk '{ s=$3;p=$4; for(i=2; i<=NF/4; i+=1) {s+=$i*4-1;p+=$i*4}; print $1,$2, s,p }'|awk '{if ($3+$4==0)print $1,$2,$3/($3+$4+1);else print $1,$2,$3/($3+$4);}'>%s"%(maplist,freq_file)
    print cmd15
    os.system(cmd15)
    self.update_state(state='PROGRESS',meta={'current': cmd15, 'status':'PROGRESS at cmd15','progress_bar':0.60})
    freq_pdf_file= os.path.join(os.path.join(output_folder,token),prefix+'_freq.pdf')
    cmd18="Rscript --vanilla %s %s %s" %(freq_diff_path,freq_file,freq_pdf_file)
    print cmd18
    os.system(cmd18)
    self.update_state(state='PROGRESS',meta={'current': cmd18, 'status':'PROGRESS at cmd18','progress_bar':0.75})
    files1=Files(ftype1='R',\
            ftype2='Map',\
            ftype3='Txt',\
            ffolder=token,\
            fname=os.path.basename(G_file),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(G_file))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(G_file)),\
            view_link=os.path.join(os.path.dirname(url_server),'show_G_file_page',token,  os.path.basename(G_file),prefix+'_threshold.txt') \
               )
    files2=Files(ftype1='R',\
            ftype2='Map',\
            ftype3='Txt',\
            ffolder=token,\
            fname=os.path.basename(freq_file),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(freq_file))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(freq_file)),\
            view_link=os.path.join(os.path.dirname(url_server),'show_freq_file_page', token, os.path.basename(freq_file)) \
               )
    files3=Files(ftype1='R',\
            ftype2='Map',\
            ftype3='Txt',\
            ffolder=token,\
            fname=os.path.basename(peak_file),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(peak_file))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server), 'downloadresults',token,os.path.basename(peak_file)),\
            view_link=os.path.join(os.path.dirname(url_server),'show_peak_page' ,token, os.path.basename(peak_file)) \
              )
    files4=Files(ftype1='R',\
            ftype2='Map',\
            ftype3='Txt',\
            ffolder=token,\
            fname=os.path.basename(region_file),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(region_file))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server), 'downloadresults',token,os.path.basename(region_file)),\
            view_link=os.path.join(os.path.dirname(url_server), 'show_region_page' ,token, os.path.basename(region_file)) 
             )
    files5=Files(ftype1='R',\
            ftype2='Map',\
            ftype3='Txt',\
            ffolder=token,\
            fname=prefix+'_threshold.txt',\
            fsize=get_file_size(os.path.join(output_folder,token,prefix+'_threshold.txt')),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server), 'downloadresults',token,prefix+'_threshold.txt'),\
            view_link=os.path.join(os.path.dirname(url_server),'show_threshold_page' ,token, prefix+'_threshold.txt') \
             )
    files6=Files(ftype1='R',\
            ftype2='Map',\
            ftype3='Pdf',\
            ffolder=token,\
            fname=os.path.basename(drawed_pdf),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(drawed_pdf))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(drawed_pdf)),\
            view_link=os.path.join(os.path.dirname(url_server), 'downloadresults',token, os.path.basename(drawed_pdf)) )               
    files7=Files(ftype1='R',\
            ftype2='Map',\
            ftype3='Pdf',\
            ffolder=token,\
            fname=os.path.basename(freq_pdf_file),\
            fsize=get_file_size(os.path.join(output_folder,token,os.path.basename(freq_pdf_file))),\
            owners_name=User.query.filter_by(email=user_email).first().username,\
            owners=User.query.filter_by(email=user_email).first().id,\
            link=os.path.join(os.path.dirname(url_server),'downloadresults',token, os.path.basename(freq_pdf_file)),\
            view_link=os.path.join(os.path.dirname(url_server),'downloadresults',token,os.path.basename(freq_pdf_file)) )
    db.session.add(files1)
    db.session.add(files2)
    db.session.add(files3)
    db.session.add(files4)
    db.session.add(files5)
    db.session.add(files6)
    db.session.add(files7)
    task_db=Tasks.query.filter_by(owners=User.query.filter_by(email=user_email).first().id).filter_by(token_id=token).first()
    if task_db:
        task_db.status='Done'
        task_db.end_time=datetime.utcnow()
        duration_time_in_secondes=(task_db.end_time-task_db.create_time).total_seconds()
        task_db.duration_time=str('%d:%d:%d'%(duration_time_in_secondes/3600,duration_time_in_secondes% 3600/60,duration_time_in_secondes% 3600%60))
        db.session.add(task_db)
    db.session.commit()
    send_email(user_email, 'Your job (%s) done!'%(str(token)),\
                   'auth/email/task_done', url_server=url_server,user=user_email, token=str(token))    
    return {'current': 'Task completed!','result':{'input_type':input_type,'G_file': os.path.basename(G_file), 'pdf_file': os.path.basename(drawed_pdf), 'pcutoff':pcutoff,'gcutoff':gcutoff,'token':token,'freq_pdf_file':os.path.basename(freq_pdf_file),'peak_file':os.path.basename(peak_file),'region_file':os.path.basename(region_file),'freq_file':os.path.basename(freq_file),'threshold_file':prefix+'_threshold.txt'},'status':'Task completed!','progress_bar':1}

    
    
    
    
    


