from flask import jsonify, flash, render_template, session, redirect, url_for, current_app, send_from_directory, request
from flask_security import Security, SQLAlchemyUserDatastore, UserMixin, RoleMixin, login_required
from werkzeug import secure_filename,formparser
from flask_login import login_user, logout_user, login_required, current_user
from . import auth
from .. import db
from ..models import User,Role,Files,Tasks
from ..email import send_email
from .forms import LoginForm, RegistrationForm, ChangePasswordForm,PasswordResetRequestForm, PasswordResetForm, ChangeEmailForm, SubmitForm,IndexForm, EditProfileForm,FastqForm2,FastqForm1,BamForm2,BamForm1,MapForm2,MapForm1,UploadTestForm
from ..decorators import admin_required
import os.path
import time 
from app.tasks.mytask import long_task_bam2,long_task_bam1,long_task_fastq2,long_task_fastq1,long_task_map2,long_task_map1
import uuid
import os 
import subprocess
import pandas as pd
import re, sys
from datetime import datetime
from app import celery
from celery.app.control import Control
import shutil
from collections import OrderedDict
import json
import gzip

basedir = os.path.dirname(os.path.dirname(__file__))

def remove_folder(path):
    # check if folder exists
    if os.path.exists(path):
         # remove if exists
         shutil.rmtree(path)

def get_file_size(f):
    if os.path.isfile(f):
        file_size=os.path.getsize(f)
        if file_size<1000:
            return str(file_size)+'B'
        elif file_size >= 1000 and file_size<1000000:
            return str(file_size/1000)+'K'
        elif file_size >= 1000000 and file_size<1000000000:
            return str(file_size/1000000)+'M'
        elif file_size >= 1000000000 and file_size<1000000000000:
            return str(file_size/1000000000)+'G'
        else:
            return 'Not Available'
    else:
        return 'Not A File'
        
def get_folder_size(start_path):
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            total_size += os.path.getsize(fp)
    return total_size

def count_free_upload_api():
    used_upload=get_folder_size(os.path.join(current_app.config['UPLOADS_FOLDER'],current_user.email))
    free_upload=int(current_app.config['CUSTOM_UPLOADS_FOLDER_SIZE_LIMIT'])-int(used_upload)
    if free_upload>0:
        return free_upload
    else:
        return 'NA'

def count_free_result_api():
    used_result=get_folder_size(os.path.join(current_app.config['RESULTS_FOLDER'],current_user.email))
    free_result=int(current_app.config['CUSTOM_RESULTS_FOLDER_SIZE_LIMIT'])-int(used_result)
    if free_result>0:
        return free_result
    else:
        return 'NA'
        
pd.set_option('display.max_colwidth', -1)  

def flash_errors(form):
    for field, errors in form.errors.items():
        for error in errors:
            flash(u"Error in the %s field - %s" % (getattr(form, field).label.text,error))

class Solution(object):
    def intToRoman(self, num):
        """
        :type num: int
        :rtype: str
        """
        M = ["", "M", "MM", "MMM"]
        C = ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"]
        X = ["", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"]
        I = ["", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"]
        return M[num/1000] + C[(num%1000)/100] + X[(num%100)/10] + I[num%10]
                            
@auth.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.ping()
        if not Files.query.filter_by(owners=current_user.id).filter_by(ftype1 ='U').filter_by(ftype3='Length').filter_by(fname='sacCer3.length').all():
            with open(os.path.join(os.path.dirname(os.path.dirname(__file__)),'genome/sc_chr.length')) as f_in:
                with open(os.path.join(current_app.config['UPLOADS_FOLDER'],current_user.email, 'sacCer3.length'), 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)
                    files1=Files(ftype1='U',ftype2='-',ftype3='Length',ffolder='-',fname='sacCer3.length',fsize=get_file_size(os.path.join(current_app.config['UPLOADS_FOLDER'],current_user.email,'sacCer3.length')),owners=current_user.id,owners_name=current_user.username)
                    db.session.add(files1)
        if not current_user.confirmed \
                and request.endpoint[:5] != 'auth.' \
                and request.endpoint != 'static':
            return redirect(url_for('auth.unconfirmed'))

@auth.route('/unconfirmed')
def unconfirmed():
    if current_user.is_anonymous or current_user.confirmed:
        return redirect(url_for('auth.index'))
    return render_template('auth/unconfirmed.html')


@auth.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm(request.form)
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is not None and user.verify_password(form.password.data):
            login_user(user, form.remember_me.data)
            return redirect(request.args.get('next') or url_for('auth.index'))
        flash('Invalid username or password.')
    return render_template('auth/login.html', form=form)


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    flash('You have been logged out.')
    return redirect(url_for('main.index'))

@auth.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm(request.form)
    if form.validate_on_submit():
        user = User(email=form.email.data,
                    username=form.username.data,
                    password=form.password.data)
        if not os.path.isdir(os.path.join(basedir,'uploads',user.email)):
            os.mkdir(os.path.join(current_app.config['UPLOADS_FOLDER'],user.email))
        else:
            flash('email is already in system. Please register again!')
            return redirect(url_for('auth.register'))
        if not os.path.isdir(os.path.join(basedir,'results',user.email)):
            os.mkdir(os.path.join(basedir,'results',user.email))
        else:
            flash('email is already in system. Please register again!')
            return redirect(url_for('auth.register'))
        db.session.add(user)
        db.session.commit()
        token = user.generate_confirmation_token()
        send_email(user.email, 'Confirm Your Account',
                   'auth/email/confirm', user=user, token=token)
        flash('A confirmation email has been sent to you by email.')
        return redirect(url_for('auth.login'))
    elif request.method == 'POST' and not form.validate():
        flash_errors(form)
#        flash('Incorrect email format or Usernames must have only letters, numbers, dots or underscores.')
        return render_template('auth/register.html', form=form)
    return render_template('auth/register.html', form=form)

@auth.route('/confirm/<token>')
@login_required
def confirm(token):
    if current_user.confirmed:
        return redirect(url_for('auth.index'))
    if current_user.confirm(token):
        flash('You have confirmed your account. Thanks!')
    else:
        flash('The confirmation link is invalid or has expired.')
    return redirect(url_for('main.index'))

@auth.route('/confirm')
@login_required
def resend_confirmation():
    token = current_user.generate_confirmation_token()
    send_email(current_user.email, 'Confirm Your Account',
               'auth/email/confirm', user=current_user, token=token)
    flash('A new confirmation email has been sent to you by email.')
    return redirect(url_for('main.index'))

@auth.route('/change-password', methods=['GET', 'POST'])
@login_required
def change_password():
    form = ChangePasswordForm(request.form)
    if form.validate_on_submit():
        if current_user.verify_password(form.old_password.data):
            current_user.password = form.password.data
            db.session.add(current_user)
            flash('Your password has been updated.')
            return redirect(url_for('main.index'))
        else:
            flash('Invalid password.')
    return render_template("auth/change_password.html", form=form)

@auth.route('/reset', methods=['GET', 'POST'])
def password_reset_request():
    if not current_user.is_anonymous:
        return redirect(url_for('main.index'))
    form = PasswordResetRequestForm(request.form)
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            token = user.generate_reset_token()
            send_email(user.email, 'Reset Your Password',
                       'auth/email/reset_password',
                       user=user, token=token,
                       next=request.args.get('next'))
        flash('An email with instructions to reset your password has been '
              'sent to you.')
        return redirect(url_for('auth.login'))
    return render_template('auth/reset_password.html', form=form)

@auth.route('/reset/<token>', methods=['GET', 'POST'])
def password_reset(token):
    if not current_user.is_anonymous:
        return redirect(url_for('main.index'))
    form = PasswordResetForm(request.form)
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is None:
            return redirect(url_for('main.index'))
        if user.reset_password(token, form.password.data):
            flash('Your password has been updated.')
            return redirect(url_for('auth.login'))
        else:
            return redirect(url_for('main.index'))
    return render_template('auth/reset_password.html', form=form,token=token)

@auth.route('/change-email', methods=['GET', 'POST'])
@login_required
def change_email_request():
    form = ChangeEmailForm(request.form)
    if form.validate_on_submit():
        if current_user.verify_password(form.password.data):
            new_email = form.email.data
            token = current_user.generate_email_change_token(new_email)
            if not os.path.isdir(os.path.join(basedir,'uploads',new_email)):
                os.mkdir(os.path.join(basedir,'uploads',user.email))
            else:
                flash('email is already in system. Please register again!')
                return redirect(url_for('auth.register'))
            if not os.path.isdir(os.path.join(basedir,'results',new_email)):
                os.mkdir(os.path.join(basedir,'results',user.email))
            else:
                flash('email is already in system. Please register again!')
                return redirect(url_for('auth.register'))
            send_email(new_email, 'Confirm your email address',
                       'auth/email/change_email',
                       user=current_user, token=token)
            flash('An email with instructions to confirm your new email '
                  'address has been sent to you.')
            return redirect(url_for('main.index'))
        else:
            flash('Invalid email or password.')
    return render_template("auth/change_email.html", form=form)

@auth.route('/change-email/<token>')
@login_required
def change_email(token):
    if current_user.change_email(token):
        flash('Your email address has been updated.')
    else:
        flash('Invalid request.')
    return redirect(url_for('main.index'))
    
@auth.route('/edit-profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm(request.form)
    if form.validate_on_submit():
        current_user.name = form.name.data
        current_user.location = form.location.data
        current_user.about_me = form.about_me.data
        db.session.add(current_user)
        flash('Your profile has been updated.')
        return redirect(url_for('.Show_user'))
    form.name.data = current_user.name
    form.location.data = current_user.location
    form.about_me.data = current_user.about_me
    return render_template('edit_profile.html', form=form)
    
@auth.route('/edit-profile/<int:id>', methods=['GET', 'POST'])
@login_required
@admin_required
def edit_profile_admin(id):
    user = User.query.get_or_404(id)
    form = EditProfileAdminForm(request.form,user=user)
    if form.validate_on_submit():
        user.email = form.email.data
        user.username = form.username.data
        user.confirmed = form.confirmed.data
        user.role = Role.query.get(form.role.data)
        user.name = form.name.data
        user.location = form.location.data
        user.about_me = form.about_me.data
        db.session.add(user)
        flash('The profile has been updated.')
        return redirect(url_for('.Show_user', username=user.username))
    form.email.data = user.email
    form.username.data = user.username
    form.confirmed.data = user.confirmed
    form.role.data = user.role_id
    form.name.data = user.name
    form.location.data = user.location
    form.about_me.data = user.about_me
    return render_template('edit_profile.html', form=form, user=user)

@auth.route('/edit-admin', methods=['GET', 'POST'])
@login_required
#@admin_required
def edit_admin():
    pass 
    return render_template('view_admin.html')   
#********************main entry**********************    
#@auth.route('/upload', methods=['GET', 'POST'])
#@login_required
#def upload(): 
#    user_upload_folder_size=  get_folder_size(os.path.join(current_app.config['UPLOADS_FOLDER'], current_user.email))
#    if user_upload_folder_size>int(current_app.config['CUSTOM_UPLOADS_FOLDER_SIZE_LIMIT']):
#        flash ('Your upload folder has too many files! Please delete some files first using Uploeaded Files tab at left nav bar.')
#        return render_template('500.html')
#    if request.method == 'POST':
#        f = request.files['file']
#        print "-----------------",request
#        print "-----------------",request.stream
#        print "-----------------",f
#        print "-----------------",f.stream
#        print dir(request)
#        print "-"*30
#        print request.files
#        print request.max_content_length
#        print request.max_form_memory_size
#        print request.cache_control
#        print request.content_length
#        print f
#        f.save(os.path.join(os.path.join(current_app.config['UPLOADS_FOLDER'], current_user.email), f.filename))
#        if f.filename.endswith('.fastq'):
#            files1=Files(ftype1='U',ftype2='-',ftype3='Fastq',ffolder='-',fname=f.filename,fsize=get_file_size(os.path.join(current_app.config['UPLOADS_FOLDER'], current_user.email,f.filename)),owners_name=current_user.username,owners=current_user.id)
#            db.session.add(files1)
#        elif f.filename.endswith('.bam'):
#            files1=Files(ftype1='U',ftype2='-',ftype3='Bam',ffolder='-',fname=f.filename,fsize=get_file_size(os.path.join(current_app.config['UPLOADS_FOLDER'], current_user.email,f.filename)),owners_name=current_user.username,owners=current_user.id)
#            db.session.add(files1)
#        elif f.filename.endswith('.map'):
#            files1=Files(ftype1='U',ftype2='-',ftype3='Map',ffolder='-',fname=f.filename,fsize=get_file_size(os.path.join(current_app.config['UPLOADS_FOLDER'], current_user.email,f.filename)),owners_name=current_user.username,owners=current_user.id)
#            db.session.add(files1) 
#        else:
#            flash('Not right file format!')
#        return redirect(url_for('auth.upload'))
#    return  render_template('upload.html')  
    
@auth.route('/upload_accept', methods=['GET', 'POST'])
@login_required
def upload_accept(): 
    try:
        user_upload_folder_size=  get_folder_size(os.path.join(current_app.config['UPLOADS_FOLDER'], current_user.email))
        if user_upload_folder_size>int(current_app.config['CUSTOM_UPLOADS_FOLDER_SIZE_LIMIT']):
            flash ('Your upload folder has too many files! Please delete some files first using Uploeaded Files tab at left nav bar.')
            return render_template('500.html')
        elif request.method == 'POST' and not request.files['file'].filename.endswith(('.map','.fastq','.fastq.gz','.bam','.length')):
            flash ("The file's name must be ended with '.fastq' ,'.fastq.gz', '.bam', '.map' or '.length'.")  
            return render_template('upload.html')
        elif request.method == 'POST' and Files.query.filter_by(owners=current_user.id).filter_by(fname =request.files['file'].filename).first():
            flash ('The file name already uesed, change another one!')
            return render_template('500.html') 
        elif request.method == 'POST' and request.files['file'].filename.endswith(('.map','.fastq','.fastq.gz','.bam','.length')):  
            upload_file = request.files['file']
            print   upload_file.filename
            print   dir(upload_file)
            print request.form
            print request
            print dir(request)
            task = request.form.get('task_id')            
            chunk = request.form.get('chunk', 0)        
            filename = '%s_%s_%s' % (upload_file.filename,task, chunk)       
            upload_file.save(os.path.join(os.path.join(current_app.config['UPLOADS_FOLDER'], current_user.email), filename)) 
    except Exception,e:
        print "***********************" 
        print task_id
        return redirect(url_for('auth.upload_accept'))      
    return render_template('upload.html') 
    
@auth.route('/upload_complete', methods=['GET'])
@login_required
def upload_complete(): 
    if request.method == 'GET':
        task = request.args.get('task_id')  
        ext = request.args.get('ext', '')  
        upload_type = request.args.get('type') 
        filename= request.args.get('filename') 
        if len(ext) == 0 and upload_type:  
            ext = upload_type.split('/')[1]  
        ext = '' if len(ext) == 0 else '.%s' % ext     
        chunk = 0  
        with open(os.path.join(os.path.join(current_app.config['UPLOADS_FOLDER'], current_user.email), filename), 'w') as target_file: 
            while True:  
                try:  
                    filename_chunk = os.path.join(os.path.join(current_app.config['UPLOADS_FOLDER'], current_user.email), filename+'_'+task+'_'+str(chunk))  
                    source_file = open(filename_chunk, 'r')                 
                    target_file.write(source_file.read())             
                    source_file.close()  
                except IOError:  
                    break  
                chunk += 1  
                os.remove(filename_chunk)
        if filename.endswith('.fastq'):
            files1=Files(ftype1='U',ftype2='-',ftype3='Fastq',ffolder='-',fname=filename,fsize=get_file_size(os.path.join(current_app.config['UPLOADS_FOLDER'], current_user.email,filename)),owners_name=current_user.username,owners=current_user.id)
            db.session.add(files1)
        elif filename.endswith('.fastq.gz'):
#            with gzip.open(os.path.join(os.path.join(current_app.config['UPLOADS_FOLDER'], current_user.email), filename), 'rb') as f_in:
#                with open(os.path.join(os.path.join(current_app.config['UPLOADS_FOLDER'], current_user.email), os.path.splitext(filename)[0]), 'wb') as f_out:
#                    shutil.copyfileobj(f_in, f_out)
#            os.remove(os.path.join(os.path.join(current_app.config['UPLOADS_FOLDER'], current_user.email), filename))
#            files1=Files(ftype1='U',ftype2='-',ftype3='Fastq',ffolder='-',fname=os.path.splitext(filename)[0],fsize=get_file_size(os.path.join(current_app.config['UPLOADS_FOLDER'], current_user.email,os.path.splitext(filename)[0])),owners_name=current_user.username,owners=current_user.id)
            files1=Files(ftype1='U',ftype2='gz',ftype3='Fastq',ffolder='-',fname=filename,fsize=get_file_size(os.path.join(current_app.config['UPLOADS_FOLDER'], current_user.email,filename)),owners_name=current_user.username,owners=current_user.id)
            db.session.add(files1)
        elif filename.endswith('.bam'):
            files1=Files(ftype1='U',ftype2='-',ftype3='Bam',ffolder='-',fname=filename,fsize=get_file_size(os.path.join(current_app.config['UPLOADS_FOLDER'], current_user.email,filename)),owners_name=current_user.username,owners=current_user.id)
            db.session.add(files1)
        elif filename.endswith('.map'):
            files1=Files(ftype1='U',ftype2='-',ftype3='Map',ffolder='-',fname=filename,fsize=get_file_size(os.path.join(current_app.config['UPLOADS_FOLDER'], current_user.email,filename)),owners_name=current_user.username,owners=current_user.id)
            db.session.add(files1) 
        elif filename.endswith('.length'):
            files1=Files(ftype1='U',ftype2='-',ftype3='Length',ffolder='-',fname=filename,fsize=get_file_size(os.path.join(current_app.config['UPLOADS_FOLDER'], current_user.email,filename)),owners_name=current_user.username,owners=current_user.id)
            db.session.add(files1)
        else:
            pass
#        return redirect(url_for('auth.upload_accept'))
        return render_template('upload_success.html')
#        flash('Not right file format!')
#        return redirect(url_for('auth.upload_accept'))                    
#    return render_template('upload.html')  
#    return redirect(url_for('auth.upload_accept'))
#    return render_template('upload.html')
    return render_template('upload_success.html')
    
@auth.route('/analysis1', methods=['GET', 'POST'])
@login_required
def analysis1():
    form = IndexForm(request.form)
    user_result_folder_size=  get_folder_size(os.path.join(current_app.config['RESULTS_FOLDER'], current_user.email))
    if user_result_folder_size>int(current_app.config['CUSTOM_RESULTS_FOLDER_SIZE_LIMIT']):
        flash ('Your result folder has too many files! Please delete some files first using Results Files tab at left nav bar.')
        return render_template('500.html') 
    if form.validate_on_submit() :
        session['input_type'] = str(form.input_type.data)
        session['numbermap'] = form.numbermap.data
        session['result_prefix'] = form.result_prefix.data
        session['bulk_design'] = form.bulk_design.data
        return redirect(url_for('auth.analysis2', input_type=session.get('input_type'), numbermap=session.get('numbermap'), result_prefix=session.get('result_prefix'), bulk_design=session.get('bulk_design')))
    elif request.method == 'POST' and not form.validate():
        flash('Prefix must have only letters, numbers, dots or underscores.')
        return render_template('analysis1.html', form=form)
    return render_template('analysis1.html', form=form)    

@auth.route('/analysis2', methods=['GET', 'POST'])
@login_required
def analysis2():
    input_type=session.get('input_type')
    numbermap = int(session.get('numbermap'))  
    result_prefix=str(session.get('result_prefix'))
    bulk_design=int(session.get('bulk_design'))
    if bulk_design==2 and input_type=='Fastq':
        form = FastqForm2(request.form)
        if not form.is_submitted() :
            for i in range(1, numbermap+1):
                form.Bulkfastqfile.append_entry(i)  
    elif bulk_design==2 and input_type=='Bam':
        form = BamForm2(request.form)
        if not form.is_submitted() :
            for i in range(1, numbermap+1):
                form.Bulkbamfile.append_entry(i)   
    elif bulk_design==2 and input_type=='Map':  
        form = MapForm2(request.form) 
        if not form.is_submitted() :
            for i in range(1, numbermap+1):
                form.Bulkmapfile.append_entry(i) 
            form.length_input.append_entry() 
    elif bulk_design==1 and input_type=='Fastq':
        form = FastqForm1(request.form)
        if not form.is_submitted() :
            for i in range(1, numbermap+1):
                form.Bulkfastqfile.append_entry(i) 
    elif bulk_design==1 and input_type=='Bam':
        form = BamForm1(request.form)  
        if not form.is_submitted() :
            for i in range(1, numbermap+1):
                form.Bulkbamfile.append_entry(i) 
    else:  
        form = MapForm1(request.form) 
        if not form.is_submitted() :
            for i in range(1, numbermap+1):
                form.Bulkmapfile.append_entry()
            form.length_input.append_entry() 
    print    form.data    
    if form.validate_on_submit():
        if bulk_design==2 and input_type=='Bam':
#            print 'success from index2 submit'
#            print form.data.get('P1bamfile')
#            print dir(form.data.get('P1bamfile'))
#            print form.data.get('P1bamfile').fname
            P1bamfile=secure_filename(form.data.get('P1bamfile').fname)
            P2bamfile=secure_filename(form.data.get('P2bamfile').fname)
            Bulkbamfile1 = []
            Bulkbamfile2 = []                
            for i in range(0, numbermap):
#                print form.data.values()
#                print dir(form.data)
#                print form.data.get('Bulkbamfile')[0]['uploadfile1']
                Bulkbamfile1.append(secure_filename(form.data.get('Bulkbamfile')[i]['uploadfile1'].fname))
                Bulkbamfile2.append(secure_filename(form.data.get('Bulkbamfile')[i]['uploadfile2'].fname))
#                print Bulkbamfile1[i]
#                print Bulkbamfile2[i]
#            print form.data
            session['P1bamfile'] = P1bamfile
            session['P2bamfile'] = P2bamfile
            session['Bulkbamfile1'] = Bulkbamfile1
            session['Bulkbamfile2'] = Bulkbamfile2
            session['cov_depth']=int(form.cov_depth.data)
            session['kernel'] = form.kernel.data
            session['width'] = int(form.width.data)
            session['chrnum'] = form.chrnum.data
            session['noraw'] = form.noraw.data
            return redirect(url_for('auth.analysis3',input_type=session.get('input_type'), numbermap=session.get('numbermap'), result_prefix=session.get('result_prefix'), bulk_design=session.get('bulk_design'),\
                                     P1bamfile=session.get('P1bamfile'),P2bamfile=session.get('P2bamfile'),Bulkbamfile1=session.get('Bulkbamfile1'),Bulkbamfile2=session.get('Bulkbamfile2'),\
                                     cov_depth=session.get('cov_depth'),kernel=session.get('kernel'),width=session.get('width'),chrnum=session.get('chrnum'),noraw=session.get('noraw')))
        elif bulk_design==1 and input_type=='Bam':
#            print 'success from index2 submit'
            P1bamfile=secure_filename(form.data.get('P1bamfile').fname)
            P2bamfile=secure_filename(form.data.get('P2bamfile').fname)
            Bulkbamfile1 = []              
            for i in range(0, numbermap):
#                print form.data.values()
#                print dir(form.data)
#                print form.data.get('Bulkbamfile')[0]['uploadfile1']
                Bulkbamfile1.append(secure_filename(form.data.get('Bulkbamfile')[i]['uploadfile1'].fname))
#                print Bulkbamfile1[i]
#            print form.data
            session['P1bamfile'] = P1bamfile
            session['P2bamfile'] = P2bamfile
            session['Bulkbamfile1'] = Bulkbamfile1
            session['cov_depth']=int(form.cov_depth.data)
            session['kernel'] = form.kernel.data
            session['width'] = int(form.width.data)
            session['chrnum'] = form.chrnum.data
            session['noraw'] = form.noraw.data
            return redirect(url_for('auth.analysis3',input_type=session.get('input_type'), numbermap=session.get('numbermap'), result_prefix=session.get('result_prefix'), bulk_design=session.get('bulk_design'),\
                                     P1bamfile=session.get('P1bamfile'),P2bamfile=session.get('P2bamfile'),Bulkbamfile1=session.get('Bulkbamfile1'),\
                                     cov_depth=session.get('cov_depth'),kernel=session.get('kernel'),width=session.get('width'),chrnum=session.get('chrnum'),noraw=session.get('noraw')))
        elif bulk_design==2 and input_type=='Fastq':
#            print 'success from index2 submit'
            P1fastqfile=[]
            P2fastqfile=[]
            P1fastqfile.append(secure_filename(form.data.get('P1fastqfile').fname))
            if 'P1fastqfile2' in request.form:
                k=int(request.form['P1fastqfile2'])
                P1fastqfile.append(secure_filename(Files.query.filter_by(id=k).first().fname))
            P2fastqfile.append(secure_filename(form.data.get('P2fastqfile').fname))
            if 'P2fastqfile2' in request.form:
                k=int(request.form['P2fastqfile2'])
                P2fastqfile.append(secure_filename(Files.query.filter_by(id=k).first().fname))
            Bulkfastqfile1 = []
            Bulkfastqfile2 = []                
            for i in range(0, numbermap):
#                print form.data.values()
#                print dir(form.data)
                print form.data.get('Bulkfastqfile')[0]['uploadfile1']
                Bulkfastqfile1.append([secure_filename(form.data.get('Bulkfastqfile')[i]['uploadfile1'].fname)])
                if 'Bulkfastqfile-'+str(i)+'-uploadfile12' in request.form:
                    k=int(request.form['Bulkfastqfile-'+str(i)+'-uploadfile12'])
                    Bulkfastqfile1[i].append(secure_filename(Files.query.filter_by(id=k).first().fname))
                Bulkfastqfile2.append([secure_filename(form.data.get('Bulkfastqfile')[i]['uploadfile2'].fname)])
                if 'Bulkfastqfile-'+str(i)+'-uploadfile22' in request.form:
                    k=int(request.form['Bulkfastqfile-'+str(i)+'-uploadfile22'])
                    Bulkfastqfile2[i].append(secure_filename(Files.query.filter_by(id=k).first().fname))
                
#                print Bulkfastqfile1[i]
#                print Bulkfastqfile2[i]
#            print form.data
            session['P1fastqfile'] = P1fastqfile
            session['P2fastqfile'] = P2fastqfile
            session['Bulkfastqfile1'] = Bulkfastqfile1
            session['Bulkfastqfile2'] = Bulkfastqfile2
            session['readlen'] = int(form.readlen.data)
            session['phred'] = form.phred.data
            session['qccheck'] = form.qccheck.data
            session['trim'] = form.trim.data
            session['lastkeep'] = form.lastkeep.data
            session['rmadapt'] = form.rmadapt.data
            session['ladapter'] = form.ladapter.data
            session['radapter'] = form.radapter.data
            session['minlen'] = form.minlen.data
            session['removen'] = form.removen.data
            session['ncutoff'] = form.ncutoff.data
            session['filtq'] = form.filtq.data
            session['minq'] = form.minq.data
            session['pminq'] = form.pminq.data
            session['rmunpaired'] = form.rmunpaired.data
            session['qcstat'] = form.qcstat.data
            session['insertsize'] = form.insertsize.data
            session['windowsize'] = form.windowsize.data
            session['cov_depth']=int(form.cov_depth.data)
            session['kernel'] = form.kernel.data
            session['width'] = int(form.width.data)
            session['chrnum'] = form.chrnum.data
            session['noraw'] = form.noraw.data
            return redirect(url_for('auth.analysis3',input_type=session.get('input_type'), numbermap=session.get('numbermap'), result_prefix=session.get('result_prefix'), bulk_design=session.get('bulk_design'),\
                                     P1fastqfile=session.get('P1fastqfile'),P2fastqfile=session.get('P2fastqfile'),Bulkfastqfile1=session.get('Bulkfastqfile1'),Bulkfastqfile2=session.get('Bulkfastqfile2'),\
                                     readlen=session.get('readlen'),phred=session.get('phred'),qccheck=session.get('qccheck'),trim=session.get('trim'),\
                                     lastkeep=session.get('lastkeep'),rmadapt=session.get('rmadapt'),ladapter=session.get('ladapter'),radapter=session.get('radapter'),\
                                     minlen=session.get('minlen'),removen=session.get('removen'),ncutoff=session.get('ncutoff'),filtq=session.get('filtq'),\
                                     minq=session.get('minq'),pminq=session.get('pminq'),rmunpaired=session.get('rmunpaired'),qcstat=session.get('qcstat'),\
                                     insertsize=session.get('insertsize'),windowsize=session.get('windowsize'),\
                                     cov_depth=session.get('cov_depth'),kernel=session.get('kernel'),width=session.get('width'),chrnum=session.get('chrnum'),noraw=session.get('noraw')))            
        elif bulk_design==1 and input_type=='Fastq':
#            print 'success from index2 submit'
            P1fastqfile=[]
            P2fastqfile=[]
            P1fastqfile.append(secure_filename(form.data.get('P1fastqfile').fname))
            if 'P1fastqfile2' in request.form:
                k=int(request.form['P1fastqfile2'])
                P1fastqfile.append(secure_filename(Files.query.filter_by(id=k).first().fname))            
            P2fastqfile.append(secure_filename(form.data.get('P2fastqfile').fname))
            if 'P2fastqfile2' in request.form:
                k=int(request.form['P2fastqfile2'])
                P2fastqfile.append(secure_filename(Files.query.filter_by(id=k).first().fname))            
            Bulkfastqfile1 = []              
            for i in range(0, numbermap):
#                print form.data.values()
#                print dir(form.data)
#                print form.data.get('Bulkfastqfile')[0]['uploadfile1']
                Bulkfastqfile1.append([secure_filename(form.data.get('Bulkfastqfile')[i]['uploadfile1'].fname)])
                if 'Bulkfastqfile-'+str(i)+'-uploadfile12' in request.form:
                    k=int(request.form['Bulkfastqfile-'+str(i)+'-uploadfile12'])
                    Bulkfastqfile1[i].append(secure_filename(Files.query.filter_by(id=k).first().fname))
#                print Bulkfastqfile1[i]
#            print form.data
            session['P1fastqfile'] = P1fastqfile
            session['P2fastqfile'] = P2fastqfile
            session['Bulkfastqfile1'] = Bulkfastqfile1
            session['readlen'] = int(form.readlen.data)
            session['phred'] = form.phred.data
            session['qccheck'] = form.qccheck.data
            session['trim'] = form.trim.data
            session['lastkeep'] = form.lastkeep.data
            session['rmadapt'] = form.rmadapt.data
            session['ladapter'] = form.ladapter.data
            session['radapter'] = form.radapter.data
            session['minlen'] = form.minlen.data
            session['removen'] = form.removen.data
            session['ncutoff'] = form.ncutoff.data
            session['filtq'] = form.filtq.data
            session['minq'] = form.minq.data
            session['pminq'] = form.pminq.data
            session['rmunpaired'] = form.rmunpaired.data
            session['qcstat'] = form.qcstat.data
            session['insertsize'] = form.insertsize.data
            session['windowsize'] = form.windowsize.data
            session['cov_depth']=int(form.cov_depth.data)
            session['kernel'] = form.kernel.data
            session['width'] = int(form.width.data)
            session['chrnum'] = form.chrnum.data
            session['noraw'] = form.noraw.data
            return redirect(url_for('auth.analysis3',input_type=session.get('input_type'), numbermap=session.get('numbermap'), result_prefix=session.get('result_prefix'), bulk_design=session.get('bulk_design'),\
                                     P1fastqfile=session.get('P1fastqfile'),P2fastqfile=session.get('P2fastqfile'),Bulkfastqfile1=session.get('Bulkfastqfile1'),\
                                     readlen=session.get('readlen'),phred=session.get('phred'),qccheck=session.get('qccheck'),trim=session.get('trim'),\
                                     lastkeep=session.get('lastkeep'),rmadapt=session.get('rmadapt'),ladapter=session.get('ladapter'),radapter=session.get('radapter'),\
                                     minlen=session.get('minlen'),removen=session.get('removen'),ncutoff=session.get('ncutoff'),filtq=session.get('filtq'),\
                                     minq=session.get('minq'),pminq=session.get('pminq'),rmunpaired=session.get('rmunpaired'),qcstat=session.get('qcstat'),\
                                     insertsize=session.get('insertsize'),windowsize=session.get('windowsize'),\
                                     cov_depth=session.get('cov_depth'),kernel=session.get('kernel'),width=session.get('width'),chrnum=session.get('chrnum'),noraw=session.get('noraw')))            
        elif bulk_design==2 and input_type=='Map':
#            print 'success from index2 submit' 
            Bulkmapfile1 = []
            Bulkmapfile2 = []                
            for i in range(0, numbermap):
#                print form.data.values()
#                print form.data.get('Bulkmapfile')[0]['uploadfile1']
                Bulkmapfile1.append(secure_filename(form.data.get('Bulkmapfile')[i]['uploadfile1'].fname))
                Bulkmapfile2.append(secure_filename(form.data.get('Bulkmapfile')[i]['uploadfile2'].fname))
#                print Bulkmapfile1[i]
#                print Bulkmapfile2[i]
#            print form.data
            session['Bulkmapfile1'] = Bulkmapfile1
            session['Bulkmapfile2'] = Bulkmapfile2
            session['kernel'] = form.kernel.data
            session['width'] = int(form.width.data)
            session['chrnum'] = form.chrnum.data
            session['noraw'] = form.noraw.data
            session['length_input'] = secure_filename(form.data.get('length_input')[0]['uploadLengthfile'].fname)
            return redirect(url_for('auth.analysis3',input_type=session.get('input_type'), numbermap=session.get('numbermap'), result_prefix=session.get('result_prefix'), bulk_design=session.get('bulk_design'),\
                                     Bulkbamfile1=session.get('Bulkbamfile1'),Bulkbamfile2=session.get('Bulkbamfile2'),\
                                     kernel=session.get('kernel'),width=session.get('width'),chrnum=session.get('chrnum'),noraw=session.get('noraw'),length_input=session.get('length_input')))
        else:
#            print 'success from index2 submit'
            Bulkmapfile1 = []              
            for i in range(0, numbermap):
#                print form.data.values()
#                print dir(form.data)
#                print form.data.get('Bulkmapfile')[0]['uploadfile1']
                Bulkmapfile1.append(secure_filename(form.data.get('Bulkmapfile')[i]['uploadfile1'].fname))
#                print Bulkmapfile1[i]
#            print form.data
            session['Bulkmapfile1'] = Bulkmapfile1
            session['kernel'] = form.kernel.data
            session['width'] = int(form.width.data)
            session['chrnum'] = form.chrnum.data
            session['noraw'] = form.noraw.data
            session['length_input'] = secure_filename(form.data.get('length_input')[0]['uploadLengthfile'].fname)
            return redirect(url_for('auth.analysis3',input_type=session.get('input_type'), numbermap=session.get('numbermap'), result_prefix=session.get('result_prefix'), bulk_design=session.get('bulk_design'),\
                                     Bulkbamfile1=session.get('Bulkbamfile1'),\
                                     kernel=session.get('kernel'),width=session.get('width'),chrnum=session.get('chrnum'),noraw=session.get('noraw'),length_input=session.get('length_input')))
    elif request.method == 'POST' and not form.validate():
        flash('Incorrect input format.')
        return render_template('analysis2.html', form=form,input_type=session.get('input_type'),numbermap=session.get('numbermap'),result_prefix=session.get('result_prefix'),bulk_design=session.get('bulk_design'))
    return render_template('analysis2.html', form=form,input_type=session.get('input_type'),numbermap=session.get('numbermap'),result_prefix=session.get('result_prefix'),bulk_design=session.get('bulk_design'))

@auth.route('/analysis3', methods=[ 'GET', 'POST'])
@login_required
def analysis3():
    form = SubmitForm(request.form)
    input_type=session.get('input_type')
    numbermap = int(session.get('numbermap'))  
    result_prefix=str(session.get('result_prefix'))
    bulk_design=int(session.get('bulk_design'))
    if bulk_design==2 and input_type=='Bam':
        P1bamfile=session.get('P1bamfile')
        P2bamfile=session.get('P2bamfile')
        Bulkbamfile1=session.get('Bulkbamfile1')
        Bulkbamfile2=session.get('Bulkbamfile2')
        cov_depth=session.get('cov_depth')
        kernel=session.get('kernel')
        width=session.get('width')
        chrnum=session.get('chrnum')
        noraw=session.get('noraw')
    elif bulk_design==1 and input_type=='Bam':
        P1bamfile=session.get('P1bamfile')
        P2bamfile=session.get('P2bamfile')
        Bulkbamfile1=session.get('Bulkbamfile1')
        cov_depth=session.get('cov_depth')
        kernel=session.get('kernel')
        width=session.get('width')
        chrnum=session.get('chrnum')
        noraw=session.get('noraw')        
    elif bulk_design==2 and input_type=='Fastq':
        P1fastqfile=session.get('P1fastqfile')
        P2fastqfile=session.get('P2fastqfile')
        Bulkfastqfile1=session.get('Bulkfastqfile1')
        Bulkfastqfile2=session.get('Bulkfastqfile2')
        readlen=session.get('readlen')
        phred=session.get('phred')
        qccheck=session.get('qccheck')
        trim=session.get('trim')
        lastkeep=session.get('lastkeep')
        rmadapt=session.get('rmadapt')
        ladapter=session.get('ladapter')
        radapter=session.get('radapter')
        minlen=session.get('minlen')
        removen=session.get('removen')
        ncutoff=session.get('ncutoff')
        filtq=session.get('filtq')
        minq=session.get('minq')
        pminq=session.get('pminq')
        rmunpaired=session.get('rmunpaired')
        qcstat=session.get('qcstat')
        insertsize=session.get('insertsize')
        windowsize=session.get('windowsize')
        cov_depth=session.get('cov_depth')
        kernel=session.get('kernel')
        width=session.get('width')
        chrnum=session.get('chrnum')
        noraw=session.get('noraw')
    elif bulk_design==1 and input_type=='Fastq':
        P1fastqfile=session.get('P1fastqfile')
        P2fastqfile=session.get('P2fastqfile')
        Bulkfastqfile1=session.get('Bulkfastqfile1')
        readlen=session.get('readlen')
        phred=session.get('phred')
        qccheck=session.get('qccheck')
        trim=session.get('trim')
        lastkeep=session.get('lastkeep')
        rmadapt=session.get('rmadapt')
        ladapter=session.get('ladapter')
        radapter=session.get('radapter')
        minlen=session.get('minlen')
        removen=session.get('removen')
        ncutoff=session.get('ncutoff')
        filtq=session.get('filtq')
        minq=session.get('minq')
        pminq=session.get('pminq')
        rmunpaired=session.get('rmunpaired')
        qcstat=session.get('qcstat')
        insertsize=session.get('insertsize')
        windowsize=session.get('windowsize')
        cov_depth=session.get('cov_depth')
        kernel=session.get('kernel')
        width=session.get('width')
        chrnum=session.get('chrnum')
        noraw=session.get('noraw')
    elif bulk_design==2 and input_type=='Map':
        Bulkmapfile1=session.get('Bulkmapfile1')
        Bulkmapfile2=session.get('Bulkmapfile2')
        kernel=session.get('kernel')
        length_input=session.get('length_input')
        width=session.get('width')
        chrnum=session.get('chrnum')
        noraw=session.get('noraw')
    else:
        Bulkmapfile1=session.get('Bulkmapfile1')
        kernel=session.get('kernel')
        length_input=session.get('length_input')
        width=session.get('width')
        chrnum=session.get('chrnum')
        noraw=session.get('noraw')
    if chrnum == 'all':
        chrnum = 0
    else:
        chrnum = int(chrnum)
    noraw_label = ''
    if noraw == 'yes':
        noraw_label = 'yes'
    else:
        noraw_label = ''
#    print form.errors
#    if form.is_submitted():
#        print "submitted"
#    if form.validate():
#        print "valid"
#    print form.errors
    if form.validate_on_submit() :
#        print 'success from upload submit'
        token = str(uuid.uuid4())
        output_folder = os.path.join(current_app.config['RESULTS_FOLDER'],current_user.email)
        if input_type=='Bam':
            P1bamfilelist=os.path.join(current_app.config['UPLOADS_FOLDER'],current_user.email,P1bamfile)
            P2bamfilelist=os.path.join(current_app.config['UPLOADS_FOLDER'],current_user.email,P2bamfile)
            Bulkbamfile1list = []
            if bulk_design==2:
                Bulkbamfile2list = []
            for i in Bulkbamfile1:
                Bulkbamfile1list.append(os.path.join(current_app.config['UPLOADS_FOLDER'],current_user.email, i))
            if bulk_design==2:
                for i in Bulkbamfile2:
                    Bulkbamfile2list.append(os.path.join(current_app.config['UPLOADS_FOLDER'],current_user.email, i))
            if bulk_design==2:
                Hbamlist = ' '.join([i for i in Bulkbamfile1list])
                Lbamlist = ' '.join([i for i in Bulkbamfile2list])
                session['Lbamlist'] = Lbamlist
                session['Hbamlist'] = Hbamlist
            else:
                bamlist = ' '.join([i for i in Bulkbamfile1list])
                session['bamlist'] = bamlist
        elif input_type=='Fastq':
#            P1fastqfilelist=os.path.join(current_app.config['UPLOADS_FOLDER'],current_user.email,P1fastqfile)
            P1fastqfilelist=[os.path.join(current_app.config['UPLOADS_FOLDER'],current_user.email,i) for i in P1fastqfile]
#            P2fastqfilelist=os.path.join(current_app.config['UPLOADS_FOLDER'],current_user.email,P2fastqfile)
            P2fastqfilelist=[os.path.join(current_app.config['UPLOADS_FOLDER'],current_user.email,i) for i in P2fastqfile]
            Bulkfastqfile1list = []
            if bulk_design==2:
                Bulkfastqfile2list = []
            for i in range(len(Bulkfastqfile1)):
#                Bulkfastqfile1list.append(os.path.join(current_app.config['UPLOADS_FOLDER'],current_user.email, i))
                print "-----------------------",i
                Bulkfastqfile1list.append([os.path.join(current_app.config['UPLOADS_FOLDER'],current_user.email, j) for j in Bulkfastqfile1[i]])
            print "-----------------------",Bulkfastqfile1list
            if bulk_design==2:
                for i in range(len(Bulkfastqfile2)):
#                    Bulkfastqfile2list.append(os.path.join(current_app.config['UPLOADS_FOLDER'],current_user.email, i))
                    Bulkfastqfile2list.append([os.path.join(current_app.config['UPLOADS_FOLDER'],current_user.email, j) for j in Bulkfastqfile2[i]])
            if bulk_design==2:
#                Hfastqlist = ' '.join([i for i in Bulkfastqfile1list])
#                Lfastqlist = ' '.join([i for i in Bulkfastqfile2list])
#                session['Lfastqlist'] = Lfastqlist
#                session['Hfastqlist'] = Hfastqlist
                Hfastqlist=Bulkfastqfile1list
                Lfastqlist =Bulkfastqfile2list
                session['Lfastqlist'] = Lfastqlist
                session['Hfastqlist'] = Hfastqlist
                print "###############",Hfastqlist
                print "###############",Lfastqlist
            else:
#                fastqlist = ' '.join([i for i in Bulkfastqfile1list])
#                session['fastqlist'] = fastqlist
                fastqlist = Bulkfastqfile1list
                session['fastqlist'] = fastqlist
        else:
            Bulkmapfile1list = []
            if bulk_design==2:
                Bulkmapfile2list = []
            for i in Bulkmapfile1:
                Bulkmapfile1list.append(os.path.join(current_app.config['UPLOADS_FOLDER'],current_user.email, i))
            if bulk_design==2:
                for i in Bulkmapfile2:
                    Bulkmapfile2list.append(os.path.join(current_app.config['UPLOADS_FOLDER'],current_user.email, i))
            if bulk_design==2:
                Hmaplist = ' '.join([i for i in Bulkmapfile1list])
                Lmaplist = ' '.join([i for i in Bulkmapfile2list])
                session['Lmaplist'] = Lmaplist
                session['Hmaplist'] = Hmaplist
            else:
                maplist = ' '.join([i for i in Bulkmapfile1list])
                session['maplist'] = maplist
            length_input_file=os.path.join(current_app.config['UPLOADS_FOLDER'],current_user.email, length_input)
            session['length_input_file'] = length_input_file
        session['token'] = token
        if bulk_design==2 and input_type=='Bam':
            task_db=Tasks(owners=current_user.id,owners_name=current_user.username,token_id=token,status='Running')
            task_db.create_time=datetime.utcnow()
            db.session.add(task_db)
            db.session.commit()
            return redirect(url_for('auth.longtask',output_folder=output_folder,url_server=url_for('auth.index',_external=True),token=token,\
            input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
            P1bamfile=P1bamfilelist,\
            P2bamfile=P2bamfilelist,\
            Hbamlist=Hbamlist,\
            Lbamlist=Lbamlist, \
            cov_depth=cov_depth,\
            kernel=kernel,\
            width=width,\
            chrnum=chrnum,\
            noraw=noraw))
        elif bulk_design==1 and input_type=='Bam':
            task_db=Tasks(owners=current_user.id,owners_name=current_user.username,token_id=token,status='Running')
            task_db.create_time=datetime.utcnow()
            db.session.add(task_db)
            db.session.commit()
            return redirect(url_for('auth.longtask',output_folder=output_folder,url_server=url_for('auth.index',_external=True),token=token,\
            input_type=input_type,numbermap = int(numbermap)  ,result_prefix=result_prefix,bulk_design=int(bulk_design),\
            P1bamfile=P1bamfilelist,\
            P2bamfile=P2bamfilelist,\
            bamlist=bamlist,\
            cov_depth=int(cov_depth),\
            kernel=kernel,\
            width=int(width),\
            chrnum=chrnum,\
            noraw=noraw))
        elif bulk_design==2 and input_type=='Fastq':
            task_db=Tasks(owners=current_user.id,owners_name=current_user.username,token_id=token,status='Running')
            task_db.create_time=datetime.utcnow()
            db.session.add(task_db)
            db.session.commit()
            print "!!!!!!!!!!!!!!",Hfastqlist
            print "!!!!!!!!!!!!!",Lfastqlist
            return redirect(url_for('auth.longtask', output_folder=output_folder,url_server=url_for('auth.index',_external=True),token=token,input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
            P1fastqfile=json.dumps(P1fastqfilelist),\
            P2fastqfile=json.dumps(P2fastqfilelist),\
            Hfastqlist=json.dumps(Hfastqlist),\
            Lfastqlist=json.dumps(Lfastqlist),\
            readlen=readlen,\
            phred=phred,\
            qccheck=qccheck,\
            trim=trim,\
            lastkeep=lastkeep,\
            rmadapt=rmadapt,\
            ladapter=ladapter,\
            radapter=radapter,\
            minlen=minlen,\
            removen=removen,\
            ncutoff=ncutoff,\
            filtq=filtq,\
            minq=minq,\
            pminq=pminq,\
            rmunpaired=rmunpaired,\
            qcstat=qcstat,\
            insertsize=insertsize,\
            windowsize=windowsize,\
            cov_depth=cov_depth,\
            kernel=kernel,\
            width=width,\
            chrnum=chrnum,\
            noraw=noraw\
            ))
        elif bulk_design==1 and input_type=='Fastq':
            task_db=Tasks(owners=current_user.id,owners_name=current_user.username,token_id=token,status='Running')
            task_db.create_time=datetime.utcnow()
            db.session.add(task_db)
            db.session.commit()
            return redirect(url_for('auth.longtask',output_folder=output_folder,url_server=url_for('auth.index',_external=True),token=token, input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
            P1fastqfile=json.dumps(P1fastqfilelist),\
            P2fastqfile=json.dumps(P2fastqfilelist),\
            fastqlist=json.dumps(fastqlist),\
            readlen=readlen,\
            phred=phred,\
            qccheck=qccheck,\
            trim=trim,\
            lastkeep=lastkeep,\
            rmadapt=rmadapt,\
            ladapter=ladapter,\
            radapter=radapter,\
            minlen=minlen,\
            removen=removen,\
            ncutoff=ncutoff,\
            filtq=filtq,\
            minq=minq,\
            pminq=pminq,\
            rmunpaired=rmunpaired,\
            qcstat=qcstat,\
            insertsize=insertsize,\
            windowsize=windowsize,\
            cov_depth=cov_depth,\
            kernel=kernel,\
            width=width,\
            chrnum=chrnum,\
            noraw=noraw\
            ))
        elif bulk_design==2 and input_type=='Map':
            task_db=Tasks(owners=current_user.id,owners_name=current_user.username,token_id=token,status='Running')
            task_db.create_time=datetime.utcnow()
            db.session.add(task_db)
            db.session.commit()
            return redirect(url_for('auth.longtask', output_folder=output_folder,url_server=url_for('auth.index',_external=True),token=token,input_type=input_type,numbermap = numbermap ,result_prefix=result_prefix,bulk_design=bulk_design,\
            Bulkmapfile1=Bulkmapfile1,\
            Bulkmapfile2=Bulkmapfile2,\
            kernel=kernel,\
            length_input=length_input,\
            width=width,\
            chrnum=chrnum,\
            noraw=noraw,\
             length_input_file=length_input_file\
            ))
        else:
            task_db=Tasks(owners=current_user.id,owners_name=current_user.username,token_id=token,status='Running')
            task_db.create_time=datetime.utcnow()
            db.session.add(task_db)
            db.session.commit()
            return redirect(url_for('auth.longtask',output_folder=output_folder,url_server=url_for('auth.index',_external=True),token=token, input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
            Bulkmapfile1=Bulkmapfile1,\
            kernel=kernel,\
            length_input=length_input,\
            width=width,\
            chrnum=chrnum,\
            noraw=noraw,\
            length_input_file=length_input_file\
            ))
    elif request.method == 'POST' and not form.validate():
        flash('Incorrect input format.')
        if bulk_design==2 and input_type=='Bam':
            return render_template('analysis3.html', form=form, input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
        P1bamfile=P1bamfile,\
        P2bamfile=P2bamfile,\
        Bulkbamfile1=Bulkbamfile1,\
        Bulkbamfile2=Bulkbamfile2,\
        cov_depth=cov_depth,\
        kernel=kernel,\
        width=width,\
        chrnum=chrnum,\
        noraw=noraw)
        elif bulk_design==1 and input_type=='Bam':
            return render_template('analysis3.html', form=form, input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
        P1bamfile=P1bamfile,\
        P2bamfile=P2bamfile,\
        Bulkbamfile1=Bulkbamfile1,\
        cov_depth=cov_depth,\
        kernel=kernel,\
        width=width,\
        chrnum=chrnum,\
        noraw=noraw)
        elif bulk_design==2 and input_type=='Fastq':
            return render_template('analysis3.html', form=form, input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
        P1fastqfile=P1fastqfile,\
        P2fastqfile=P2fastqfile,\
        Bulkfastqfile1=Bulkfastqfile1,\
        Bulkfastqfile2=Bulkfastqfile2,\
        readlen=readlen,\
        phred=phred,\
        qccheck=qccheck,\
        trim=trim,\
        lastkeep=lastkeep,\
        rmadapt=rmadapt,\
        ladapter=ladapter,\
        radapter=radapter,\
        minlen=minlen,\
        removen=removen,\
        ncutoff=ncutoff,\
        filtq=filtq,\
        minq=minq,\
        pminq=pminq,\
        rmunpaired=rmunpaired,\
        qcstat=qcstat,\
        insertsize=insertsize,\
        windowsize=windowsize,\
        cov_depth=cov_depth,\
        kernel=kernel,\
        width=width,\
        chrnum=chrnum,\
        noraw=noraw\
        )        
        elif bulk_design==1 and input_type=='Fastq':
            return render_template('analysis3.html', form=form, input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
        P1fastqfile=P1fastqfile,\
        P2fastqfile=P2fastqfile,\
        Bulkfastqfile1=Bulkfastqfile1,\
        readlen=readlen,\
        phred=phred,\
        qccheck=qccheck,\
        trim=trim,\
        lastkeep=lastkeep,\
        rmadapt=rmadapt,\
        ladapter=ladapter,\
        radapter=radapter,\
        minlen=minlen,\
        removen=removen,\
        ncutoff=ncutoff,\
        filtq=filtq,\
        minq=minq,\
        pminq=pminq,\
        rmunpaired=rmunpaired,\
        qcstat=qcstat,\
        insertsize=insertsize,\
        windowsize=windowsize,\
        cov_depth=cov_depth,\
        kernel=kernel,\
        width=width,\
        chrnum=chrnum,\
        noraw=noraw\
        )
        elif bulk_design==2 and input_type=='Map':
            return render_template('analysis3.html', form=form, input_type=input_type,numbermap = numbermap ,result_prefix=result_prefix,bulk_design=bulk_design,\
        Bulkmapfile1=Bulkmapfile1,\
        Bulkmapfile2=Bulkmapfile2,\
        kernel=kernel,\
        length_input=length_input,\
        width=width,\
        chrnum=chrnum,\
        noraw=noraw\
        )
        else:
            return render_template('analysis3.html', form=form, input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
        Bulkmapfile1=Bulkmapfile1,\
        kernel=kernel,\
        length_input=length_input,\
        width=width,\
        chrnum=chrnum,\
        noraw=noraw\
        )
    if bulk_design==2 and input_type=='Bam':
        return render_template('analysis3.html', form=form, input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
        P1bamfile=P1bamfile,\
        P2bamfile=P2bamfile,\
        Bulkbamfile1=Bulkbamfile1,\
        Bulkbamfile2=Bulkbamfile2,\
        cov_depth=cov_depth,\
        kernel=kernel,\
        width=width,\
        chrnum=chrnum,\
        noraw=noraw)
    elif bulk_design==1 and input_type=='Bam':
        return render_template('analysis3.html', form=form, input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
        P1bamfile=P1bamfile,\
        P2bamfile=P2bamfile,\
        Bulkbamfile1=Bulkbamfile1,\
        cov_depth=cov_depth,\
        kernel=kernel,\
        width=width,\
        chrnum=chrnum,\
        noraw=noraw)
    elif bulk_design==2 and input_type=='Fastq':
        return render_template('analysis3.html', form=form, input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
        P1fastqfile=P1fastqfile,\
        P2fastqfile=P2fastqfile,\
        Bulkfastqfile1=Bulkfastqfile1,\
        Bulkfastqfile2=Bulkfastqfile2,\
        readlen=readlen,\
        phred=phred,\
        qccheck=qccheck,\
        trim=trim,\
        lastkeep=lastkeep,\
        rmadapt=rmadapt,\
        ladapter=ladapter,\
        radapter=radapter,\
        minlen=minlen,\
        removen=removen,\
        ncutoff=ncutoff,\
        filtq=filtq,\
        minq=minq,\
        pminq=pminq,\
        rmunpaired=rmunpaired,\
        qcstat=qcstat,\
        insertsize=insertsize,\
        windowsize=windowsize,\
        cov_depth=cov_depth,\
        kernel=kernel,\
        width=width,\
        chrnum=chrnum,\
        noraw=noraw\
        )        
    elif bulk_design==1 and input_type=='Fastq':
        return render_template('analysis3.html', form=form, input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
        P1fastqfile=P1fastqfile,\
        P2fastqfile=P2fastqfile,\
        Bulkfastqfile1=Bulkfastqfile1,\
        readlen=readlen,\
        phred=phred,\
        qccheck=qccheck,\
        trim=trim,\
        lastkeep=lastkeep,\
        rmadapt=rmadapt,\
        ladapter=ladapter,\
        radapter=radapter,\
        minlen=minlen,\
        removen=removen,\
        ncutoff=ncutoff,\
        filtq=filtq,\
        minq=minq,\
        pminq=pminq,\
        rmunpaired=rmunpaired,\
        qcstat=qcstat,\
        insertsize=insertsize,\
        windowsize=windowsize,\
        cov_depth=cov_depth,\
        kernel=kernel,\
        width=width,\
        chrnum=chrnum,\
        noraw=noraw\
        )
    elif bulk_design==2 and input_type=='Map':
        return render_template('analysis3.html', form=form, input_type=input_type,numbermap = numbermap ,result_prefix=result_prefix,bulk_design=bulk_design,\
        Bulkmapfile1=Bulkmapfile1,\
        Bulkmapfile2=Bulkmapfile2,\
        kernel=kernel,\
        length_input=length_input,\
        width=width,\
        chrnum=chrnum,\
        noraw=noraw\
        )
    else:
        return render_template('analysis3.html', form=form, input_type=input_type,numbermap = numbermap  ,result_prefix=result_prefix,bulk_design=bulk_design,\
        Bulkmapfile1=Bulkmapfile1,\
        kernel=kernel,\
        length_input=length_input,\
        width=width,\
        chrnum=chrnum,\
        noraw=noraw\
        )

@auth.route('/longtask', methods=['GET', 'POST'])
@login_required
def longtask():
    params = request.args.items()
    print "@@@@@@@@@@@@@@@@@@@@@@@@",params
    for i, j in params:
        session[i] = j.encode("ascii")
    if int(session.get('bulk_design'))==2 and session.get('input_type')=='Bam' :
        output_folder=session.get('output_folder')
        url_server=session.get('url_server')
        token=session.get('token')
#        task_id=session.get('task_id')
        input_type=session.get('input_type')
        numbermap = int(session.get('numbermap'))
        result_prefix=session.get('result_prefix')
        bulk_design=int(session.get('bulk_design'))
        P1bamfile=session.get('P1bamfile')
        P2bamfile=session.get('P2bamfile')
        Hbamlist=session.get('Hbamlist')
        Lbamlist=session.get('Lbamlist')
        cov_depth=int(session.get('cov_depth'))
        kernel=session.get('kernel')
        width=int(session.get('width'))
        chrnum=session.get('chrnum')
        noraw=session.get('noraw')
        print "*************",Hbamlist
        print "*************",Lbamlist
        task = long_task_bam2.apply_async(args=[output_folder,url_server,token,input_type,numbermap,result_prefix,bulk_design,P1bamfile,P2bamfile,Hbamlist,Lbamlist,cov_depth,kernel,width,chrnum,noraw],task_id=session.get('token'))
        session['input_type'] = input_type
        session['bulk_design'] = bulk_design
        return render_template('waiting.html',task_id=session.get('token'),token=session.get('token'))
    elif int(session.get('bulk_design'))==1 and session.get('input_type')=='Bam' :
        output_folder=session.get('output_folder')
        url_server=session.get('url_server')
        token=session.get('token')
#        task_id=session.get('task_id')
        input_type=session.get('input_type')
        numbermap = int(session.get('numbermap') )
        result_prefix=session.get('result_prefix')
        bulk_design=int(session.get('bulk_design'))
        P1bamfile=session.get('P1bamfile')
        P2bamfile=session.get('P2bamfile')
        bamlist=session.get('bamlist')
        cov_depth=int(session.get('cov_depth'))
        kernel=session.get('kernel')
        width=int(session.get('width'))
        chrnum=session.get('chrnum')
        noraw=session.get('noraw')
        task = long_task_bam1.apply_async(args=[output_folder,url_server,token,input_type,numbermap,result_prefix,bulk_design,P1bamfile,P2bamfile,bamlist,cov_depth,kernel,width,chrnum,noraw],task_id=session.get('token'))
#        session['task_id'] = task.id
        session['input_type'] = input_type
        session['bulk_design'] = bulk_design
        return render_template('waiting.html',task_id=session.get('token'),token=session.get('token'))
    elif int(session.get('bulk_design'))==2 and session.get('input_type')=='Fastq' :
        output_folder=session.get('output_folder')
        url_server=session.get('url_server')
        token=session.get('token')
#        task_id=session.get('task_id')
        input_type=session.get('input_type')
        numbermap = int(session.get('numbermap')) 
        result_prefix=session.get('result_prefix')
        bulk_design=session.get('bulk_design')
        P1fastqfile=session.get('P1fastqfile')
        P2fastqfile=session.get('P2fastqfile')
        Hfastqlist=session.get('Hfastqlist')
        Lfastqlist=session.get('Lfastqlist')
        readlen=session.get('readlen')
        phred=session.get('phred')
        qccheck=session.get('qccheck')
        trim=session.get('trim')
        lastkeep=session.get('lastkeep')
        rmadapt=session.get('rmadapt')
        ladapter=session.get('ladapter')
        radapter=session.get('radapter')
        minlen=session.get('minlen')
        removen=session.get('removen')
        ncutoff=session.get('ncutoff')
        filtq=session.get('filtq')
        minq=session.get('minq')
        pminq=session.get('pminq')
        rmunpaired=session.get('rmunpaired')
        qcstat=session.get('qcstat')
        insertsize=session.get('insertsize')
        windowsize=session.get('windowsize')
        cov_depth=int(session.get('cov_depth'))
        kernel=session.get('kernel')
        width=int(session.get('width'))
        chrnum=session.get('chrnum')
        noraw=session.get('noraw')
        task = long_task_fastq2.apply_async(args=[output_folder,url_server,token,input_type,numbermap,result_prefix,bulk_design,P1fastqfile,P2fastqfile,Hfastqlist,Lfastqlist,readlen,phred,qccheck,trim,lastkeep,rmadapt,ladapter,radapter,minlen,removen,ncutoff,filtq,minq,pminq,rmunpaired,qcstat,insertsize,windowsize,cov_depth,kernel,width,chrnum,noraw],task_id=session.get('token'))
#        session['task_id'] = task.id
        session['input_type'] = input_type
        session['bulk_design'] = bulk_design
        return render_template('waiting.html',task_id=session.get('token'),token=session.get('token'))
    elif int(session.get('bulk_design'))==1 and session.get('input_type')=='Fastq' :
        output_folder=session.get('output_folder')
        url_server=session.get('url_server')
        token=session.get('token')
#        task_id=session.get('task_id')
        input_type=session.get('input_type')
        numbermap = int(session.get('numbermap') )
        result_prefix=session.get('result_prefix')
        bulk_design=int(session.get('bulk_design'))
        P1fastqfile=session.get('P1fastqfile')
        P2fastqfile=session.get('P2fastqfile')
        fastqlist=session.get('fastqlist')
        readlen=session.get('readlen')
        phred=session.get('phred')
        qccheck=session.get('qccheck')
        trim=session.get('trim')
        lastkeep=session.get('lastkeep')
        rmadapt=session.get('rmadapt')
        ladapter=session.get('ladapter')
        radapter=session.get('radapter')
        minlen=session.get('minlen')
        removen=session.get('removen')
        ncutoff=session.get('ncutoff')
        filtq=session.get('filtq')
        minq=session.get('minq')
        pminq=session.get('pminq')
        rmunpaired=session.get('rmunpaired')
        qcstat=session.get('qcstat')
        insertsize=session.get('insertsize')
        windowsize=session.get('windowsize')
        cov_depth=int(session.get('cov_depth'))
        kernel=session.get('kernel')
        width=int(session.get('width'))
        chrnum=session.get('chrnum')
        noraw=session.get('noraw')
        task = long_task_fastq1.apply_async(args=[output_folder,url_server,token,input_type,numbermap,result_prefix,bulk_design,P1fastqfile,P2fastqfile,fastqlist,readlen,phred,qccheck,trim,lastkeep,rmadapt,ladapter,radapter,minlen,removen,ncutoff,filtq,minq,pminq,rmunpaired,qcstat,insertsize,windowsize,cov_depth,kernel,width,chrnum,noraw],task_id=session.get('token'))
#        session['task_id'] = task.id
        session['input_type'] = input_type
        session['bulk_design'] = bulk_design
        return render_template('waiting.html',task_id=session.get('token'),token=session.get('token'))
    elif int(session.get('bulk_design'))==2 and session.get('input_type')=='Map' :
        output_folder=session.get('output_folder')
        url_server=session.get('url_server')
        token=session.get('token')
 #       task_id=session.get('task_id')
        input_type=session.get('input_type')
        numbermap = int(session.get('numbermap')) 
        result_prefix=session.get('result_prefix')
        bulk_design=int(session.get('bulk_design'))
        Hmaplist=session.get('Hmaplist')
        Lmaplist=session.get('Lmaplist')
        kernel=session.get('kernel')
        length_input_file=session.get('length_input_file')
        width=int(session.get('width'))
        chrnum=session.get('chrnum')
        noraw=session.get('noraw')
        task = long_task_map2.apply_async(args=[output_folder,url_server,token,input_type,numbermap,result_prefix,bulk_design,Hmaplist,Lmaplist,kernel,width,chrnum,noraw,length_input_file],task_id=session.get('token'))
#        session['task_id'] = task.id
        session['input_type'] = input_type
        session['bulk_design'] = bulk_design
        return render_template('waiting.html',task_id=session.get('token'),token=session.get('token'))
    else :
        output_folder=session.get('output_folder')
        url_server=session.get('url_server')
        token=session.get('token')
 #       task_id=session.get('task_id')
        input_type=session.get('input_type')
        numbermap = int(session.get('numbermap')) 
        result_prefix=session.get('result_prefix')
        bulk_design=int(session.get('bulk_design'))
        maplist=session.get('maplist')
        kernel=session.get('kernel')
        length_input_file=session.get('length_input_file')
        width=int(session.get('width'))
        chrnum=session.get('chrnum')
        noraw=session.get('noraw')
        task = long_task_map1.apply_async(args=[output_folder,url_server,token,input_type,numbermap,result_prefix,bulk_design,maplist,kernel,width,chrnum,noraw,length_input_file],task_id=session.get('token'))
#        session['task_id'] = task.id
        session['input_type'] = input_type
        session['bulk_design'] = bulk_design
        return render_template('waiting.html',task_id=session.get('token'),token=session.get('token'))

@auth.route('/longtask_run_example_fastq', methods=['GET', 'POST'])
@login_required
def longtask_run_example_fastq():
    token = str(uuid.uuid4())
    output_folder = os.path.join(current_app.config['RESULTS_FOLDER'],current_user.email)
    session['token'] = token
    bulk_design=2
    input_type='Fastq'
    numbermap=1
    result_prefix="myresult"
    cov_depth=10
    kernel='tricube'
    width=33750
    chrnum=0
    noraw='yes'
    P1fastqfilelist=[os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','sake_strain_P1_1.fastq'),os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','sake_strain_P1_2.fastq')]
    P2fastqfilelist=[os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','white_tecc_strain_P2_1.fastq'),os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','white_tecc_strain_P2_2.fastq')]
    Hfastqlist=[[os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','Bulk_H.fastq')]]
    Lfastqlist=[[os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','Bulk_L.fastq')]]
    optional_email=''
    task_db=Tasks(owners=current_user.id,owners_name=current_user.username,token_id=token,status='Running')
    task_db.create_time=datetime.utcnow()
    db.session.add(task_db)
    db.session.commit()
    return redirect(url_for('auth.longtask', output_folder=output_folder,url_server=url_for('auth.index',_external=True),token=token,input_type=input_type,numbermap = numbermap ,result_prefix=result_prefix,bulk_design=bulk_design,\
            P1fastqfile=json.dumps(P1fastqfilelist),\
            P2fastqfile=json.dumps(P2fastqfilelist),\
            Hfastqlist=json.dumps(Hfastqlist),\
            Lfastqlist=json.dumps(Lfastqlist),\
            readlen="",\
            phred="",\
            qccheck="",\
            trim="",\
            lastkeep="",\
            rmadapt="",\
            ladapter="",\
            radapter="",\
            minlen="",\
            removen="",\
            ncutoff="",\
            filtq="",\
            minq="",\
            pminq="",\
            rmunpaired="",\
            qcstat="",\
            insertsize="",\
            windowsize="",\
            cov_depth=cov_depth,\
            kernel=kernel,\
            width=width,\
            chrnum=chrnum,\
            noraw=noraw\
            ))
    
@auth.route('/longtask_run_example_bam', methods=['GET', 'POST'])
@login_required
def longtask_run_example_bam():
    token = str(uuid.uuid4())
    output_folder = os.path.join(current_app.config['RESULTS_FOLDER'],current_user.email)
    session['token'] = token
    bulk_design=2
    input_type='Bam'
    numbermap=1
    result_prefix="myresult"
    P1bamfilelist=os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','sake_strain_P1.bam')
    P2bamfilelist=os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','white_tecc_strain_P2.bam')
    Hbamlist=os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','Bulk_H.bam')
    Lbamlist=os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','Bulk_L.bam')
    cov_depth=10
    kernel='tricube'
    width=33750
    chrnum=0
    noraw='yes'
    session['Lbamlist'] = Lbamlist
    session['Hbamlist'] = Hbamlist
    optional_email=''
    task_db=Tasks(owners=current_user.id,owners_name=current_user.username,token_id=token,status='Running')
    task_db.create_time=datetime.utcnow()
    db.session.add(task_db)
    db.session.commit()
    return redirect(url_for('auth.longtask', output_folder=output_folder,url_server=url_for('auth.index',_external=True),token=token,input_type=input_type,numbermap = numbermap ,result_prefix=result_prefix,bulk_design=bulk_design,\
            P1bamfile=P1bamfilelist,\
            P2bamfile=P2bamfilelist,\
            Hbamlist=Hbamlist,\
            Lbamlist=Lbamlist, \
            cov_depth=cov_depth,\
            kernel=kernel,\
            width=width,\
            chrnum=chrnum,\
            noraw=noraw\
            ))
    
@auth.route('/longtask_run_example_map', methods=['GET', 'POST'])
@login_required
def longtask_run_example_map():
    token = str(uuid.uuid4())
    output_folder = os.path.join(current_app.config['RESULTS_FOLDER'],current_user.email)
    session['token'] = token
    bulk_design=2
    input_type='Map'
    numbermap=1
    result_prefix="myresult"
    Bulkmapfile1=['Bulk_H.map']
    Bulkmapfile2=['Bulk_L.map']
    kernel='tricube'
    length_input_file=os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','sc_chr.length')
    width=33750
    chrnum=0
    noraw='yes'
    Hmaplist = os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','Bulk_H.map')
    Lmaplist = os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','Bulk_L.map')
    session['Lmaplist'] = Lmaplist
    session['Hmaplist'] = Hmaplist
    optional_email=''
    task_db=Tasks(owners=current_user.id,owners_name=current_user.username,token_id=token,status='Running')
    task_db.create_time=datetime.utcnow()
    db.session.add(task_db)
    db.session.commit()
    return redirect(url_for('auth.longtask', output_folder=output_folder,url_server=url_for('auth.index',_external=True),token=token,input_type=input_type,numbermap = numbermap ,result_prefix=result_prefix,bulk_design=bulk_design,\
            Bulkmapfile1=Bulkmapfile1,\
            Bulkmapfile2=Bulkmapfile2,\
            kernel=kernel,\
            width=width,\
            chrnum=chrnum,\
            noraw=noraw,\
            length_input_file=length_input_file\
            ))

@auth.route('/longtask_run_example_pseudo_map', methods=['GET', 'POST'])
@login_required
def longtask_run_example_pseudo_map():
    token = str(uuid.uuid4())
    output_folder = os.path.join(current_app.config['RESULTS_FOLDER'],current_user.email)
    session['token'] = token
    bulk_design=2
    input_type='Map'
    numbermap=1
    result_prefix="myresult"
    Bulkmapfile1=['pseudo_Bulk_H.map']
    Bulkmapfile2=['pseudo_Bulk_L.map']
    kernel='tricube'
    length_input_file=os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','pseudo_chr.length')
    width=33750
    chrnum=0
    noraw='yes'
    Hmaplist = os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','pseudo_Bulk_H.map')
    Lmaplist = os.path.join(current_app.config['UPLOADS_FOLDER'],'ref','pseudo_Bulk_L.map')
    session['Lmaplist'] = Lmaplist
    session['Hmaplist'] = Hmaplist
    optional_email=''
    task_db=Tasks(owners=current_user.id,owners_name=current_user.username,token_id=token,status='Running')
    task_db.create_time=datetime.utcnow()
    db.session.add(task_db)
    db.session.commit()
    return redirect(url_for('auth.longtask', output_folder=output_folder,url_server=url_for('auth.index',_external=True),token=token,input_type=input_type,numbermap = numbermap ,result_prefix=result_prefix,bulk_design=bulk_design,\
            Bulkmapfile1=Bulkmapfile1,\
            Bulkmapfile2=Bulkmapfile2,\
            kernel=kernel,\
            width=width,\
            chrnum=chrnum,\
            noraw=noraw,\
            length_input_file=length_input_file\
            ))
                    
@auth.route('/waiting/<task_id>', methods=['GET', 'POST'])
@login_required
def waiting(task_id):
    if int(session.get('bulk_design'))==2 and session.get('input_type')=='Bam' :
        task = long_task_bam2.AsyncResult(task_id)
    elif int(session.get('bulk_design'))==1 and session.get('input_type')=='Bam' :
        task = long_task_bam1.AsyncResult(task_id)
    elif int(session.get('bulk_design'))==2 and session.get('input_type')=='Fastq' :
        task = long_task_fastq2.AsyncResult(task_id)
    elif int(session.get('bulk_design'))==1 and session.get('input_type')=='Fastq' :
        task = long_task_fastq1.AsyncResult(task_id)
    elif int(session.get('bulk_design'))==2 and session.get('input_type')=='Map' :
        task = long_task_map2.AsyncResult(task_id)
    else:
        task = long_task_map1.AsyncResult(task_id)
    if task.state == 'PENDING':
        response = {
            'state': task.state,
            'current': 0,
            'status': 'Pending...'
        }
    elif task.state != 'FAILURE':
        response = {
            'state': task.state,
            'current': task.info.get('current', 0),
            'status': task.info.get('status', ''),
            "progress_bar":task.info.get('progress_bar', '')
        }
        if 'result' in task.info:
            response['result'] = task.info['result']
    else:
        # something went wrong in the background job
        response = {
            'state': task.state,
            'current': 1,
            'status': str(task.info),  # this is the exception raised
            "progress_bar":task.info.get('progress_bar', '')
        }
    return jsonify(response)
        
@auth.route('/results', methods=['GET', 'POST'])
@login_required
def results():
    params = request.args.items()
    print params
    for i, j in params:
        session[i] = j.encode("ascii")
    flash('Calculation done!')
#    token = str(session.get('token'))
    if session.get('input_type')=='Bam':
        flash('A job done email has been sent to you by email.')
        return render_template('results.html', input_type=session.get('input_type'),token=session.get('token'),pcutoff=float(session.get('pcutoff')), pdf_file=str(session.get('pdf_file')), gcutoff=float(session.get('gcutoff')), region_anno_file=str(session.get('region_anno_file'))+'.copy', G_file=str(session.get('G_file')), peak_anno_file=str(session.get('peak_anno_file'))+'.copy',freq_pdf_file=str(session.get('freq_pdf_file')),P1_P2_combined_exonic_variant_function_file=str(session.get('P1_P2_combined_exonic_variant_function_file'))+'copy')
    elif session.get('input_type')=='Fastq':
        flash('A job done email has been sent to you by email.')
        return render_template('results.html', input_type=session.get('input_type'),token=session.get('token'),pcutoff=float(session.get('pcutoff')), pdf_file=str(session.get('pdf_file')), gcutoff=float(session.get('gcutoff')), region_anno_file=str(session.get('region_anno_file'))+'.copy', G_file=str(session.get('G_file')), peak_anno_file=str(session.get('peak_anno_file'))+'.copy',freq_pdf_file=str(session.get('freq_pdf_file')),P1_P2_combined_exonic_variant_function_file=str(session.get('P1_P2_combined_exonic_variant_function_file'))+'copy')
    elif session.get('input_type')=='Map':
        flash('A job done email has been sent to you by email.')
        return render_template('results.html', input_type=session.get('input_type'),token=session.get('token'),pcutoff=float(session.get('pcutoff')), pdf_file=str(session.get('pdf_file')), gcutoff=float(session.get('gcutoff')),  G_file=str(session.get('G_file')), freq_pdf_file=str(session.get('freq_pdf_file')))
    else:
        pass
        
@auth.route('/download/<token_id>/<file>')
@login_required
def download(token_id,file):
    """Download"""
    return send_from_directory(os.path.join(current_app.config['RESULTS_FOLDER'], current_user.email, token_id),file, as_attachment=True)

@auth.route('/downloadref/<file>')
@login_required
def downloadref(file):
    """Download ref"""
    return send_from_directory(os.path.join(current_app.config['UPLOADS_FOLDER'] ,'ref'), file, as_attachment=True)

@auth.route('/downloadresults/<token_id>/<file>')
@login_required
def downloadresults(token_id,file):
    """Download results"""
    return send_from_directory(os.path.join(current_app.config['RESULTS_FOLDER'], current_user.email, token_id), file, as_attachment=True)

@auth.route("/show_G_file/<token_id>/<file>")
@login_required
def show_G_file(token_id,file):
    G_table = pd.read_table(os.path.join(current_app.config['RESULTS_FOLDER'], current_user.email,token_id, file), header=None, names=['chr', 'coord', 'G','Gprime'], index_col=False)
    Gprime_mean=G_table.groupby(['chr'])['Gprime'].quantile([0,0.25,0.5,0.75,1])
    n_groups=G_table.groupby(['chr']).ngroups
    chr_levels=[]
    for i in range(1,n_groups+1):
        chr_levels.append(Solution().intToRoman(i))
    Gprime_mean.index.levels=[chr_levels,[0.0, 0.25, 0.5, 0.75, 1.0]]
    Gprime_mean_OrderedDict=OrderedDict()
    for i,j in enumerate(chr_levels):
        Gprime_mean_OrderedDict[j]=[Gprime_mean[5*i],Gprime_mean[5*i+1],Gprime_mean[5*i+3],Gprime_mean[5*i+4],Gprime_mean[5*i+2]]
#    G_table['chr']=G_table['chr'].replace([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],['I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII','XIII','XIV','XV','XVI'])
    return jsonify(Gprime_mean_OrderedDict)
    
@auth.route("/show_G_file2/<token_id>/<file>")
@login_required
def show_G_file2(token_id,file):
    G_table = pd.read_table(os.path.join(current_app.config['RESULTS_FOLDER'], current_user.email,token_id, file), header=None, names=['chr', 'coord', 'G','Gprime'], index_col=False)
    n_groups=G_table.groupby(['chr']).ngroups
    chr_levels=[]
    chr_levels_numeric=[]
    for i in range(1,n_groups+1):
        chr_levels_numeric.append(i-1)
        chr_levels.append(Solution().intToRoman(i))     
    G_table =G_table [['chr', 'coord','Gprime']]
    G_table['chr']=G_table['chr'].replace(chr_levels_numeric,chr_levels)
    Gprime_OrderedDict=G_table.to_dict(orient='index')
#    Gprime_OrderedDict=OrderedDict()
#    for i in range(len(G_table)):
#        Gprime_OrderedDict[i]=[G_table['chr'][i],G_table['coord'][i],G_table['Gprime'][i]]
    return jsonify(Gprime_OrderedDict)
    
    
@auth.route("/show_mapfile_tables/<token_id>/<file>")
@login_required
def show_mapfile_tables(token_id,file):
    mapfile_table = pd.read_table(os.path.join(current_app.config['RESULTS_FOLDER'], current_user.email,token_id, file), header=None, names=['chr', 'coord', 'P1', 'P2'], index_col=False)
#    mapfile_table_P1_mean=mapfile_table.groupby(['chr'])['P1'].quantile([0,0.25,0.5,0.75,1])
#    mapfile_table_P2_mean=mapfile_table.groupby(['chr'])['P2'].quantile([0,0.25,0.5,0.75,1])
    mapfile_table_P1_mean=mapfile_table.groupby(['chr'])['P1'].mean().to_dict()
    mapfile_table_P2_mean=mapfile_table.groupby(['chr'])['P2'].mean().to_dict()
#    mapfile_table_P1_mean.index.levels=[['I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII','XIII','XIV','XV','XVI'],[0.0, 0.25, 0.5, 0.75, 1.0]]
#    mapfile_table_P1_mean_OrderedDict=OrderedDict()
#    for i,j in enumerate(['I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII','XIII','XIV','XV','XVI']):
#        mapfile_table_P1_mean_OrderedDict[j]=[mapfile_table_P1_mean[5*i],mapfile_table_P1_mean[5*i+1],mapfile_table_P1_mean[5*i+3],mapfile_table_P1_mean[5*i+4],mapfile_table_P1_mean[5*i+2]]
#    mapfile_table_P2_mean.index.levels=[['I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII','XIII','XIV','XV','XVI'],[0.0, 0.25, 0.5, 0.75, 1.0]]
#    mapfile_table_P2_mean_OrderedDict=OrderedDict()
#    for i,j in enumerate(['I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII','XIII','XIV','XV','XVI']):
#        mapfile_table_P2_mean_OrderedDict[j]=[mapfile_table_P2_mean[5*i],mapfile_table_P2_mean[5*i+1],mapfile_table_P2_mean[5*i+3],mapfile_table_P2_mean[5*i+4],mapfile_table_P2_mean[5*i+2]]    
#    return jsonify({'P1':mapfile_table_P1_mean_OrderedDict,'P2':mapfile_table_P2_mean_OrderedDict})
    return jsonify({'P1':mapfile_table_P1_mean,'P2':mapfile_table_P2_mean})

@auth.route("/show_mapfile_tables_page/<token_id>/<file>")
@login_required
def show_mapfile_tables_page(token_id,file):
    return render_template('view_mapfile_tables.html',token_id=token_id,file=file)    

@auth.route("/show_freq_file/<token_id>/<file>")
@login_required
def show_freq_file(token_id,file):
    freq_table = pd.read_table(os.path.join(current_app.config['RESULTS_FOLDER'], current_user.email,token_id, file), header=None, names=['label', 'x', 'y'], index_col=False,sep=' ')
    n_groups=freq_table.groupby(['label']).ngroups
    color_name_index=['Blue','BlueViolet','Brown','BurlyWood','CadetBlue','Chartreuse','Chocolate','CornflowerBlue',\
                 'Crimson','DarkBlue','DarkCyan','DarkGoldenRod','DarkMagenta','DarkOrange','DeepSkyBlue','ForestGreen']
    color_index=OrderedDict()
    for i in range(1,n_groups+1):
        color_index.update({Solution().intToRoman(i):color_name_index[i%16]})   
#    color_index={'I':'Blue','II':'BlueViolet','III':'Brown','IV':'BurlyWood','V':'CadetBlue','VI':'Chartreuse','VII':'Chocolate','VIII':'CornflowerBlue',\
#                 'IX':'Crimson','X':'DarkBlue','XI':'DarkCyan','XII':'DarkGoldenRod','XIII':'DarkMagenta','XIV':'DarkOrange','XV':'DeepSkyBlue','XVI':'ForestGreen'}
    freq_table['color']=[color_index[i] for i in [x for x in freq_table['label']]]
    freq_Dict={}
    for i in color_index.keys():
        freq_Dict[i]=freq_table.groupby('label').get_group(i).to_dict(orient='records')
    return jsonify(freq_Dict)


@auth.route("/show_freq_file_mean/<token_id>/<file>")
@login_required
def show_freq_file_mean(token_id,file):
    freq_table = pd.read_table(os.path.join(current_app.config['RESULTS_FOLDER'], current_user.email,token_id, file), header=None, names=['chr', 'coord', 'freq'], index_col=False,sep=' ')       
    freq_table_mean=freq_table['freq'].mean()
    return jsonify({'freq_mean':freq_table_mean})
    
@auth.route("/show_freq_file_page/<token_id>/<file>")
@login_required
def show_freq_file_page(token_id,file):
    return render_template('view_freq_table.html',token_id=token_id,file=file)

    
@auth.route("/show_threshold/<token_id>/<threshold_file>")
@login_required
def show_threshold(token_id,threshold_file):
    threshold_table = pd.read_table(os.path.join(current_app.config['RESULTS_FOLDER'], current_user.email,token_id, threshold_file), header=0, names=['pcutoff','gcutoff'], index_col=False)
    if threshold_table['pcutoff'][0]=='None':
        threshold_table['pcutoff'][0]=0
    if threshold_table['gcutoff'][0]=='None':
        threshold_table['gcutoff'][0]=0    
    return jsonify({'pcutoff':threshold_table['pcutoff'][0],'gcutoff':threshold_table['gcutoff'][0]})

@auth.route("/show_threshold_page/<token_id>/<threshold_file>")
@login_required
def show_threshold_page(token_id,threshold_file):
    threshold_table = pd.read_table(os.path.join(current_app.config['RESULTS_FOLDER'], current_user.email,token_id, threshold_file), header=0, names=['pcutoff','gcutoff'], index_col=False)
    return render_template('view_threshold_table.html',token_id=token_id,threshold_table=threshold_table)

@auth.route("/show_peak_page/<token_id>/<peak_file>")
@login_required
def show_peak_page(token_id,peak_file):
    peak_table = pd.read_table(os.path.join(current_app.config['RESULTS_FOLDER'], current_user.email,token_id, peak_file), header=None, names=['chr','coord','coord1'], index_col=False)
    return render_template('view_peak_table.html',token_id=token_id,peak_table=peak_table)

@auth.route("/show_region_page/<token_id>/<region_file>")
@login_required
def show_region_page(token_id,region_file):
    region_table = pd.read_table(os.path.join(current_app.config['RESULTS_FOLDER'], current_user.email,token_id, region_file), header=None, names=['chr','coord1','coord2'], index_col=False)
    return render_template('view_region_table.html',token_id=token_id,region_table=region_table)

@auth.route("/show_G_file_page/<token_id>/<file>/<threshold_file>")
@login_required
def show_G_file_page(token_id,file,threshold_file):
    return render_template('view_G_table.html',token_id=token_id,file=file,threshold_file=threshold_file)

@auth.route("/show_region_anno_tables/<token_id>/<file>")
@login_required
def show_region_anno_tables(token_id,file):
    region_anno = pd.read_table(os.path.join(current_app.config['RESULTS_FOLDER'], current_user.email,token_id, file), header=0, names=['chr', 'coord_variant', 'P1_start', 'P1_stop', 'P1_ref', 'P1_alt', 'P2_alt', 'P1_variant_type', 'P1_anno', 'gene_id_P1', 'exon_P1', 'nt_P1', 'protein_P1', 'gene_id_link_P1', 'P2_variant_type', 'P2_anno', 'gene_id_P2', 'exon_P2', 'nt_P2', 'protein_P2', 'gene_id_link_P2','P1_SNAP',	'P2_SNAP', 'chr_region', 'region_start', 'region_end'], index_col=False)
    region_anno1=region_anno[['chr', 'region_start', 'region_end', 'coord_variant',  'P1_ref', 'P1_alt', 'P2_alt', 'P1_variant_type', 'P1_anno', 'gene_id_P1', 'exon_P1', 'nt_P1', 'protein_P1','P1_SNAP', 'gene_id_link_P1', 'P2_variant_type', 'P2_anno', 'gene_id_P2', 'exon_P2', 'nt_P2', 'protein_P2','P2_SNAP', 'gene_id_link_P2']]
    return render_template('view_region_anno.html',token_id=token_id,table_file=region_anno1)

@auth.route("/show_peak_anno_tables/<token_id>/<file>")
@login_required
def show_peak_anno_tables(token_id,file):
    peak_anno = pd.read_table(os.path.join(current_app.config['RESULTS_FOLDER'], current_user.email,token_id, file), header=0, names=['chr', 'coord_variant', 'start', 'stop', 'P1_ref', 'P1_alt', 'P2_alt', 'P1_variant_type', 'P1_anno', 'gene_id_P1', 'exon_P1', 'nt_P1', 'protein_P1', 'gene_id_link_P1', 'P2_variant_type', 'P2_anno', 'gene_id_P2', 'exon_P2', 'nt_P2', 'protein_P2', 'gene_id_link_P2','P1_SNAP',	'P2_SNAP', 'chr_peak', 'peak', 'end_region'], index_col=False)
    peak_anno1=peak_anno[['chr', 'peak', 'coord_variant',  'P1_ref', 'P1_alt', 'P2_alt', 'P1_variant_type', 'P1_anno', 'gene_id_P1', 'exon_P1', 'nt_P1', 'protein_P1', 'P1_SNAP','gene_id_link_P1', 'P2_variant_type', 'P2_anno', 'gene_id_P2', 'exon_P2', 'nt_P2', 'protein_P2', 'P2_SNAP','gene_id_link_P2']]
    return render_template('view_peak_anno.html',token_id=token_id,table_file=peak_anno1)

@auth.route("/show_tables/<token_id>/<file>")
@login_required
def show_tables(token_id,file):
    return render_template('view.html',token_id=token_id,table_file=file)

@auth.route("/show_P1tables_api/<token_id>/<file>")
@login_required
def show_P1tables_api(token_id,file):
    ff=pd.read_table(os.path.join(current_app.config['RESULTS_FOLDER'], current_user.email, token_id,file), header=0)
    ff.fillna('NotAvail',inplace=True)
    data_table=ff
    aa=data_table.groupby(['P1_variant_type'])['P1_variant_type'].value_counts()
    aa_OrderedDict=OrderedDict()
    for i in aa.index.levels[0]:
        aa_OrderedDict[i]=aa[i].values[0]
    aa_OrderedDict.popitem(last=True)
    aa_OrderedDict.popitem(last=False)    
    return jsonify(aa_OrderedDict)

@auth.route("/show_P2tables_api/<token_id>/<file>")
@login_required
def show_P2tables_api(token_id,file):
    ff=pd.read_table(os.path.join(current_app.config['RESULTS_FOLDER'], current_user.email, token_id,file), header=0)
    ff.fillna('NotAvail',inplace=True)
    data_table=ff
    aa=data_table.groupby(['P2_variant_type'])['P2_variant_type'].value_counts()
    aa_OrderedDict=OrderedDict()
    for i in aa.index.levels[0]:
        aa_OrderedDict[i]=aa[i].values[0]
    aa_OrderedDict.popitem(last=False)    
    return jsonify(aa_OrderedDict)
    
@auth.route("/tables/<token_id>/<file>")
@login_required
def tables(token_id,file):
    params = request.args.items()
    params1={}
    for i in params:
        j,k=i
        params1[j]=k
    print params1
    ff=pd.read_table(os.path.join(current_app.config['RESULTS_FOLDER'], current_user.email, token_id,file), header=0)
    ff.fillna('NotAvail',inplace=True)
    data_table=ff
    request_values=params1
    data_table_columns=list(data_table)
    result_data_table = None
    data_table_cardinality_filtered = 0
    data_table_cardinality = len(data_table.index)
    data_table_draw=int(params1['draw'])
    data_table_start=int(params1['start'])
    data_table_length=int(params1['length'])
    if (request_values.has_key('search[value]') ) and ( request_values['search[value]'] != "" ):             
        row_filter = [] 
        alist=[]
        for i in data_table_columns:
            data_table[i]=data_table[i].astype(str)
            alist=alist+data_table.index[data_table[i].str.contains(request_values['search[value]'])].tolist()
        row_filter=list(set(alist))             
        result_data_table = data_table.iloc[row_filter]
        data_table_cardinality_filtered=len(result_data_table.index)
        if request_values.has_key('order[0][column]'):
            if request_values['order[0][dir]']=='asc':
                result_data_table.sort_values(data_table_columns[int(request_values['order[0][column]'])],0,True,True)
            else:
                result_data_table.sort_values(data_table_columns[int(request_values['order[0][column]'])],0,False,True)
    else:
        result_data_table=data_table
        data_table_cardinality_filtered=len(result_data_table.index)
        if request_values.has_key('order[0][column]'):
            if request_values['order[0][dir]']=='asc':
                result_data_table.sort_values(data_table_columns[int(request_values['order[0][column]'])],0,True,True)
            else:
                result_data_table.sort_values(data_table_columns[int(request_values['order[0][column]'])],0,False,True)
    if data_table_start+data_table_length>ff.shape[0]:
        try:
            last_nrow=ff.shape[0]-data_table_start
            result_data_table2=result_data_table.iloc[range(data_table_start,data_table_start+last_nrow)]
        except:
            result_data_table2=result_data_table
    else:
        try:
            result_data_table2=result_data_table.iloc[range(data_table_start,data_table_start+data_table_length)]
        except:
            result_data_table2=result_data_table    
    return jsonify({'draw':data_table_draw,'recordsTotal':data_table_cardinality,'recordsFiltered':data_table_cardinality_filtered,'data':result_data_table2.to_dict(orient='records')})

@auth.route('/Home', methods=['GET', 'POST'])
@login_required
def Home():
    return redirect(url_for('auth.index'))
#    return render_template('Home.html')

@auth.route('/index', methods=['GET', 'POST'])
@login_required
def index():
    return render_template('index.html')

@auth.route('/Use', methods=['GET', 'POST'])
@login_required
def Use():
    return render_template('Use.html')

@auth.route('/Contact', methods=['GET', 'POST'])
@login_required
def Contact():
    return render_template('Contact.html')

@auth.route('/Reference', methods=['GET', 'POST'])
@login_required
def Reference():
    ref = 'BSA_NGS.pdf'
    ref2= 'ref2.pdf'
    return render_template('Reference.html', ref=ref,ref2=ref2)

@auth.route('/Genome', methods=['GET', 'POST'])
@login_required
def Genome():
    sacCer3_refGene='sacCer3_refGene.txt'
    sacCer3_refGeneMrna='sacCer3_refGeneMrna.fa'
    sc_chr_length='sc_chr.length'
    sc64_bed='sc64.bed'
    sc64_fa='sc64.fa'
    sc64_fa_fai='sc64.fa.fai'
    sc64_gtf='sc64.gtf'
    sacCer3_zip='sacCer3.zip'
    return render_template('Genome.html', sacCer3_zip=sacCer3_zip,sacCer3_refGene=sacCer3_refGene,sacCer3_refGeneMrna=sacCer3_refGeneMrna,sc_chr_length=sc_chr_length,sc64_bed=sc64_bed,sc64_fa=sc64_fa,sc64_fa_fai=sc64_fa_fai,sc64_gtf=sc64_gtf)

@auth.route('/Example', methods=['GET', 'POST'])
@login_required
def Example():
    P1_1_fastq_zip='sake_strain_P1_1.fastq.gz'
    P1_2_fastq_zip='sake_strain_P1_2.fastq.gz'
    P2_1_fastq_zip='white_tecc_strain_P2_1.fastq.gz'
    P2_2_fastq_zip='white_tecc_strain_P2_2.fastq.gz'
    P1_bam='sake_strain_P1.bam'
    P2_bam='white_tecc_strain_P2.bam'
    Hbulk_fastq_zip='Bulk_H.fastq.gz'
    Lbulk_fastq_zip='Bulk_L.fastq.gz'
    Hbulk_bam='Bulk_H.bam'
    Lbulk_bam='Bulk_L.bam'
    Hbulk_map_zip='Bulk_H.map.zip'
    Lbulk_map_zip='Bulk_L.map.zip'
    pseudo_Hbulk_map_zip='pseudo_Bulk_H.map.zip'
    pseudo_Lbulk_map_zip='pseudo_Bulk_L.map.zip'
    pseudo_chr_length='pseudo_chr.length'
    sc_chr_length='sc_chr.length'
    return render_template('Example.html', P1_1_fastq_zip=P1_1_fastq_zip,P1_2_fastq_zip=P1_2_fastq_zip,P2_1_fastq_zip=P2_1_fastq_zip,P2_2_fastq_zip=P2_2_fastq_zip,P1_bam=P1_bam,P2_bam=P2_bam,Hbulk_fastq_zip=Hbulk_fastq_zip,Lbulk_fastq_zip=Lbulk_fastq_zip,Hbulk_bam=Hbulk_bam,Lbulk_bam=Lbulk_bam,Hbulk_map_zip=Hbulk_map_zip,Lbulk_map_zip=Lbulk_map_zip,pseudo_Hbulk_map_zip=pseudo_Hbulk_map_zip,pseudo_Lbulk_map_zip=pseudo_Lbulk_map_zip,pseudo_chr_length=pseudo_chr_length,sc_chr_length=sc_chr_length)
     
#@auth.route('/Show_user', methods=['GET', 'POST'])
#@login_required
#def Show_user():
#    user=User.query.filter_by(id=current_user.id).first()
##    return redirect(url_for('auth.index'))
#    return render_template('Show_user.html',user=user)
    
@auth.route('/show_upload', methods=['GET', 'POST'])
@login_required
def show_upload():
    upload_files=Files.query.filter_by(owners=current_user.id).filter_by(ftype1 ='U').all()
    return render_template('show_upload.html',upload_files=upload_files)

@auth.route('/count_upload', methods=['GET', 'POST'])
@login_required
def count_upload():
    n=Files.query.filter_by(owners=current_user.id).filter_by(ftype1 ='U').count()
#    return jsonify({'num_uploaded_files':n})   
    return str(n)
    
@auth.route('/delete_upload', methods=['GET', 'POST'])
@login_required
def delete_upload():
    params = request.args.items()
    delete_items={}
    for i in params:
        j,k=i
        if re.search('checkbox_',j):
             delete_items[j]=k
    print delete_items
    if delete_items:
        for i,j in delete_items.items():
            f=os.path.join(current_app.config['UPLOADS_FOLDER'],current_user.email,Files.query.filter_by(id=j).first().fname)
            print f
#            if f.endswith('.fastq.gz') and os.path.isfile(os.path.splitext(f)[0]):
#                os.remove(os.path.splitext(f)[0])
            if os.path.isfile(f):
                os.remove(f)
            Files.query.filter_by(id=j).delete()
            db.session.commit()
    upload_files=Files.query.filter_by(owners=current_user.id).filter_by(ftype1 ='U').all()
    return render_template('show_upload.html',upload_files=upload_files)

@auth.route('/refresh_upload', methods=['GET', 'POST'])
@login_required
def refresh_upload():
    upload_files=Files.query.filter_by(owners=current_user.id).filter_by(ftype1 ='U').all()
    return render_template('show_upload.html',upload_files=upload_files)

@auth.route('/show_result', methods=['GET', 'POST'])
@login_required
def show_result():
    result_files=Files.query.filter_by(owners=current_user.id).filter_by(ftype1='R').all()
    return render_template('show_result.html',result_files=result_files)

@auth.route('/count_result', methods=['GET', 'POST'])
@login_required
def count_result():
    n=Files.query.filter_by(owners=current_user.id).filter_by(ftype1 ='R').count()
    return str(n)
        
@auth.route('/delete_result', methods=['GET', 'POST'])
@login_required
def delete_result():
    params = request.args.items()
    delete_items={}
    for i in params:
        j,k=i
        if re.search('checkbox_',j):
             delete_items[j]=k
    print delete_items
    if delete_items:
        for i,j in delete_items.items():
            f=os.path.join(current_app.config['RESULTS_FOLDER'],current_user.email,Files.query.filter_by(id=j).first().ffolder,Files.query.filter_by(id=j).first().fname)
            print f
            if os.path.isfile(f):
                os.remove(f)
            Files.query.filter_by(id=j).delete()
            db.session.commit()
    result_files=Files.query.filter_by(owners=current_user.id).filter_by(ftype1='R').all()
    return render_template('show_result.html',result_files=result_files)

#@auth.route('/Download_all_result', methods=['GET', 'POST'])
#@login_required
#def Download_all_result():
#    params = request.args.items()
#    delete_items={}
#    for i in params:
#        j,k=i
#        if re.search('checkbox_',j):
#             delete_items[j]=k
#    print delete_items
#    if delete_items:
#        for i,j in delete_items.items():
#            Files.query.filter_by(id=j).delete()
#            db.session.commit()
#    result_files=Files.query.filter_by(owners=current_user.id).filter_by(ftype1='R').all()
#    return render_template('show_result.html',result_files=result_files)
    
@auth.route('/refresh_result', methods=['GET', 'POST'])
@login_required
def refresh_result():
    result_files=Files.query.filter_by(owners=current_user.id).filter_by(ftype1='R').all()
    return render_template('show_result.html',result_files=result_files)
    
@auth.route('/show_job', methods=['GET', 'POST'])
@login_required
def show_job():
    job_files=Tasks.query.filter_by(owners=current_user.id).all()
    return render_template('show_job.html',job_files=job_files)

@auth.route('/delete_job', methods=['GET', 'POST'])
@login_required
def delete_job():
    params = request.args.items()
    delete_items={}
    for i in params:
        j,k=i
        if re.search('checkbox_',j):
             delete_items[j]=k
    print delete_items
    if delete_items:
        for i,j in delete_items.items():
            running_tasks=Tasks.query.filter_by(id=j).filter_by(status='Running').all()
            if running_tasks:
                for running_task in running_tasks:
                    running_task_id=running_task.token_id
                    Control(app=celery).revoke(running_task_id,  terminate=True)
                    Tasks.query.filter_by(owners=current_user.id).filter_by(token_id=running_task_id).delete()        
                    Files.query.filter_by(owners=current_user.id).filter_by(ffolder=running_task_id).delete()        
                    remove_folder(os.path.join(current_app.config['RESULTS_FOLDER'],current_user.email,running_task_id))
            other_tasks=Tasks.query.filter_by(id=j).filter(Tasks.status!='Running').all()
            if other_tasks:
                for other_task in other_tasks:
                    other_task_id=other_task.token_id
                    Tasks.query.filter_by(owners=current_user.id).filter_by(token_id=other_task_id).delete()        
                    Files.query.filter_by(owners=current_user.id).filter_by(ffolder=other_task_id).delete()        
                    remove_folder(os.path.join(current_app.config['RESULTS_FOLDER'],current_user.email,other_task_id))        
            db.session.commit()
    job_files=Tasks.query.filter_by(owners=current_user.id).all()
    return render_template('show_job.html',job_files=job_files)

@auth.route('/terminate_job', methods=['GET', 'POST'])
@login_required
def terminate_job():
    params = request.args.items()
    terminate_items={}
    for i in params:
        j,k=i
        if re.search('checkbox_',j):
             terminate_items[j]=k
    print terminate_items
    if terminate_items:
        for i,j in terminate_items.items():
            running_tasks=Tasks.query.filter_by(id=j).filter_by(status='Running').all()
            if running_tasks:
                for running_task in running_tasks:
                    running_task_id=running_task.token_id
                    Control(app=celery).revoke(running_task_id,  terminate=True)
                    task_db=Tasks.query.filter_by(owners=current_user.id).filter_by(token_id=running_task_id).first()
                    if task_db:
                        task_db.status='Failed'        
                    Files.query.filter_by(owners=current_user.id).filter_by(ffolder=running_task_id).delete()        
                    db.session.add(task_db)
            db.session.commit()
    job_files=Tasks.query.filter_by(owners=current_user.id).all()
    return render_template('show_job.html',job_files=job_files)
        
@auth.route('/refresh_job', methods=['GET', 'POST'])
@login_required
def refresh_job():
    job_files=Tasks.query.filter_by(owners=current_user.id).all()
    return render_template('show_job.html',job_files=job_files)
       
@auth.route('/count_job_running', methods=['GET', 'POST'])
@login_required
def count_job_running():
    n=Tasks.query.filter_by(owners=current_user.id).filter_by(status ='Running').count()
    return str(n)

@auth.route('/count_job_done', methods=['GET', 'POST'])
@login_required
def count_job_done():
    n=Tasks.query.filter_by(owners=current_user.id).filter_by(status ='Done').count()
    return str(n)

@auth.route('/count_job_failed', methods=['GET', 'POST'])
@login_required
def count_job_failed():
    n=Tasks.query.filter_by(owners=current_user.id).filter_by(status ='Failed').count()
    return str(n)
    
@auth.route('/count_free_upload', methods=['GET', 'POST'])
@login_required
def count_free_upload():
    free_upload=count_free_upload_api()
    if free_upload<1000 and free_upload>0:
        return '<span  class="badge badge-danger">'+str(free_upload)+'B'+'</span>'
    elif free_upload >= 1000 and free_upload<1000000:
        return '<span  class="badge badge-warning">'+str(free_upload/1000)+'K'+'</span>'
    elif free_upload >= 1000000 and free_upload<1000000000:
        return '<span  class="badge badge-success">'+str(free_upload/1000000)+'M'+'</span>'
    elif free_upload >= 1000000000 and free_upload<1000000000000:
        return '<span  class="badge badge-success">'+str(free_upload/1000000000)+'G'+'</span>'
    else:
        return '<span  class="badge badge-danger">'+'NA'+'</span>'

@auth.route('/view_free_upload', methods=['GET', 'POST'])
@login_required
def view_free_upload():
    used_upload=get_folder_size(os.path.join(current_app.config['UPLOADS_FOLDER'],current_user.email))
    total_space=int(current_app.config['CUSTOM_UPLOADS_FOLDER_SIZE_LIMIT'])
    free_upload=int(current_app.config['CUSTOM_UPLOADS_FOLDER_SIZE_LIMIT'])-int(used_upload)
    if free_upload<0:
        free_upload=0
    return render_template('view_free_upload.html',total= total_space,used=used_upload,free=free_upload)

@auth.route('/view_free_result', methods=['GET', 'POST'])
@login_required
def view_free_result():
    used_result=get_folder_size(os.path.join(current_app.config['RESULTS_FOLDER'],current_user.email))
    total_space=int(current_app.config['CUSTOM_RESULTS_FOLDER_SIZE_LIMIT'])
    free_result=int(current_app.config['CUSTOM_RESULTS_FOLDER_SIZE_LIMIT'])-int(used_result)
    if free_result<0:
        free_result=0
    return render_template('view_free_result.html',total= total_space,used=used_result,free=free_result)

@auth.route('/count_free_result', methods=['GET', 'POST'])
@login_required
def count_free_result():
    free_result=count_free_result_api()
    if free_result<1000 and free_result>0:
        return '<span  class="badge badge-danger">'+str(free_result)+'B'+'</span>'
    elif free_result >= 1000 and free_result<1000000:
        return '<span  class="badge badge-warning">'+str(free_result/1000)+'K'+'</span>'
    elif free_result >= 1000000 and free_result<1000000000:
        return '<span  class="badge badge-success">'+str(free_result/1000000)+'M'+'</span>'
    elif free_result >= 1000000000 and free_result<1000000000000:
        return '<span  class="badge badge-success">'+str(free_result/1000000000)+'G'+'</span>'
    else:
        return '<span  class="badge badge-danger">'+'NA'+'</span>'
        
@auth.route('/guideline', methods=['GET', 'POST'])
@login_required
def guideline():
    return render_template('guideline.html')
    
@auth.route('/result_format', methods=['GET', 'POST'])
@login_required
def result_format():
    return render_template('result_format.html')

@auth.route('/get_chr', methods=['GET', 'POST'])
@login_required
def get_chr():
    filename=request.json['chr_file_name']
    print filename
    nline=len(open(os.path.join(current_app.config['UPLOADS_FOLDER'], current_user.email,filename)).readlines(  ))        
    chr_choice=OrderedDict()
    chr_choice['all']='all'
    for i in range(1,nline+1):
        chr_choice[i]=i
    return jsonify(chr_choice)