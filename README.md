# BSA4Yeast

BSA4Yeast is a comprehensive web application that performs QTL (Quantitative Trait Loci) mapping using bulk segregant analysis (BSA) of yeast sequencing data. It has the following main advantages and features: 

- fully automated
- multiple input file types
- private storage space
- comprehensive annotations
- web-based exploration

## Website of BSA4Yeast
[https://bsa4yeast.lcsb.uni.lu](https://bsa4yeast.lcsb.uni.lu)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

#### Virtual machine info
```
- os: CentOS 7.2
- memory: 16GB
- cores: 8
- user: bsa4yeast
- python app: /home/bsa4yeast/bsa4yeast
- python version: 2.7
```

### Installing

A step by step series of examples that tell you how to get a development env running

```
sudo yum update
sudo yum install redis python-devel tkinter
sudo yum group install "Development Tools"
sudo systemctl start redis
sudo systemctl enable redis

sudo easy_install pip

sudo adduser bsa4yeast
sudo su - bsa4yeast
git clone ssh://git@git-r3lab-server.uni.lu:8022/zhi.zhang/bsa4yeast.git
exit
sudo pip install -r /home/bsa4yeast/bsa4yeast/requirements.txt

sudo pip install gunicorn

sudo yum install nginx
```

### Deployment

#### Gunicorn

1) Create the file ```/etc/systemd/system/gunicorn.service``` with the following content:

```
[Unit]
Description=gunicorn daemon
After=network.target

[Service]
PIDFile=/run/gunicorn/pid
User=bsa4yeast
Group=bsa4yeast
WorkingDirectory=/home/bsa4yeast/bsa4yeast
ExecStart=/usr/bin/gunicorn --limit-request-line 0 --access-logfile /home/bsa4yeast/log/gunicorn_access.log --error-logfile /home/bsa4yeast/log/gunicorn_error.log --log-level debug --workers 2 --bind unix:/home/bsa4yeast/bsa4yeast/bsa4yeast.sock manage:app
ExecReload=/bin/kill -s HUP $MAINPID
ExecStop=/bin/kill -s TERM  $MAINPID

[Install]
WantedBy=multi-user.target
```
#### User

application is installed under bsa4yeast user
use `sudo su - bsa4yeast` to change user



#### Web server

1) Create the file ```/etc/nginx/conf.d/bsa4yeast.conf``` with the following content:

```
proxy_connect_timeout       600;
proxy_send_timeout          600;
proxy_read_timeout          600;
send_timeout                600;

server {
    listen 80;
    client_max_body_size    3000m;
    server_name bsa4yeast.uni.lu;

    location /static {
        alias /home/bsa4yeast/bsa4yeast/app/static;
        autoindex on;
    }

   
    location / {
        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_pass http://unix:/home/bsa4yeast/bsa4yeast/bsa4yeast.sock;
    }
}
```


2) Edit the file /etc/nginx/nginx.conf:

Comment out the block server {} in /etc/nginx/nginx.conf
Change the user running nginx from nginx to bsa4yeast

3) Restart nginx

```
sudo systemctl restart nginx
```

#### Celery Worker

```
sudo yum install supervisor
```
Create the file /etc/supervisord.d/workers.ini with the following content:

```
[program:worker]
command=/bin/celery -A celery_worker.celery worker --workdir=/home/bsa4yeast/bsa4yeast/ --concurrency=4 --loglevel=debug -n %%h-parse
stdout_logfile=/home/bsa4yeast/log/celery_worker.log
stderr_logfile=/home/bsa4yeast/log/celery_worker.log
stdout_logfile_backups=5
stdout_logfile_maxbytes=10MB
stderr_logfile_backups=5
stderr_logfile_maxbytes=10MB
autostart=true
autorestart=true
startsecs=10
stopwaitsecs=3600
killasgroup=true
user=bsa4yeast
```
#### Operation Manual

If web application python code is modified, gunicorn should be restarted:

```
sudo systemctl restart gunicorn
```

If celery worker python code is modified, restart the worker with:

```
sudo supervisorctl restart worker
```

#### config.py in bsa4yeast
```
import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'hard to guess string'
    WTF_CSRF_ENABLED = False
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS= True
    MAIL_SERVER = 'localhost'
    MAIL_PORT = 25
#    MAIL_USE_TLS = True
    MAIL_USERNAME = 'root'
    MAIL_PASSWORD = ''
    FLASKY_MAIL_SUBJECT_PREFIX = '[BSA4Yeast]'
    FLASKY_MAIL_SENDER = 'BSA4Yeast Admin <Admin@BSA4Yeast.com>'
    UPLOADS_FOLDER = os.path.join(basedir, 'app/uploads')
    RESULTS_FOLDER = os.path.join(basedir, 'app/results')
    UPLOADS_ANONY_FOLDER = os.path.join(basedir, 'app/uploads_anony')
    RESULTS_ANONY_FOLDER = os.path.join(basedir, 'app/results_anony')
    GENOMES_FOLDER = os.path.join(basedir, 'app/genomes')
    ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
    FLASKY_ADMIN = os.environ.get('FLASKY_ADMIN')
    CELERY_BROKER_URL = 'redis://localhost:6379/1'
    CELERY_RESULT_BACKEND = 'redis://localhost:6379/2'
    CELERY_TASK_SERIALIZER = 'json'
    CELERY_IMPORTS=("app.tasks.mytask")
    DROPZONE_MAX_FILE_SIZE=3000
    DROPZONE_ALLOWED_FILE_CUSTOM=True
    DROPZONE_ALLOWED_FILE_TYPE='.fastq,.map,.bam'
    CUSTOM_UPLOADS_FOLDER_SIZE_LIMIT=8000000000
    CUSTOM_RESULTS_FOLDER_SIZE_LIMIT=4000000000
    DROPZONE_REDIRECT_VIEW='auth.upload'

    @staticmethod
    def init_app(app):
        pass

class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'data-dev.sqlite')

class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'data-test.sqlite')

class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, 'data.sqlite')

config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}
```

## Authors

****
|Author|Zhi Zhang|
|---|---
|E-mail|[zhi.zhang@uni.lu](mailto:zhi.zhang@uni.lu)

****

## License

This project is licensed under the GNU GPL License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* [Valentin Groues](https://wwwen.uni.lu/lcsb/people/valentin_groues)


